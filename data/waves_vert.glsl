uniform float time;
uniform float xs, ys; // X/Y Frequencies
uniform float h; // Height scale value

void main()
{
	// Get the GL vertex and alter it according to the sin function.
	vec4 t = gl_Vertex;
	t.z = gl_Vertex.z
		+ h * sin(time + xs * gl_Vertex.x);
		+ h * sin(time + ys * gl_Vertex.y);
	// Set the new position thingummy.
	
	gl_Position =
		gl_ModelViewProjectionMatrix * t;

	// Texture
	gl_TexCoord[0] = gl_MultiTexCoord0;
	
}
