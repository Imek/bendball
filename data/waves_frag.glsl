uniform sampler2D tex;

// Doesn't do anything extra..
void main(void)
{
	//gl_FragColor = gl_Color;
	gl_FragColor = texture2D(tex,gl_TexCoord[0].st);
}
