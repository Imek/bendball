Introduction
============

BendBall is a 2-player 3D game, in which the objective of each player is to get
the ball past the other. The first player to 10 points wins the game. This is a
very preliminary version, and there are already a few known issues that need
ironing out.

Players move up/down/left/right to position their paddle to deflect the ball,
and back and forth to serve or increase the ball's speed.


Development Version Info
========================

This project comes with a CMakeLists file, allowing you to generate any
platform-specific project and build files you desire. For details, see:

http://www.cmake.org

Primarily developed and tested in VS2010 on Windows 7; comes with bundled GLUT
library for that environment.


Controls
========

Player 1 (Blue)
--
W     - Up
S     - Down
A     - Left
D     - Right
G     - Back
T     - Forward


Player 2 (Red)
--
8     - Up
2     - Down
4     - Left
6     - Right
1     - Back
7     - Forward


Known Issues
============

- The depth marker for the ball may still show after the ball sinks into lava
  but before it respawns.
- Player 1 currently always gets to serve, even if Player 2 scored.
- It is possible for the ball to slow down too much and the game to become
  impossible to continue. A non-game-breaking grabber, or possible a minimum
  lateral ball speed, could be implemented to fix this.
- Collision resolution needs improvement to prevent objects penetrating,
  particularly between the ball and the walls/deflectors.

Changelog
=========

0.12
--
-Project code restructure, and bundled libraries.
-Grabber use is now automatic, preventing players from just grabbing the ball during normal play.
-Settings tweaks to improve gameplay; particularly physics and game field size.

0.11b
--
-Fixed & set up to build with CMake and VS2010.

0.11a
--
-Added the maximum speed requirement to the grab feature.
-Tweaked physics settings to reduce the appearance of glitchy behaviour.

0.1a
--
-Initial release of alpha version.


Author & Licence
================

Name: Joe Forster
E-mail: me@joeforster.com
Website: http://www.joeforster.com
Licence: See accompanying file LICENCE.txt
