#ifndef BALL_H
#define BALL_H

#include "Engine/WorldEntity.h"
#include "Engine/Particles.h"

/**
 * Ball class: this specialisation of WorldEntity is for a ball, including the particles.
 * The putting of particles and their updates here is justified in the same way as with the
 * Deflector class. Also controlled here is the physical spinning of the ball, changed when
 * the ball collides with a moving deflector. Perhaps a better way to do this would have been
 * to have all concepts of "spin" kept in the BallController and just the orientation of the
 * ball set through a method, though this is arguably a small problem.
 */
class Ball : public WorldEntity
{
private:
	// Physical characteristics (use superclass size as radius)
	int m_iSlices;
	int m_iStacks;

	// Rotation of the marker
	GLfloat m_fRotation;
	// Orientation of the ball itself.
	Quaternion m_qOrientation;

	bool m_bDying;

	// Particles for impact with the wall.
	ParticleGenerator m_oImpactParticles;

	// Axis about which to spin (set by the controller)
	Vector3f m_vecSpinAxis;
	Vector3f m_vecTotalSpin;

	// Quadric for the ball (needs freeing on destruction!)
	GLUquadric* m_pQuadric;

public:
	Ball(Vector3f vecPosition);
	~Ball() { gluDeleteQuadric(m_pQuadric); }

	void Render();

	void SetDying(bool bVal) { m_bDying = bVal; }
	bool GetDying() { return m_bDying; }
	Colour4f& GetColour() { return m_colColour; }
	Quaternion& GetOrientation() { return m_qOrientation; }
	// Fire the impact particles.
	//void FireParticles(const CollisionData& rData);
	void FireParticles(const CollisionData& rData);

	// Call the update function for particles (do this on every game update!)
	void UpdateParticles(int iTime) { m_oImpactParticles.Update(iTime); }
	// Set the spin axis whenever spin changes.
	void SetSpinAxis(const Vector3f& rVec) { m_vecSpinAxis = rVec; }
	// Add to the spin speed.
	void AddToSpin(const Vector3f& rVec) { m_vecTotalSpin += rVec; }

	// Set the rotation of the marker to match that of the game field.
	void SetMarkerRotation(GLfloat fRotation)
	{
		m_fRotation = fRotation;
	}

};

#endif /* BALL_H */
