#include "Core.h"

// Global stuff for dealing with GLUT.

// reshape: adjust the size of the viewing area to that of the new window size.
void reshape(int iWidth, int iHeight)
{
	const GameSettings& rSettings = GameSettings::Get();

	// Always keep the window the same size.
	glutReshapeWindow(rSettings.GetXResolution(), rSettings.GetYResolution());
}

// render(): refresh the contents of the window.
void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Render the game.
	glPushMatrix();
	pGame->Render();
	glPopMatrix();

	glutSwapBuffers();
	glFlush();
}

// update: the timer function, which calls the game update loop.
void update(int iVal)
{
	const GameSettings& rSettings = GameSettings::Get();
	int iTime = rSettings.GetTimerDelay();

	// Call the game's update function
	pGame->Update(iTime);

	glutTimerFunc(iTime, update, 0);
	glutPostRedisplay();
}

// keyDown: callback function for keyboard input.
void keyDown(unsigned char iKey, int iX, int iY)
{
	// If we're quitting..
	if (iKey == 27)
		exit(0);

	// Otherwise, send the key to the game.
	pGame->KeyDown(iKey);
}

// keyUp: callback function for keyboard input.
void keyUp(unsigned char iKey, int iX, int iY)
{
	// Send the key to the game.
	pGame->KeyUp(iKey);
}


// mouse functions
void mouse(int iButton, int iState, int iX, int iY)
{
	// TODO: Control firing with mouse
}

void activeMotion(int iX, int iY)
{
	pGame->ActiveMouseMotion(iX, iY);
}

void passiveMotion(int iX, int iY)
{
	pGame->PassiveMouseMotion(iX, iY);
}



// Main function.
int main(int argc, char **argv)
{
	// Set up the random number generator
	srand((unsigned int)time(0)); 
	// Get game settings
	const GameSettings& rSettings = GameSettings::Get();

	// Initialise GLUT and set up the window.
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(rSettings.GetXResolution(), rSettings.GetYResolution());
	glutCreateWindow(rSettings.GetWindowTitle());

	// Set up the game
	pGame = new FPGame();
	//pGame->Init();

	// Give GLUT our little functions that interact with the game itself.
	glutDisplayFunc(render);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyDown);
	glutKeyboardUpFunc(keyUp);
	glutMouseFunc(mouse);
	glutMotionFunc(activeMotion);
	glutPassiveMotionFunc(passiveMotion);

	//glutIdleFunc(idle);
	glutTimerFunc(GameSettings::Get().GetTimerDelay(), update, 0);

	glutMainLoop();
	return 0;
}