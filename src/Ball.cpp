#include "Ball.h"

#include "FPSettings.h"

/**
 * Constructor for a Ball object; just sets the various attributes.
 */
Ball::Ball(Vector3f vecPosition)
: WorldEntity(vecPosition, FPSettings::Get().GetBallRadius(), FPSettings::Get().GetBallColour())
, m_bDying(false)
, m_oImpactParticles(FPSettings::Get().GetImpactParticles(), new QuadP())
, m_iSlices(FPSettings::Get().GetBallSlices())
, m_iStacks(FPSettings::Get().GetBallStacks())
, m_fRotation(0) 
{ m_pQuadric = gluNewQuadric(); }


/** 
 * This "fires" the particle effect for ball collision with the walls; we derive the origin
 * point and motion direction of the ball from the normal of the surface being collided with.
 */
void Ball::FireParticles(const CollisionData& rData)
{ 
	assert(rData.m_vecNormal.Length() == 1);
	const FPSettings& rFPSettings = FPSettings::Get();

	// Reorient the particles to the wall being collided with.
	ParticleConfig& rConf = m_oImpactParticles.GetConfig();
	const ParticleConfig& rOrigConf = rFPSettings.GetImpactParticles();
	const Vector3f& rOrigVelMin = rOrigConf.m_vecStartVelMin;
	const Vector3f& rOrigVelMax = rOrigConf.m_vecStartVelMax;
	
	//cout << rData.m_vecPoint.GetString() << endl;

	Vector3f vecToWall = -(rData.m_vecNormal * m_fSize);
	rConf.m_vecOriginMin = vecToWall;
	rConf.m_vecOriginMax = vecToWall;
	rConf.m_vecStartVelMin = rData.m_vecNormal * rOrigVelMin.Length();
	rConf.m_vecStartVelMax = rData.m_vecNormal * rOrigVelMax.Length();

	m_oImpactParticles.FireOnce(); 
}

/**
 * The Render() function for Ball - this draws the actual ball in the game view, as part of the
 * render cycle linked to GLUT.
 */
void Ball::Render()
{
	const FPSettings& rFPSettings = FPSettings::Get();
     
	glPushMatrix();

	// Set up the material attributes
	
	glColor4fv(m_colColour.arrVals);

	// Border to show depth of ball clearly.
	// Draw a border highlighting the depth of the ball.
	GLfloat fBallDepth = *m_vecPos.z;
	GLfloat fGap = 0.1f;

	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glPushMatrix();
	glRotatef(m_fRotation, 0, 0, 1);

	glBegin(GL_LINES);

		glVertex3f(*rFPSettings.GetFTL().x+fGap, *rFPSettings.GetFTL().y-fGap, fBallDepth);
		glVertex3f(*rFPSettings.GetFTR().x-fGap, *rFPSettings.GetFTR().y-fGap, fBallDepth);
		
		glVertex3f(*rFPSettings.GetFTR().x-fGap, *rFPSettings.GetFTR().y-fGap, fBallDepth);
		glVertex3f(*rFPSettings.GetFBR().x-fGap, *rFPSettings.GetFBR().y+fGap, fBallDepth);
		
		glVertex3f(*rFPSettings.GetFBR().x-fGap, *rFPSettings.GetFBR().y+fGap, fBallDepth);
		glVertex3f(*rFPSettings.GetFBL().x+fGap, *rFPSettings.GetFBL().y+fGap, fBallDepth);
		
		glVertex3f(*rFPSettings.GetFBL().x+fGap, *rFPSettings.GetFBL().y+fGap, fBallDepth);
		glVertex3f(*rFPSettings.GetFTL().x+fGap, *rFPSettings.GetFTL().y-fGap, fBallDepth);

	glEnd();

	glPopMatrix();

	// Position of the ball.
	glTranslatef(*m_vecPos.x, *m_vecPos.y, *m_vecPos.z);
	// Particles (fired once on bounce).
	m_oImpactParticles.Render();

	glEnable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);

	
	glColor4fv(m_colColour.arrVals);


	// Material attributes for the ball	
	glMaterialfv(GL_FRONT, GL_AMBIENT, rFPSettings.GetBallAmbient().arrVals);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, rFPSettings.GetBallDiffuse().arrVals);
	glMaterialfv(GL_FRONT, GL_SPECULAR, rFPSettings.GetBallSpecular().arrVals);
	glMateriali(GL_FRONT, GL_SHININESS, rFPSettings.GetBallShininess());
	glMaterialfv(GL_FRONT, GL_EMISSION, rFPSettings.GetBallEmissive().arrVals);
	
	// Ball.
	
	//glPushAttrib(GL_NORMALIZE);	
	glTexGeni(GL_S,GL_TEXTURE_GEN_MODE,GL_SPHERE_MAP);
	glBindTexture(GL_TEXTURE_2D, FPTextureManager::Get().GetTexture(BALL));
	gluQuadricTexture(m_pQuadric, GL_TRUE);

	// Rotate by our orientation then draw the ball.
	glPushMatrix();
		Vector3f vecAxis;
		GLfloat fAngle;
		m_qOrientation.GetAxisAngle(&vecAxis, &fAngle);
		glRotatef(fAngle, *vecAxis.x, *vecAxis.y, *vecAxis.z);		
		gluSphere(m_pQuadric, m_fSize, m_iSlices, m_iStacks);
	glPopMatrix();
	
	//glPopAttrib() ;
	
	glPopMatrix();
}
