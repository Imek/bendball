/**
 * FPSettings.h: Declarations for the settings class that contains globally-accessible configuration
 * values for the Future Pong game.
 *
 * Author: Joe Forster
 */

#ifndef FPSETTINGS_H
#define FPSETTINGS_H

#include <iostream>
#include "Engine/GameSettings.h"
#include "Engine/3DUtil.h"

/**
 * Textures are managed here in the settings class, as they will need to be accessed from multiple
 * places (e.g. by textured particles if/when they are implemented). They could have been kept
 * within the Game class, but this would have lead to needing to pass around the IDs a lot via
 * constructors or methods. These are passed as integers to the 
 */
enum FP_TEXTURE { 
		DEF_FIELD1, DEF_FIELD2, DEF_FIELD3, DEF_FIELD4, DEF_FIELD5, DEF_FIELD6,
		DEF_METAL, BALL, WALL, WALL2, LAVA, PARTICLE
	};

/**
 * This singleton class implements TextureManager for our game. The main difference is that we have
 * a Get method to get the instance cast as an FPTextureManager, and that the constructor puts
 * together the textures. This code was originally put with FPGame and could also be 
 */
class FPTextureManager : public TextureManager
{
private:
	// Instance, constructor and destructor.
	FPTextureManager();
	~FPTextureManager() {}

public:
	// Get and possible initialise the instance of this config class.
	static const FPTextureManager& Get()
	{
		// Returns a blank generic config if it wasn't initialised.
		if (m_pInstance == NULL)
			m_pInstance = new FPTextureManager();
		return *((FPTextureManager*)m_pInstance);
	}

	// Init loads up all the textures. This should be called in the game's Init method or similar.
	void Init() { m_pInstance = new FPTextureManager(); }
	
};


// Physical attributes of a surface.
struct SurfaceConfig
{
	FP_TEXTURE m_iTexture;
	GLfloat m_fRestitution;
	GLfloat m_fFriction;
};

/**
 * The FPSettings is a globally-accessible singleton class, containing only information that's
 * needed by code throughout the project. A single instance is created on demand with settings
 * specified in the constructor, and no values can be changed externally.
 */
// XXX: This is pretty big.. Could it be split up?
// XXX: Having all these getter functions is a bit clunky. I'd rather have all the properties be
// public but const. Would need to change how this class is constructed though. Might be a good thing
// to do at the same time as splitting this into classes per entity etc.
class FPSettings
{
private:
	// Lighting settings
	LightConfig* m_pP1LavaLight;
	LightConfig* m_pP2LavaLight;
	LightConfig* m_pBallLight1;
	LightConfig* m_pBallLight2;

	// Deflector settings
	Vector3f m_vecDeflectorSize;
	GLfloat m_fDeflectorXStartPos;
	GLfloat m_fDeflectorYStartPos;
	GLfloat m_fDeflectorAccel;
	GLfloat m_fDeflectorSpeed;
	unsigned int m_iFieldAnimTime;

	Colour4f m_colDefAmb;
	Colour4f m_colDefDif;
	Colour4f m_colDefSpe;
	Colour4f m_colDefEmi;
	Colour4f m_colP1;
	Colour4f m_colP2;
	GLfloat m_fDefGlowRate;
	GLfloat m_fDefMaxGlow;

	// Ball settings
	Colour4f m_colBall;
	GLfloat m_fBallRadius;
	int m_iBallSlices;
	int m_iBallStacks;
	GLfloat m_fBallMaxSpeed;
	GLfloat m_fBallSinkSpeed;
	GLfloat m_fBallFireMult;
	int m_iBallRespawnDelay;
	Colour4f m_colBallAmb;
	Colour4f m_colBallDif;
	Colour4f m_colBallSpe;
	GLuint m_iBallShi;
	Colour4f m_colBallEmi;

	// Game field settings
	GLfloat m_fGameFieldLength;
	GLfloat m_fGameFieldWidth;
	GLfloat m_fGameFieldHeight;
	GLfloat m_fGameFieldDistFromCam;
	int m_iNoFieldDivisions;
	Colour4f m_colWallAmb;
	Colour4f m_colWallDif;
	Colour4f m_colWallSpe;
	Colour4f m_colWallEmi;
	Colour4f m_colLavaAmb;
	Colour4f m_colLavaDif;
	Colour4f m_colLavaSpe;
	Colour4f m_colLavaEmi; // Do lava light with a point light

	// Settings for shaders.
	GLfloat m_fWaveXFreq;
	GLfloat m_fWaveYFreq;
	GLfloat m_fWaveHeight;

	// How fast to rotate the game field.
	GLfloat m_fGameFieldRotateRate;

	// Keyboard controls
	int m_iStartKey;
	int m_iP1UpKey;
	int m_iP1LeftKey;
	int m_iP1DownKey;
	int m_iP1RightKey;
	int m_iP1BackKey;
	int m_iP1ForwardKey;
	int m_iP1FireKey;

	int m_iP2UpKey;
	int m_iP2LeftKey;
	int m_iP2DownKey;
	int m_iP2RightKey;
	int m_iP2BackKey;
	int m_iP2ForwardKey;
	int m_iP2FireKey;

	unsigned int m_iMaxScore;

	// Precalculated stuff for efficiency
	GLfloat m_fDeflectorMaxX;
	GLfloat m_fDeflectorMinX;
	GLfloat m_fDeflectorMaxY;
	GLfloat m_fDeflectorMinY;
	// Relative values?
	GLfloat m_fDeflectorMinZ;
	GLfloat m_fDeflectorMaxZ;

	GLfloat m_fBallMinX;
	GLfloat m_fBallMaxX;
	GLfloat m_fBallMinY;
	GLfloat m_fBallMaxY;
	GLfloat m_fBallMinZ;
	GLfloat m_fBallMaxZ;
	Vector3f m_vBallStartPos;
	GLfloat m_fBallCircum;
	
	// A vector for each corner of the game field...
	Vector3f m_vecFTL;
	Vector3f m_vecFTR;
	Vector3f m_vecFBR;
	Vector3f m_vecFBL;
	Vector3f m_vecBTL;
	Vector3f m_vecBTR;
	Vector3f m_vecBBR;
	Vector3f m_vecBBL;
	
	// Information for perspectives of players.
	Vector3f m_vecP1Eye;
	Vector3f m_vecP1Centre;
	Vector3f m_vecP1Up;
	Vector3f m_vecP2Eye;
	Vector3f m_vecP2Centre;
	Vector3f m_vecP2Up;

	// Configurations for the particle system.
	ParticleConfig m_oLavaParticles;
	ParticleConfig m_oEngineParticles;
	ParticleConfig m_oImpactParticles;

	// Shader configurations.
	ShaderConfig* m_pLavaShader;

	// Physics settings.
	Vector3f m_vecGravityAccel;
	
	GLfloat m_fBallInvMass;
	GLfloat m_fAttachedDamping;
	GLfloat m_fBallDamping;
	GLfloat m_fBallAngularDamping;

	// Surface properties
	SurfaceConfig m_oMetal1;
	SurfaceConfig m_oMetal2;

	GLfloat m_fServingSpringConst;
	GLfloat m_fServingZDist;
	GLfloat m_fGrabMaxDist;
	GLfloat m_fGrabMaxXYDist;
	GLfloat m_fGrabMaxSpeed;

	GLfloat m_fCurveFactor; // For the ball when spinning
	GLfloat m_fWallVelocity; // Of walls turning

	// Only ever need to keep one instance of FPSettings, and to pass around const references 
	// to it.
	static FPSettings* m_pInstance;
	FPSettings();
	// XXX: Should delete our pointers in here (e.g. lights)!
	// (though currently this is only destroyed on program quit anyway,
	// it's still good practice to clean up the managed memory here...)
	~FPSettings() {}

public:
	// Get the one istance of the singleton class.
	static const FPSettings& Get();	

	// XXX: Should use arrays/vectors rather than separate variables for these, 
	// particular balls (since we want to be able to vary their number).

	// Lighting settings
	const LightConfig& GetP1LavaLight() const { return *m_pP1LavaLight; }
	const LightConfig& GetP2LavaLight() const { return *m_pP2LavaLight; }
	const LightConfig& GetBallLight1() const { return *m_pBallLight1; }
	const LightConfig& GetBallLight2() const { return *m_pBallLight2; }

	// Deflector settings
	const Vector3f& GetDeflectorSize() const { return m_vecDeflectorSize; }
	GLfloat GetDeflectorXStartPos() const { return m_fDeflectorXStartPos; }
	GLfloat GetDeflectorYStartPos() const { return m_fDeflectorYStartPos; }
	GLfloat GetDeflectorAccel() const { return m_fDeflectorAccel; }
	GLfloat GetDeflectorSpeed() const { return m_fDeflectorSpeed; }
	unsigned int GetFieldAnimTime() const { return m_iFieldAnimTime; }

	// Lighting and visuals for deflectors
	const Colour4f& GetDefAmb() const { return m_colDefAmb; }
	const Colour4f& GetDefDif() const { return m_colDefDif; }
	const Colour4f& GetDefSpe() const { return m_colDefSpe; }
	const Colour4f& GetDefEmi() const { return m_colDefEmi; }
	const Colour4f& GetP1Colour() const { return m_colP1; }
	const Colour4f& GetP2Colour() const { return m_colP2; }
	GLfloat GetDefGlowRate() const { return m_fDefGlowRate; }
	GLfloat GetDefMaxGlow() const { return m_fDefMaxGlow; }

	// Ball settings
	const Colour4f& GetBallColour() const { return m_colBall; }
	GLfloat GetBallRadius() const { return m_fBallRadius; }
	int GetBallSlices() const { return m_iBallSlices; }
	int GetBallStacks() const { return m_iBallStacks; }
	GLfloat GetBallMaxSpeed() const { return m_fBallMaxSpeed; }
	GLfloat GetBallSinkSpeed() const { return m_fBallSinkSpeed; }
	GLfloat GetBallFireMult() const { return m_fBallFireMult; }
	int GetBallRespawnDelay() const { return m_iBallRespawnDelay; }

	const Colour4f& GetBallAmbient() const { return m_colBallAmb; }
	const Colour4f& GetBallDiffuse() const { return m_colBallDif; }
	const Colour4f& GetBallSpecular() const { return m_colBallSpe; }
	GLuint GetBallShininess() const { return m_iBallShi; }
	const Colour4f& GetBallEmissive() const { return m_colBallEmi; }

	// Game field settings
	GLfloat GetGameFieldLength() const { return m_fGameFieldLength; }
	GLfloat GetGameFieldWidth() const { return m_fGameFieldWidth; }
	GLfloat GetGameFieldHeight() const { return m_fGameFieldHeight; }
	GLfloat GetGameFieldDistFromCam() const { return m_fGameFieldDistFromCam; }
	int GetNoFieldDivisions() const { return m_iNoFieldDivisions; }
	const Colour4f& GetWallAmb() const { return m_colWallAmb; }
	const Colour4f& GetWallDif() const { return m_colWallDif; }
	const Colour4f& GetWallSpe() const { return m_colWallSpe; }
	const Colour4f& GetWallEmi() const { return m_colWallEmi; }
	const Colour4f& GetLavaAmb() const { return m_colLavaAmb; }
	const Colour4f& GetLavaDif() const { return m_colLavaDif; }
	const Colour4f& GetLavaSpe() const { return m_colLavaSpe; }
	const Colour4f& GetLavaEmi() const { return m_colLavaEmi; }
	
	// Shader settings for the lava.
	GLfloat GetWaveXFreq() const { return m_fWaveXFreq; }
	GLfloat GetWaveYFreq() const { return m_fWaveYFreq; }
	GLfloat GetWaveHeight() const { return m_fWaveHeight; }

	GLfloat GetGameFieldRotateRate() const { return m_fGameFieldRotateRate; }


	// Keyboard controls
	int GetStartKey() const { return m_iStartKey; }
	int GetP1UpKey() const { return m_iP1UpKey; }
	int GetP1LeftKey() const { return m_iP1LeftKey; }
	int GetP1DownKey() const { return m_iP1DownKey; }
	int GetP1RightKey() const { return m_iP1RightKey; }
	int GetP1BackKey() const { return m_iP1BackKey; }
	int GetP1ForwardKey() const { return m_iP1ForwardKey; }
	int GetP1FireKey() const { return m_iP1FireKey; }

	int GetP2UpKey() const { return m_iP2UpKey; }
	int GetP2LeftKey() const { return m_iP2LeftKey; }
	int GetP2DownKey() const { return m_iP2DownKey; }
	int GetP2RightKey() const { return m_iP2RightKey; }
	int GetP2BackKey() const { return m_iP2BackKey; }
	int GetP2ForwardKey() const { return m_iP2ForwardKey; }
	int GetP2FireKey() const { return m_iP2FireKey; }

	unsigned int GetMaxScore() const { return m_iMaxScore; }

	// Precalculated stuffs
	GLfloat GetDeflectorMaxX() const { return m_fDeflectorMaxX; }
	GLfloat GetDeflectorMinX() const { return m_fDeflectorMinX; }
	GLfloat GetDeflectorMaxY() const { return m_fDeflectorMaxY; }
	GLfloat GetDeflectorMinY() const { return m_fDeflectorMinY; }
	GLfloat GetDeflectorMinZ() const { return m_fDeflectorMinZ; }
	GLfloat GetDeflectorMaxZ() const { return m_fDeflectorMaxZ; }
	GLfloat GetBallMinX() const { return m_fBallMinX; }
	GLfloat GetBallMaxX() const { return m_fBallMaxX; }
	GLfloat GetBallMinY() const { return m_fBallMinY; }
	GLfloat GetBallMaxY() const { return m_fBallMaxY; }
	GLfloat GetBallMinZ() const { return m_fBallMinZ; }
	GLfloat GetBallMaxZ() const { return m_fBallMaxZ; }
	const Vector3f& GetBallStartPos() const { return m_vBallStartPos; }
	GLfloat GetBallCircumference() const { return m_fBallCircum; }

	const Vector3f& GetFTL() const { return m_vecFTL; }
	const Vector3f& GetFTR() const { return m_vecFTR; }
	const Vector3f& GetFBR() const { return m_vecFBR; }
	const Vector3f& GetFBL() const { return m_vecFBL; }
	const Vector3f& GetBTL() const { return m_vecBTL; }
	const Vector3f& GetBTR() const { return m_vecBTR; }
	const Vector3f& GetBBR() const { return m_vecBBR; }
	const Vector3f& GetBBL() const { return m_vecBBL; }

	const Vector3f& GetP1Eye() const { return m_vecP1Eye; }
	const Vector3f& GetP1Centre() const { return m_vecP1Centre; }
	const Vector3f& GetP1Up() const { return m_vecP1Up; }
	const Vector3f& GetP2Eye() const { return m_vecP2Eye; }
	const Vector3f& GetP2Centre() const { return m_vecP2Centre; }
	const Vector3f& GetP2Up() const { return m_vecP2Up; }

	// Configurations for the particle system.
	const ParticleConfig& GetLavaParticles() const { return m_oLavaParticles; }
	const ParticleConfig& GetEngineParticles() const { return m_oEngineParticles; }
	const ParticleConfig& GetImpactParticles() const { return m_oImpactParticles; }

	// Configurations for the shader system.
	const ShaderConfig& GetLavaShader() const { return *m_pLavaShader; }

	// Physics settings.
	const Vector3f& GetGravityAccel() const { return m_vecGravityAccel; }

	GLfloat GetBallInvMass() const { return m_fBallInvMass; }
	GLfloat GetAttachedDamping() const { return m_fAttachedDamping; }
	GLfloat GetBallDamping() const { return m_fBallDamping; }
	GLfloat GetBallAngularDamping() const { return m_fBallAngularDamping; }

	const SurfaceConfig& GetMetal1Config() const { return m_oMetal1; }
	const SurfaceConfig& GetMetal2Config() const { return m_oMetal2; }

	GLfloat GetServingSpringConst() const { return m_fServingSpringConst; }
	GLfloat GetServingZDist() const { return m_fServingZDist; }
	GLfloat GetGrabMaxDist() const { return m_fGrabMaxDist; }
	GLfloat GetGrabMaxXYDist() const { return m_fGrabMaxXYDist; }
	GLfloat GetGrabMaxSpeed() const { return m_fGrabMaxSpeed; }

	GLfloat GetCurveFactor() const { return m_fCurveFactor; } // For the ball when spinning
	GLfloat GetWallSpeed() const { return m_fWallVelocity; } // Of the turning walls (in a circle)

};

#endif /* FPSETTINGS_H */
