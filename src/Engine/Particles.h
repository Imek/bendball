/**
 * Particles.h: definitions for classes that comprise the particle system. This particle system is
 * highly flexible, allowing many different values to be set. Particles can fire once or be
 * continuous; the maximum number of particles and the generation rate can be adjusted; the
 * possible origin points and starting directions; the possible angle deviation from the starting
 * velocities; colours and colour fading can be used, as well as particle trails; and so on. It is
 * fairly straightforward to add further features as they are needed, but this header file is
 * intended to be kept reusable and separate from my game.
 */

#ifndef PARTICLES_H
#define PARTICLES_H

#include <list>
#include <map>

#include "GameSettings.h"
#include "Particles.h"
#include "Physics.h"
#include "3DUtil.h"
#include "WorldEntity.h"

using namespace std;

/**
 * A class for a basic point particle (only one instance needed for the whole engine!).
 */
struct PointP : public WorldEntity
{
	// Constructor passes references up the hierarchy
	PointP(const Vector3f& rPos, const GLfloat& rSize, const Colour4f& rColour)
	: WorldEntity(rPos, rSize, rColour) {}
	// Blank initial values (set before drawing!)
	PointP()
	: WorldEntity() {}
	
	// Just render it.
	void Render();

};

/**
 * Class for a basic quad particle; pretty much the same as a point particle, only that a quad of
 * the given size is drawn.
 */
struct QuadP : public WorldEntity
{
	// Constructor passes references up the hierarchy
	QuadP(const Vector3f& rPos, const GLfloat& rSize, const Colour4f& rColour)
	: WorldEntity(rPos, rSize, rColour) {}
	// Blank initial values (set before drawing!)
	QuadP()
	: WorldEntity() {}

	// To avoid overhead, we just pass in position and colour (as they'll be changing a lot).
	void Render();
};

/**
 * Yet another drawable particle entity class. This draws a sphere, with the size parameter
 * specifying its radius.
 */
struct SphereP : public WorldEntity
{
	// Constructor passes references up the hierarchy
	SphereP(const Vector3f& rPos, const GLfloat& rSize, const Colour4f& rColour)
	: WorldEntity(rPos, rSize, rColour) {}
	// Blank initial values (set before drawing!)
	SphereP()
	: WorldEntity() {}

	// To avoid overhead, we just pass in position and colour (as they'll be changing a lot).
	void Render();
};


class ParticleController
{
private:
	// How many milliseconds until this particle should become visible and start updating.
	GLint m_iDelayLeft;
	// How many milliseconds this particle has left to live.
	GLint m_iLifeSpan;
	// Physical characteristics.
	GLfloat m_fSize;
	Colour4f m_colColour;
	Colour4f m_colFadeRate;
	
	// The actual particle object.
	Particle m_oParticle;

	// Keep the config for when the particle needs resetting.
	const ParticleConfig* m_pConf;
	
public:
	// Constructor
	ParticleController(const ParticleConfig& rConf, GLuint iDelay = 0)
	: m_iDelayLeft(iDelay)
	, m_iLifeSpan(rConf.GetRandLifeSpan())
	, m_fSize(rConf.GetRandSize())
	, m_colColour(rConf.GetRandInitColour())
	, m_colFadeRate(rConf.GetRandColFade(m_colColour, m_iLifeSpan))
	, m_oParticle(rConf.GetRandOrigin(), rConf.GetRandVel(), rConf.GetRandAccel(),
				  rConf.GetRandInvMass(), rConf.GetRandDamping(), rConf.m_fMaxSpeed)
	, m_pConf(&rConf)
	{
	}

	// Reset this particle's propertiesto new starting values (i.e. like the constructor)
	void Reset(const ParticleConfig& rConf, GLuint iDelay = 0)
	{
		m_pConf = &rConf;
		m_iDelayLeft = iDelay;
		m_iLifeSpan = m_pConf->GetRandLifeSpan();
		m_fSize = m_pConf->GetRandSize();
		m_colColour = m_pConf->GetRandInitColour();
		m_colFadeRate = m_pConf->GetRandColFade(m_colColour, m_iLifeSpan);
		m_oParticle = Particle(
			m_pConf->GetRandOrigin(), m_pConf->GetRandVel(), m_pConf->GetRandAccel(),
			m_pConf->GetRandInvMass(), m_pConf->GetRandDamping(), rConf.m_fMaxSpeed);
	}

	// Should this particle be disposed of (i.e. moved to the free list) this update?
	bool Expired() const { return m_iLifeSpan <= 0; }

	// These must be implemented depending on the specific type of particle.
	void Update(int iTime)
	{
		// Do nothing with this particle if it's still waiting to be born.
		if (m_iDelayLeft > 0)
		{
			m_iDelayLeft -= iTime;
			return;
		}

		// Update all the varying values, then call Update on the particle.
		m_iLifeSpan -= iTime;
		m_colColour += m_colFadeRate * iTime;
		m_oParticle.Update(iTime);
	}

	// Get/Set methods
	GLfloat GetSize() const { return m_fSize; }
	const Colour4f& GetConstColour() const { return m_colColour; }
	const Particle& GetConstParticle() const { return m_oParticle; }

	void SetSize(GLfloat fSize) { m_fSize = fSize; }
	Colour4f& GetColour() { return m_colColour; }
	Particle& GetParticle() { return m_oParticle; }

	// Set the attributes (pos, size, colour) of a given WorldEntity to those in this particle.
	void SetEntityAtts(WorldEntity* pEntity)
	{
		pEntity->GetPosition().Set(m_oParticle.m_vecPos);
		pEntity->SetSize(m_fSize);
		pEntity->GetColour().Set(m_colColour);		
	}

};


/**
 * ParticleGenerator class: manages a set of particles according to the provided settings.
 * the given template type is the kind of entity that is generated as particles by this generator.
 * This is done so that it is easy to implement a new object class, or to adapt an existing
 * class representing a drawable object.
 */
class ParticleGenerator
{
private:

	// Contains all the pertinent settings for the generator and particles.
	ParticleConfig m_oPConf;

	// Here we have a list of particles in use (which should be drawn) and particles not in use
	// (the free list). Initially, m_lParticles is empty and m_lFree is full; all particles are
	// allocated in the constructor, so that the ParticleGenerator isn't messing around with
	// constantly freeing and reallocating memory whenever the Update function is called.
	list< ParticleController* > m_lParticles;
	list< ParticleController* > m_lFree;

	// Rate*time since the last particle was generated, in case the rate is lower than the update time.
	GLfloat m_fLeftOver;

	// The particle object we're dealing with. 
	WorldEntity* m_pParticleEntity;

	// Whether this generator is switched on or not. Existing particles will still be drawn,
	// but no new ones will be generated.
	bool m_bOn;
	// Fire all the particles once until we've reached the max, but don't recreate any.
	bool m_bFireOnce;

public:
	// Constructor. Note that pParticleEntity is memory managed within this class (bad idea?).
	ParticleGenerator(const ParticleConfig& rPConf, WorldEntity* pParticleEntity)
	: m_oPConf(rPConf) 
	, m_fLeftOver(0)
	, m_pParticleEntity(pParticleEntity)
	, m_bOn(false)
	, m_bFireOnce(false)
	{
		// Allocate the memory for all our particles.
		for (unsigned int i = 0; i < rPConf.m_iMaxParticles; ++i)
		{
			m_lFree.push_back(new ParticleController(rPConf, 0));
		}
	}

	// Destructor frees all of the memory assigned in the constructor.
	virtual ~ParticleGenerator() 
	{
		// Free it all...
		for (list< ParticleController* >::iterator it = m_lParticles.begin();
			 it != m_lParticles.end(); ++it)
		{
			delete *it;
		}
		for (list< ParticleController* >::iterator it = m_lFree.begin();
			 it != m_lFree.end(); ++it)
		{
			delete *it;
		}
		delete m_pParticleEntity;
	}

	// Update or render all the particles in m_lParticles.
	// In update, we may also need to create and/or destroy some particles.
	// This update function is called along with the regular update function its controller or
	// manager is attached to.
	void Update(int iTime);	

	// Draw all the particles. The form and purpose of this function follows that of a standard
	// drawable object, except that it encompasses all particles owned by this generator; it 
	// should be used as so, i.e. drawn on every render loop.
	void Render();

	// Get the particle config, possible to be altered. We may use the same config for multiple
	// generators and alter their origins or the orientation of their initial velocity or what have
	// you.
	ParticleConfig& GetConfig() { return m_oPConf; }

	// Switch this generator on or off.
	bool GetOn() const { return m_bOn; }
	void SetOn(bool bVal) { m_bOn = bVal; }
	// Fire all the particles once until we've reached the max, but don't recreate any.
	// This could have been made an actual setting of the generator instead, but this way seemed
	// simpler to implement. A possible extension of this functionality is to have it fire for a 
	// given period of time and then switch off once done.
	void FireOnce() { m_bOn = true; m_bFireOnce = true; }
};


#endif /* PARTICLES_H */
