#ifndef EVENT_H
#define EVENT_H

#include <iostream>
#include <list>

using namespace std;

// TODO: Game-specific events separated from this code
enum EVENT_TYPE { BALL_COLLISION_EVENT };

/**
 * This class represents an event triggered in the game control system.
 */
class Event
{
private:
	EVENT_TYPE m_iType;

public:
	Event(EVENT_TYPE iType) : m_iType(iType) {}
	virtual ~Event() {}

	// Get the type.
	EVENT_TYPE GetType() const { return m_iType; }

	// Get a string for debug purposes.
	virtual string GetString() const = 0;
};

/**
 * A class containing a queue of Events to be added and handled.
 */
class EventHandler
{
protected:
	list<Event*> m_lEvents;

public:
	
	EventHandler() {}
	
	virtual ~EventHandler()
	{
		for (list<Event*>::iterator it = m_lEvents.begin();
			 it != m_lEvents.end(); ++it)
		{
			delete *it;
		}
	}

	// Shove a new event onto the top (back) of the stack.
	void PushEvent(Event* pEvent)
	{
		m_lEvents.push_back(pEvent);
	}

	// Take the next event off the stack (back of list).
	Event* PopEvent()
	{
		if (m_lEvents.empty()) return NULL;

		Event* pEv = m_lEvents.back();
		m_lEvents.pop_back();
		return pEv;
	}

	void Clear()
	{
		for (list<Event*>::iterator it = m_lEvents.begin();
			 it != m_lEvents.end(); ++it)
		{
			delete *it;
		}
		m_lEvents.clear();
	}
	
	bool Empty() { return m_lEvents.empty(); }
};

#endif /* EVENT_H */
