#include "AbstractGame.h"

/**
 * AbstractGame code
 */

// "Print" some text to the screen using glutBitmapCharacter.
void AbstractGame::PrintText(GLfloat fX, GLfloat fY, GLfloat fZ, const std::string &rStr)
{
	const GameSettings& rSettings = GameSettings::Get();
	glRasterPos3f(fX, fY, fZ);
	for (string::const_iterator it = rStr.begin(); it != rStr.end(); ++it)
	{
		glutBitmapCharacter(rSettings.GetFont(), *it);
	}
	
}

/**
 * AbstractGame destructor: delete every WorldEntity and AbstractController associated with the
 * game. Any implementing class should be sure to put everything into m_vEntities and 
 * m_vControllers, so that they can be disposed of here.
 */
AbstractGame::~AbstractGame()
{
	for (vector<WorldEntity*>::iterator it = m_vEntities.begin();
		 it != m_vEntities.end(); it++)
	{
		delete *it;
	}
	for (vector<AbstractController*>::iterator it = m_vControllers.begin();
		 it != m_vControllers.end(); it++)
	{
		delete *it;
	}
}
