/**
 * GameSettings.h: declarations concerning the GameSettings singleton class, which contains all
 * of the pertinent game configuration values that may need to be accessed globally.
 *
 * Author: Joe Forster
 */

#ifndef GAMESETTINGS_H
#define GAMESETTINGS_H

#include <map>
#include <iostream>
#include <fstream>
#include "assert.h"
// GLEW incluces (for shaders)
#define GLEW_STATIC 1
#include "GL/glew.h"
// OpenGL includes
#include <GL/glut.h>
//#include <GL/gl.h>
//#include <GL/glu.h>
// Bitmap loader & utils
#include "Bitmap.h"
#include "3DUtil.h"


/**
 * LightConfig class: This simple class contains all the possible attributes of a light source
 * in the game. Values are passed through the constructor because it is not necessary to directly
 * observe or alter them after creation; only to turn the light on and off with the public member
 * classes. This could have been done without an extra class, but it seemed a good use of OO to
 * hide config details and put reusable code for doing lighting in the same place.
 */
// XXX: This config seems to also represent the light itself in the world.
// These two concepts should be sparated. e.g. for balls there should be a one
// light config and a light entity generated for each ball.
class LightConfig
{
private:
	GLenum m_eLight;
	Colour4f m_colAmbient;
	Colour4f m_colDiffuse;
	Colour4f m_colSpecular;
	Vector3f m_vecPosition;
	// Attenuation values
	GLfloat m_fConstAtt;
	GLfloat m_fLinearAtt;
	GLfloat m_fQuadAtt;


public:
	LightConfig(GLenum eLight, Colour4f colAmb, Colour4f colDif, Colour4f colSpe, Vector3f vecPos,
		GLfloat fConstAtt, GLfloat fLinearAtt, GLfloat fQuadAtt)
	: m_eLight(eLight), m_colAmbient(colAmb), m_colDiffuse(colDif), m_colSpecular(colSpe), m_vecPosition(vecPos)
	, m_fConstAtt(fConstAtt), m_fLinearAtt(fLinearAtt), m_fQuadAtt(fQuadAtt) {}

	// Switch the light on or off.
	void SwitchOn() const
	{
		// Get an array for this position
		GLfloat arrPosPlusOne[4] = { *m_vecPosition.x, *m_vecPosition.y, *m_vecPosition.z, 1.0f };

		glLightfv(m_eLight, GL_AMBIENT, m_colAmbient.arrVals);
		glLightfv(m_eLight, GL_DIFFUSE, m_colDiffuse.arrVals);
		glLightfv(m_eLight, GL_SPECULAR, m_colSpecular.arrVals);
		glLightfv(m_eLight, GL_POSITION, arrPosPlusOne);
		glLightf(m_eLight, GL_CONSTANT_ATTENUATION, m_fConstAtt);
		glLightf(m_eLight, GL_LINEAR_ATTENUATION, m_fLinearAtt);
		glLightf(m_eLight, GL_QUADRATIC_ATTENUATION, m_fQuadAtt);

		glEnable(m_eLight);
	}

	void SwitchOff() const
	{
		glDisable(m_eLight);
	}

	/**
	 * Sets the position of the light. As some light sources need to be re-used or re-drawn
	 * in different places relative to the camera(s), this method helps remove the necessity
	 * to make multiple redundant LightConfig instances.
	 */
	void SetPosition(Vector3f vecNewPos)
	{
		m_vecPosition = vecNewPos;
	}
};


/**
 * ParticleConfig class: keeps all the relevant information for a configuration of the#
 * ParticleGenerator class.
 */
struct ParticleConfig
{
	// Default start vectors for a particle, plus how much they could randomly vary.
	Vector3f m_vecOriginMin;
	Vector3f m_vecOriginMax;

	Vector3f m_vecStartVelMin;
	Vector3f m_vecStartVelMax;

	Vector3f m_vecAccelMin;
	Vector3f m_vecAccelMax;

	// Minimum and maximum angle for altering the starting direction.
	GLint m_iMinAngle;
	GLint m_iMaxAngle;
	// Lifespan of a particle, plus how much it could randomly vary.
	GLint m_iLifeSpanMin;
	GLint m_iLifeSpanMax;

	// Physical characteristics: inverse mass, damping and size.
	GLfloat m_fInvMassMin;
	GLfloat m_fInvMassMax;
	GLfloat m_fDampingMin;
	GLfloat m_fDampingMax;
	GLfloat m_fSizeMin;
	GLfloat m_fSizeMax;

	// If these are different, then the particles will fade from the start colour to the end one.
	Colour4f m_colStartMin;
	Colour4f m_colStartMax;

	Colour4f m_colEndMin;
	Colour4f m_colEndMax;

	// Maximum number of particles, and particle creation rate.
	GLuint m_iMaxParticles;
	GLfloat m_fParticleRate;

	// No. trailer particles
	GLuint m_iTrailNoMin;
	GLuint m_iTrailNoMax;
	// Delay in milliseconds between each particle in the trail.
	GLuint m_iTrailDelayMin;
	GLuint m_iTrailDelayMax;
	// What the size/colour  the trailing particles should fade towards.
	GLfloat m_fTrailSizeEndMin; 
	GLfloat m_fTrailSizeEndMax; 
	Colour4f m_colTrailColEndMin;
	Colour4f m_colTrailColEndMax;

	// Physics-related value; doesn't need randomising.
	GLfloat m_fMaxSpeed;


	// Random values for Particles and ParticleControllers
	Vector3f GetRandOrigin() const { return Vector3f::RandomBetween(m_vecOriginMin, m_vecOriginMax); }
	Vector3f GetRandVel() const 
	{
		// Randomise the starting velocity by position and angle
		GLint iRand1 = RandomBetween(0, 359);
		GLint iRand2 = RandomBetween(m_iMinAngle, m_iMaxAngle);
		if (rand() % 2 == 0) // 50% chance to make it negative
			iRand2 = -iRand2;
		Vector3f vecReturn = Vector3f::RandomBetween(m_vecStartVelMin, m_vecStartVelMax);		
		Vector3f vecCalc = vecReturn.GetNormal(); // m_vecCalc used as axis
		
		vecCalc.Rotate(vecReturn, iRand1);
		vecReturn.Rotate(vecCalc, iRand2);
		return vecReturn;
	}
	Vector3f GetRandAccel() const { return Vector3f::RandomBetween(m_vecAccelMin, m_vecAccelMax); }

	GLuint GetRandLifeSpan() const { return (GLuint)RandomBetween(m_iLifeSpanMin, m_iLifeSpanMax); }
	GLfloat GetRandInvMass() const { return RandomBetween(m_fInvMassMin, m_fInvMassMax); }
	GLfloat GetRandDamping() const { return RandomBetween(m_fDampingMin, m_fDampingMax); }
	GLfloat GetRandSize() const { return RandomBetween(m_fSizeMin, m_fSizeMax); }

	Colour4f GetRandInitColour() const { return Colour4f::RandomBetween(m_colStartMin, m_colStartMax); }
	Colour4f GetRandColFade(const Colour4f& rInitial, const GLuint& rLifeSpan) const 
	{ 
		return (Colour4f::RandomBetween(m_colEndMin, m_colEndMax) - rInitial) 
			   / rLifeSpan; 
	}

	// Random values for the ParticleGenerator
	// No. trailer particles
	GLuint GetRandTrailNo() const { return RandomBetween((GLint)m_iTrailNoMin, (GLint)m_iTrailNoMax); }
	GLuint GetRandTrailDelay() const { return RandomBetween((GLint)m_iTrailDelayMin, (GLint)m_iTrailDelayMax); }
	GLfloat GetRandTrailSizeEnd() const { return RandomBetween(m_fTrailSizeEndMin, m_fTrailSizeEndMax); }
	Colour4f GetRandTrailColEnd() const { return Colour4f::RandomBetween(m_colTrailColEndMin, m_colTrailColEndMax); }

};

/**
 * ShaderConfig: This works exactly like LightConfig; we provide a path for a vertex shader and a
 * path for a fragment shader, it does its stuff, and it can then be switched on and off at will.
 * Originally, each shader was hard-coded into the game's Init() function, but this is clearly a
 * better way in terms of expandability and reusability.
 */
class ShaderConfig
{
protected:
	// Read some text from a file (i.e. source code for a shader) by line into a string array.
	static GLcharARB* ReadShaderSrc(const char* szFilePath, 
									GLint* pLength);

	// The object by which we will refer to the shader.
	GLhandleARB m_iShaderObj;
	// Whether we've done the setting up for the shader system.
	static bool s_bInitialised;
	// Whether we can actually do shaders.
	static bool s_bSupported;

public:
	// Load up the shader stuff.
	ShaderConfig(const char *szVertPath, const char *szFragPath);

	// Start or stop using this shader in the GL state.
	void SwitchOn() const;
	void SwitchOff() const;

	// Set a Uniform value in the shader.
	void SetUniform1f(const char *szName, GLfloat fVal) const;

	// Initialise the shader system - common code for multiple different ShaderConfig instances.
	static void InitSystem();
};

/**
 * TextureManager abstract singleton: this class encapsulates a particular texture configuration
 * for the game. Here lies any common code needed between different subclasses, such as the 
 * ability to load GL textures from files. This uses the bitmap loader in Bitmap.h. It is intended
 * for Init to be called at the start of the program, so that all the textures can be loaded ready.
 * They can then be accessed globally without the need to pass around texture IDs all the time. 
 * This seemed to be the best way to handle the problem of loading textures, as the maximum amount 
 * of code can be re-used between games and only the game-specific code (i.e. loading of particular 
 * textures) need be kept in a subclass of TextureManager. One possible extension of this system,
 * which was planned but I didn't have time for, was a Texture class that would completely
 * encapsulate the ID numbers, allowing textures to be "switched" on and off like shaders and
 * lights.
 */
class TextureManager
{
protected:
	// All of our loaded texture names to be looked up.
	map<GLuint, GLuint> m_mTextures;

	// Instance, constructor and destructor.
	static TextureManager* m_pInstance;
	TextureManager() {}
	~TextureManager() {}

	// Load some textures, returning IDs in an array.
	static void LoadTextures(unsigned int iNo, char** pPaths, unsigned int* pNames);

public:
	// Abstract function - should just call the subclass constructor or whatever.
	// This is mostly here so that nobody can instantiate this base class.
	virtual void Init() = 0;
	
	// Get a texture ID by looking it up in the map.
	GLuint GetTexture(GLuint iEnumVal) const
	{
		map<GLuint, GLuint>::const_iterator itTex = m_mTextures.find(iEnumVal);

		if (itTex == m_mTextures.end())
		{
			cerr << "TextureManager::GetTexture was given an invalid value: " << iEnumVal << endl;
			exit(EXIT_FAILURE);
		}

		return itTex->second;
	}
};


/**
 * GameSettings class: This class works exactly like the FPSettings class, only that it contains
 * settings that could be relevant across different games. These were split so that more general
 * settings could be "packaged" with the abstract game classes for reuse. 
 */
class GameSettings
{
protected:
	// Delay between calls of the timer function
	unsigned int m_iTimerDelay;

	// Screen resolution
	unsigned int m_iXResolution;
	unsigned int m_iYResolution;

	// Window settings
	char* m_pWindowTitle;
	GLfloat m_fAspectRatio;

	// Font settings
	void* m_pFont;
	
	static GameSettings* m_pInstance;
	GameSettings();
	~GameSettings() {}

public:
	static const GameSettings& Get();	

	// Delay between calls of the timer function
	unsigned int GetTimerDelay() const { return m_iTimerDelay; }

	// Screen resolution
	unsigned int GetXResolution() const { return m_iXResolution; }
	unsigned int GetYResolution() const { return m_iYResolution; }

	// Window settings
	char* GetWindowTitle() const { return m_pWindowTitle; }
	GLfloat GetAspectRatio() const { return m_fAspectRatio; }

	// Font settings
	void* GetFont() const { return m_pFont; }
};

#endif /* GAMESETTINGS_H */
