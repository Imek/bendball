#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "WorldEntity.h"

// Enums
enum CONTROLLER_TYPE { MOUSE_CONTROLLER, KEYBOARD_CONTROLLER, PHYSICS_CONTROLLER };

/**
 * AbstactController abstract class: similar in purpose to the WorldEntity class, in that it
 * provides a base class for the various controllers that are needed by a game. A controller
 * is basically a class with two features: an Update function, linked in with the main update
 * loop of the game, and a WorldEntity being controlled. This could be control via keyboard
 * input, or possibly some kind of physical simulation.
 */
class AbstractController
{
protected:
	// What we're controlling
	WorldEntity* m_pControlled;
	// Type of controller?
	const CONTROLLER_TYPE m_iType;

public:
	// Constructor
	AbstractController(CONTROLLER_TYPE iType, WorldEntity* pControlled = NULL)
	: m_iType(iType)
	, m_pControlled(pControlled) {}
	// Destructor
	virtual ~AbstractController() {}

	CONTROLLER_TYPE GetType() const { return m_iType; }

	// Update function called by the game's update function.
	virtual void Update(int iTime) = 0;
};

#endif /* CONTROLLER_H */
