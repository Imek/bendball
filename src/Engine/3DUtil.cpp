#include "3DUtil.h"

// Static values for mathematical calculations.
const GLfloat Vector3f::PI = 3.14159265358979323846;
const GLfloat Vector3f::DEGREES_TO_RADIANS = PI/180;
const GLfloat Vector3f::RADIANS_TO_DEGREES = 180/PI;

/**
 * DrawQuadMesh: sometimes, instead of just drawing a quad, we want to draw a grid of quads.
 * An example of a use for this is for a wave effect vertex shader, or to make lighting appear
 * more realistic on a flat surface without having to make shaders for per-pixel lighting.
 * We provide the corners of the quad and the resolution both horizontally and vertically.
 * Generally we want to bind textures too, so this is done, though it may be better to pass in a
 * boolean value to say whether we want to perform these operations or not.
 */
void DrawQuadMesh(const Vector3f& rTL, const Vector3f& rTR,
				  const Vector3f& rBR, const Vector3f& rBL, const Vector3f& rN,
				  unsigned int iHRes, unsigned int iVRes)
{
	// Remember: coordinates are from bottom left, to make texture mapping easier.

	// Make a Vector3f for every vertex that we need.
	vector< vector<Vector3f> > vVertices (iVRes+1, vector<Vector3f>(iHRes+1));
	vector< vector<Vector3f> > vTexVerts (iVRes+1, vector<Vector3f>(iHRes+1));
	vVertices[0][0] = rBL;
	vTexVerts[0][0] = Vector3f(0.0f, 0.0f, 0.0f); // Not using the last value.

	Vector3f vecHInc = (rBR - rBL) / (GLfloat)iHRes;
	Vector3f vecVInc = (rTL - rBL) / (GLfloat)iVRes;
	Vector3f vecHTexInc = Vector3f(1.0f / (GLfloat)iHRes, 0.0f, 0.0f);
	Vector3f vecVTexInc = Vector3f(0.0f, 1.0f / (GLfloat)iVRes, 0.0f);


	// Do all the top vertices
	for (unsigned int iCol = 1; iCol <= iHRes; ++iCol)
	{
		vVertices[0][iCol] = vVertices[0][iCol-1] + vecHInc;
		vTexVerts[0][iCol] = vTexVerts[0][iCol-1] + vecHTexInc;
	}

	// Vector for the top left of each column and row
	for (unsigned int iRow = 1; iRow <= iVRes; ++iRow)
	{
		for (unsigned int iCol = 0; iCol <= iHRes; ++iCol)
		{
			vVertices[iRow][iCol] = vVertices[iRow-1][iCol] + vecVInc;
			vTexVerts[iRow][iCol] = vTexVerts[iRow-1][iCol] + vecVTexInc;
		}
	}

	// Draw all the quads.
	glNormal3fv(rN.arrVals);
	for (unsigned int iRow = 0; iRow < iVRes; ++iRow)
	{
		for (unsigned int iCol = 0; iCol < iHRes; ++iCol)
		{
			// Order: Bottom left, bottom right, top right, top left.
			const Vector3f& rBL = vVertices[iRow][iCol];
			const Vector3f& rTexBL = vTexVerts[iRow][iCol];
			const Vector3f& rBR = vVertices[iRow][iCol+1];
			const Vector3f& rTexBR = vTexVerts[iRow][iCol+1];
			const Vector3f& rTR = vVertices[iRow+1][iCol+1];
			const Vector3f& rTexTR = vTexVerts[iRow+1][iCol+1];
			const Vector3f& rTL = vVertices[iRow+1][iCol];
			const Vector3f& rTexTL = vTexVerts[iRow+1][iCol];

			glTexCoord2f(*rTexBL.x, *rTexBL.y); glVertex3fv(rBL.arrVals);
			glTexCoord2f(*rTexBR.x, *rTexBR.y); glVertex3fv(rBR.arrVals);
			glTexCoord2f(*rTexTR.x, *rTexTR.y); glVertex3fv(rTR.arrVals);
			glTexCoord2f(*rTexTL.x, *rTexTL.y); glVertex3fv(rTL.arrVals);

		}
	}
}


/**
 * Return a random vector with values between the two provided. This is used in the particle
 * system, to randomise the origin of a particle if required, but it is general enough to perhaps
 * be useful for other purposes.
 */
Vector3f Vector3f::RandomBetween(const Vector3f& rOne, const Vector3f& rTwo)	
{
	GLfloat fXVariance = *rTwo.x - *rOne.x;
	GLfloat fYVariance = *rTwo.y - *rOne.y;
	GLfloat fZVariance = *rTwo.z - *rOne.z;

	// Multiply random values between 0 and 1
	GLfloat fRandX = *rOne.x + 
		fXVariance * ((GLfloat)rand() / RAND_MAX);
	GLfloat fRandY = *rOne.y + 
		fYVariance * ((GLfloat)rand() / RAND_MAX);
	GLfloat fRandZ = *rOne.z + 
		fZVariance * ((GLfloat)rand() / RAND_MAX);

	return Vector3f (fRandX, fRandY, fRandZ);
}

/**
 * Get the mid point between two vectors.
 */
Vector3f Vector3f::MidPoint(const Vector3f& rOne, const Vector3f& rTwo)
{
	if (rOne == rTwo) return rOne;

	Vector3f vecOneToMid = (rTwo - rOne) / 2;
	return rOne + vecOneToMid;
}


/**
 * Return a random colour between two provided colours. The purpose and method behind this are
 * basically the same as the RandomBetween function for Vector3f; it is used in the particle
 * system to vary the possible initial colours of a particle.
 */
Colour4f Colour4f::RandomBetween(const Colour4f& rOne, const Colour4f& rTwo)
{
	if (rOne == rTwo) return rOne;

	GLfloat fRVariance = *rTwo.r - *rOne.r;
	GLfloat fGVariance = *rTwo.g - *rOne.g;
	GLfloat fBVariance = *rTwo.b - *rOne.b;
	GLfloat fAVariance = *rTwo.a - *rOne.a;

	// Multiply random values between 0 and 1
	GLfloat fRandR = *rOne.r + 
		fRVariance * ((GLfloat)rand() / RAND_MAX);
	GLfloat fRandG = *rOne.g + 
		fGVariance * ((GLfloat)rand() / RAND_MAX);
	GLfloat fRandB = *rOne.b + 
		fBVariance * ((GLfloat)rand() / RAND_MAX);
	GLfloat fRandA = *rOne.a + 
		fAVariance * ((GLfloat)rand() / RAND_MAX);

	return Colour4f (fRandR, fRandG, fRandB, fRandA);
}

/**
 * Get a random number between two integers. Again, this uses the C random number generator, so
 * if something better is to be used this function is to be changed.
 */
GLint RandomBetween(const GLint& rOne, const GLint& rTwo)
{
	if (rOne == rTwo) return rOne;

	return rOne + ( rand() % (rTwo - rOne) );
}

/**
 * Yet another random number function, this time for floating point numbers. There may be a better
 * and less naive way of generating a random float than this that takes into account the accuracy
 * of a float, but this seems to work fine for now.
 */
GLfloat RandomBetween(const GLfloat& rOne, const GLfloat& rTwo)
{
	if (rOne == rTwo) return rOne;

	GLfloat fRand = (GLfloat)rand() / RAND_MAX;
	return rOne + ( fRand * (rTwo - rOne) );
}
