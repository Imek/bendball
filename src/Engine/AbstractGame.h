#ifndef ABSTRACTGAME_H
#define ABSTRACTGAME_H

#include <iostream>
#include <vector>

#include "Event.h"
#include "WorldEntity.h"
#include "Controller.h"
#include "GameSettings.h"

using namespace std;

/**
 * This class forms the core class of a game, containing all the WorldEntities and Controllers.
 * it also specifies the bare minimum of member functions for a game to have, and possibly some
 * utility code that may not belong anywhere else. The main purpose of having an abstract game
 * class like this is so that any common code can be put in one place, but also so that we have
 * a specification of what functions exactly need to be implemented to make a properly designed
 * game glass.
 */
class AbstractGame
{
protected:
	// All entities in the game..
	vector<WorldEntity*> m_vEntities;
	// All entity controllers in the game..
	vector<AbstractController*> m_vControllers;

	// The event handler.
	EventHandler m_oEventHandler;
	
	// Draw some text. This could have possibly been put in 3DUtil.h, but as it is only used
	// by code in the game classes it seemed better to put it here,
	static void PrintText(GLfloat fX, GLfloat fY, GLfloat fZ, const string& rStr);

public:
	AbstractGame() {}
	virtual ~AbstractGame();

	// The following functions need to be implemented by any deriving Game class.
	// Initialise the environment for the game; generally involves enabling various OpenGL
	// properties, loading textures, shaders, etc.
	virtual void Init() = 0;
	// Render the game. This function is linked with GLUT's main render loop.
	virtual void Render() = 0;
	// Update the game. This function is linked with GLUT's timer function, and is intended to be
	// called every so many milliseconds (specified in the game settings).
	virtual void Update(int iTime) = 0;
	// These are callback functions linked with GLUT, for handling mouse and keyboard controls.
	virtual void ActiveMouseMotion(int iX, int iY) = 0;
	virtual void PassiveMouseMotion(int iX, int iY) = 0;
	virtual void KeyDown(int iKey) = 0;
	virtual void KeyUp(int iKey) = 0;

};

#endif /* ABSTRACTGAME_H */
