/**
 * Physics.h - the physics simulation system for the game engine.
 *
 * Author: Joe Forster (with some inspiration from Ian Millington's Cyclone engine)
 */

#ifndef PHYSICS_H
#define PHYSICS_H

#include "3DUtil.h"

/**
 * CollisionController - keep track of repeating collisions between two objects. Now this is just
 * used by the DefController class to prevent it from moving through the wall; the BallController
 * does something similar, but keeps track of this itself. This class is kind of semi-obsolete;
 * the project should be adapted so that we can remove it if time allows. (TODO)
 */
struct CollisionController
{
	// Contact normals of walls being collided with.
	Vector3f*			m_pContNormLastFrame;
	Vector3f*			m_pContNormThisFrame;
	vector<Vector3f>	m_vContactNormals; // For repeating collisions

	CollisionController()
	: m_pContNormLastFrame(NULL), m_pContNormThisFrame(NULL) {}

	bool StickingToSurfaceNorm(const Vector3f& rContNormal) const
	{
		for (vector<Vector3f>::const_iterator it = m_vContactNormals.begin();
			 it != m_vContactNormals.end(); ++it)
		{
			if (*it == rContNormal) return true;
		}
		return false;
	}

	void Update()
	{
		// Keep track of when we're repeatedly colliding.
		if (m_pContNormLastFrame)
		{
			delete m_pContNormLastFrame;
			m_pContNormLastFrame = NULL;
		}
		if (m_pContNormThisFrame)
		{
			delete m_pContNormLastFrame;
			m_pContNormLastFrame = m_pContNormThisFrame;
			m_pContNormThisFrame = NULL;
		}	
	}
};

/**
 * CollisionData - contains information on a collision for collision resolution.
 * This includes data for the resolution of velocity and penetration. The penetration
 * is a vector representing how far the first object (parameter) provided has moved
 * "inside" the second provided object.
 */
struct CollisionData
{
	Vector3f m_vecPoint;
	Vector3f m_vecNormal;
	Vector3f m_vecPenetration;
};


/**
 * AxisAlignedBoundingBox - this class represents a box aligned by the X, Y and Z
 * axes. These are useful for simple collision detection, as they are simple to implement
 * and more efficient to check against than arbitrarily-aligned bounding boxes.
 */
class AxisAlignedBoundingBox
{
private:
	// Minimum and maximum points for this box.
	Vector3f m_vecMin;
	Vector3f m_vecMax;
	// Calculated centre of the box.
	Vector3f m_vecCentre;
	Vector3f m_vecMaxFromCentre;

public:
	AxisAlignedBoundingBox(Vector3f vecMin, Vector3f vecMax)
	: m_vecMin(vecMin), m_vecMax(vecMax)
	{
		// Check validity (min < max)
		if (*m_vecMin.x > *m_vecMax.x ||
			*m_vecMin.y > *m_vecMax.y ||
			*m_vecMin.z > *m_vecMax.z)
		{
			cerr << "Invalid minimum vector given to AxisAlignedBoundingBox constructor." << endl;
			exit(EXIT_FAILURE);
		}

		m_vecCentre = Vector3f::MidPoint(m_vecMin, m_vecMax);
		m_vecMaxFromCentre = m_vecMax - m_vecCentre;
	}

	// Translate this bounding box by a given vector.
	void Translate(const Vector3f& rVec)
	{
		m_vecMin += rVec;
		m_vecMax += rVec;
		m_vecCentre += rVec;
	}

	// Check if the given point lies within this box.
	bool CheckPoint(const Vector3f& rVec) const
	{
		return (rVec.x >= m_vecMin.x && rVec.x <= m_vecMax.x &&
				rVec.y >= m_vecMin.y && rVec.y <= m_vecMax.y &&
				rVec.z >= m_vecMin.z && rVec.z <= m_vecMax.z);
	}

	// Check if a given AABB is overlapping this box.
	CollisionData* CheckAABB(const AxisAlignedBoundingBox& rBox) const;

	const Vector3f& GetCentre() const { return m_vecCentre; }
	const Vector3f& GetMaxFromCentre() const { return m_vecMaxFromCentre; }
	const Vector3f& GetMax() const { return m_vecMax; }
	const Vector3f& GetMin() const { return m_vecMin; }

};

/**
 * Particle class: this class represents a single point mass being physically simulated in the
 * game world. It is kept separate from the game itself or any notion of rendering anything on the
 * screen; the intention is for any code that uses this class to handle all that stuff. This code
 * is largely inspired by the class of the same name in Cyclone, though some extra behaviour has
 * been made to handle problems with collisions.
 */
struct Particle
{
	// Attributes of this particle.
	Vector3f			m_vecPos;
	Vector3f			m_vecVel;
	Vector3f			m_vecAccel;
	GLfloat				m_fInvMass;
	GLfloat				m_fDamping;	
	GLfloat				m_fMaxSpeed;

	vector<Vector3f>	m_vContactNormals;

	// Accumulated force since the last update.
	Vector3f m_vecForceAccum;

	// Empty constructor/destructor.
	Particle() { }
	Particle(Vector3f vecPos, Vector3f vecVel, Vector3f vecAccel, 
			 GLfloat fInvMass, GLfloat fDamping, GLfloat fMaxSpeed)
	: m_vecPos(vecPos)
	, m_vecVel(vecVel)
	, m_vecAccel(vecAccel)
	, m_fInvMass(fInvMass)
	, m_fDamping(fDamping)
	, m_fMaxSpeed(fMaxSpeed) {}
	virtual ~Particle() {}

	// Updates the new position of the particle, given iTime milliseconds have passed.
	void Update(int iTime);

	void AddForce(const Vector3f& rForce)
	{
		m_vecForceAccum += rForce;
	}

	void AddContactNormal(const Vector3f& rNormal)
	{
		m_vContactNormals.push_back(rNormal);	
	}

	GLfloat GetMass() const
	{
		assert(m_fInvMass >= 0);
		if (m_fInvMass == 0) return -1.0;
		else return (GLfloat)1/m_fInvMass;
	}

};


/**
 * Represents some force in the world, possibly acting on one or more particles.
 */
struct Force
{
	virtual void Update(Particle *pParticle, int iTime) = 0;
};


/**
 * Holds all the forces currently acting on some particle in the game world.
 * Note that we don't manage any memory here; we just hold the pointers.
 */
class ParticleForceManager
{
private:
	struct ParticleForce
	{
		Particle* m_pParticle;
		Force*	  m_pForce;

		/*ParticleForce(Particle* pParticle, Force* pForce)
		: m_pParticle(pParticle), m_pForce(pForce) {}
		virtual ~ParticleForce() { delete m_pParticle; delete m_pForce; }*/
	};
	vector<ParticleForce> m_vParticleForces;

public:
	// Add a particle/force pairing to this force manager; the force will be applied 
	// when Update is called.
	void Add(Particle* pParticle, Force *pForce)
	{
		ParticleForce oNewOne;		
		oNewOne.m_pParticle = pParticle;
		oNewOne.m_pForce = pForce;
		m_vParticleForces.push_back(oNewOne);
	}

	// Remove a particle/force pairing from the manager. Note that no memory is deleted here.
	void Remove(Particle* pParticle, Force *pForce)
	{
		for (vector<ParticleForce>::iterator it = m_vParticleForces.begin();
			 it != m_vParticleForces.end();)
		{
			if (it->m_pParticle == pParticle &&
				it->m_pForce == pForce)
			{
				it = m_vParticleForces.erase(it);
			}
			else
			{
				++it;
			}
		}
	}

	// Remove every particle/force pairing in this instance.
	void Clear()
	{
		m_vParticleForces.clear();
	}
	
	// Update all the forces managed on their corresponding particles.
	void Update(int iTime)
	{
		for (vector<ParticleForce>::iterator it = m_vParticleForces.begin();
			 it != m_vParticleForces.end(); ++it)
		{
			it->m_pForce->Update(it->m_pParticle, iTime);
		}
	}

	string GetString() const
	{
		stringstream sout;
		sout << m_vParticleForces.size();
		return sout.str();
	}

};

/**
 * Force class for gravity acting on a particle.
 */
class GravityForce : public Force
{
private:
	Vector3f m_vecGravity;

public:
	GravityForce(const Vector3f& rGravity) : m_vecGravity(rGravity) {}

	// Update the force: just add the acceleration to the particle.
	virtual void Update(Particle* pParticle, int iTime)
	{
		// Don't do anything if the particle has infinite mass.
		if (pParticle->m_fInvMass == 0) return;

		pParticle->AddForce(m_vecGravity * pParticle->GetMass());
	}

};

/**
 * Modified anchored spring force class for attaching the ball to the deflector by X and Y but
 * not Z. This is used for when the player is serving the ball or has "grabbed" the ball in play
 * using the fire command.
 */
class DeflectorSpringForce : public Force
{
private:
	const Vector3f*	m_pAnchor; // Position of deflector to anchor to
	float			m_fSpringConst;
	// The rest position is at a particular Z coordinate.
	GLfloat 		m_fRestZ; 
	// If the particle is further than this distance, the spring does nothing.
	// But if it is 0 or less, then the max distance is considered infinite.
	GLfloat			m_fMaxDist; 
	// Maximum distance in the X/Y
	GLfloat			m_fMaxXYDist;
	// The max speed the ball should be moving for the grabber to work, or 0 if no maximum speed.
	GLfloat			m_fMaxSpeed;

	// Which direction it should act (so that they can't grab the ball from behind).
	bool m_bP1;

public:
	DeflectorSpringForce(const Vector3f* pAnchor, float fSpringConst, GLfloat fRestZ, bool bP1, 
		GLfloat fMaxDist = 0, GLfloat fMaxXYDist = 0, GLfloat fMaxSpeed = 0)
	: m_pAnchor(pAnchor), m_fSpringConst(fSpringConst), m_fRestZ(fRestZ), m_bP1(bP1)
	, m_fMaxDist(fMaxDist), m_fMaxXYDist(fMaxXYDist), m_fMaxSpeed(fMaxSpeed) {}

	virtual void Update(Particle* pParticle, int iTime);

	// The force has a rest point on the Z axis, but follows its target on the X and Y axes.
	GLfloat GetRestZ() const { return m_fRestZ; }
	void SetRestZ(GLfloat fNewZ) { m_fRestZ = fNewZ; }
	void SetAnchor(const Vector3f* pNewAnchor) { m_pAnchor = pNewAnchor; }

	const Vector3f& GetAnchor() const { return *m_pAnchor; }
	// Whether the given point is in range of the spring, and moving slowly enough.
	bool InRange(const Vector3f& rPoint, GLfloat fSpeed) const
	{
		if (m_fMaxSpeed > 0 && fSpeed > m_fMaxSpeed)
			return false;

		if (m_fMaxDist <= 0) 
			return true;
		else
		{
			Vector3f vecAnchorToPoint = (rPoint - *m_pAnchor);
			// Return false if the X/Y component sum is greater than the maximum.
			if (m_fMaxXYDist > 0 && 
				sqrtf((*vecAnchorToPoint.x) * (*vecAnchorToPoint.x) + 
					  (*vecAnchorToPoint.y) + (*vecAnchorToPoint.y)) > m_fMaxXYDist)
				return false;

			// Only grab if the ball is in front of us, not behind.
			GLfloat fDist = vecAnchorToPoint.Length();
			if  (fDist > m_fMaxDist) 
				return false;
			else
			{
				Vector3f vecNormal = m_bP1 ?
					Vector3f(0, 0, -1) : Vector3f(0, 0, 1);
				return (vecAnchorToPoint.Dot(vecNormal) > 0);
			}
	
		}	
	}

	void SetIsP1(bool bVal) { m_bP1 = bVal; }

};


/**
 * A collision involving one particle and possibly another. Calculates the resulting velocity
 * of each particle; also resolves penetration.
 */
class ParticleCollision
{
public:
	Particle*	m_pOne;
	Particle*	m_pTwo;
	float		m_fRestitution;
	Vector3f	m_vecContNormal;
	Vector3f	m_vecPenetration;

	ParticleCollision(Particle* pOne, Particle* pTwo, GLfloat fRestitution, const CollisionData& rData)
	: m_pOne(pOne), m_pTwo(pTwo), m_fRestitution(fRestitution)
	, m_vecContNormal(rData.m_vecNormal), m_vecPenetration(rData.m_vecPenetration)
	{
		m_vecContNormal.Normalise();
		//assert(m_vecContNormal.Length() == 1);
	}

	// Resolve the collision, altering the attributes of the particle(s) accordingly.
	void Resolve(int iTime)
	{
		ResolveVelocity(iTime);
		ResolvePenetration(iTime);
	}

private:
	float CalcSeparatingVelocity() const
	{
		Vector3f vecRelVel = m_pOne->m_vecVel;
		if (m_pTwo) vecRelVel -= m_pTwo->m_vecVel;
		return vecRelVel * m_vecContNormal;
	}

	// Resolve velocity: calculate a new velocity for the particle(s) given the attributes of this
	// collision.
	void ResolveVelocity(int iTime)
	{
		// Resolve the velocity of the first particle.

		GLfloat fSepVel = CalcSeparatingVelocity();

		// The particles are moving apart from each other??
		if (fSepVel > 0) return;

		GLfloat fNewSepVel = -fSepVel * m_fRestitution;

		Vector3f vecCausedVel = m_pOne->m_vecAccel;
		if (m_pTwo) vecCausedVel -= m_pTwo->m_vecAccel;
		GLfloat fCausedSepVel = vecCausedVel * m_vecContNormal * iTime;

		if (fCausedSepVel < 0) 
		{
			fNewSepVel += m_fRestitution * fCausedSepVel;
			if (fNewSepVel < 0) fNewSepVel = 0;
		}

		GLfloat fDeltaVel = fNewSepVel - fSepVel;

		GLfloat fTotalInvMass = m_pOne->m_fInvMass;
		if (m_pTwo) fTotalInvMass += m_pTwo->m_fInvMass;

		// All particles have infinite pass, so we can't do much.
		if (fTotalInvMass <= 0) return;

		// Calculate the impulse to apply
		GLfloat fImpulse = fDeltaVel / fTotalInvMass;
		Vector3f vecImpulsePerInvMass = m_vecContNormal * fImpulse;

		//cout << "vel before: " << m_pOne->m_vecVel.GetString() << endl;
		// Apply impulses: they are applied in the direction of the contact,
		// and are proportional to the inverse mass.
		m_pOne->m_vecVel.Set(m_pOne->m_vecVel +
			vecImpulsePerInvMass * m_pOne->m_fInvMass
			);		
		//cout << "vel after: " << m_pOne->m_vecVel.GetString() << endl << endl;
		    
		if (m_pTwo)
		{
			// The second particle is pretty much the same, except in the opposite direction.
			m_pTwo->m_vecVel.Set(m_pTwo->m_vecVel +
				vecImpulsePerInvMass * -m_pTwo->m_fInvMass
				);
		}

	}

	// Resolve penetration: the CollisionData object contains a vector describing how fat the 
	// first object is penetrating into the second; we need to alter its position to avoid this.
	// For our purposes, it should be sufficient just to move the first particle all the way.
	void ResolvePenetration(int iTime)
	{
		m_pOne->m_vecPos -= m_vecPenetration;	
	}

};


/**
 * Static code for fine-grain collision detection between various basic primitives.
 * We return either a pointer to a ContactData object if a collision was detected, or a null
 * pointer if not.
 */
struct CollisionDetect
{
	// Collisions for spheres.
	static CollisionData* SphereAndSphere(const Sphere& rOne, const Sphere& rTwo);
	static CollisionData* SphereAndPlane(const Sphere& rSphere, const Plane& rPlane);
	static CollisionData* SphereAndQuad(const Sphere& rSphere, const Quad& rQuad);
	static CollisionData* SphereAndAABB(const Sphere& rSphere, const AxisAlignedBoundingBox& rBox);

	static CollisionData* AABBAndQuad(const AxisAlignedBoundingBox& rBox, const Quad& rQuad);
};


#endif /* PHYSICS_H */