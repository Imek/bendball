#ifndef WORLDENTITY_H
#define WORLDENTITY_H

#include "3DUtil.h"

/**
 * WorldEntity abstract class: this class represents any object in the AbstractGame that can be
 * drawn to the screen. Generally, these objects have positions. This is another use of the
 * inheritance system in OO for separating the reusable code from the game-specific code; any
 * functionality that could be reused between game projects could be put here. Of course this
 * isn't technically necessary, but it seems to be the most sensible way to do it.
 */
class WorldEntity
{
protected:
	// A point vector representing this entity's position
	Vector3f m_vecPos;
	// Generic values for overall size and colour, depending on what exactly the thing is.
	GLfloat m_fSize;
	Colour4f m_colColour;

public:
	// Constructor from a vector.
	WorldEntity(const Vector3f& rPos, const GLfloat& rSize, const Colour4f& rColour)
	: m_vecPos(rPos)
	, m_fSize(rSize)
	, m_colColour(rColour) {}
	// Constructor from zeroes (expected to set the values before drawing!)
	WorldEntity()
	: m_vecPos()
	, m_fSize(1.0f)
	, m_colColour() {}
	// Destructor
	virtual ~WorldEntity() {}


	// Getters/setters
	const Vector3f& GetConstPosition() const { return m_vecPos; }
	GLfloat GetSize() const { return m_fSize; }
	const Colour4f& GetConstColour() const { return m_colColour; }

	Vector3f& GetPosition() { return m_vecPos; }
	void SetSize(GLfloat fNewSize) { m_fSize = fNewSize; }
	Colour4f& GetColour() { return m_colColour; }	

	// Abstract functions that need implementing.
	virtual void Render() = 0;
};

#endif /* WORLDENTITY_H */
