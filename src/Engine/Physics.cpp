/**
 * Physics.cpp - implementations for Physics.h (mostly collision detection stuff).
 */

#include "Physics.h"

/**
 * Particle code
 */

/**
 * AxisAlignedBoundingBox code
 */
CollisionData* AxisAlignedBoundingBox::CheckAABB(const AxisAlignedBoundingBox& rBox) const
{
	// Vector from this box's centre to that box's centre.
	Vector3f vecBetween = rBox.m_vecCentre - m_vecCentre;

	if	 (*m_vecMin.x <= *rBox.m_vecMax.x && *m_vecMax.x >= *rBox.m_vecMin.x && 
		  *m_vecMin.y <= *rBox.m_vecMax.y && *m_vecMax.y >= *rBox.m_vecMin.y && 
		  *m_vecMin.z <= *rBox.m_vecMax.z && *m_vecMax.z >= *rBox.m_vecMin.z)
	{			
		// Work out which direction the contact normal should be by deciding what surface
		// it's most likely to have hit.
		Vector3f vecNormal;
		Vector3f vecCentreToContact;
		Vector3f vecContactToOtherCentre;
		GLfloat fXRatio = *vecBetween.x / *m_vecMaxFromCentre.x;
		GLfloat fYRatio = *vecBetween.y / *m_vecMaxFromCentre.y;
		GLfloat fZRatio = *vecBetween.z / *m_vecMaxFromCentre.z;
		GLfloat fXRatioAbs = abs(fXRatio);
		GLfloat fYRatioAbs = abs(fYRatio);
		GLfloat fZRatioAbs = abs(fZRatio);

		// Work out which side of the box was hit.
		// TODO: Pretty sure there's a more elegant way to do this mathematically.
		if (fXRatioAbs >= fYRatioAbs && fXRatioAbs >= fZRatioAbs)
		{
			if (fXRatio >= 0)
			{
				vecNormal.Set(1, 0, 0);
				vecCentreToContact.Set(*m_vecMaxFromCentre.x, 0, 0);
				vecContactToOtherCentre.Set(*rBox.m_vecMaxFromCentre.x, 0, 0);
			}
			else
			{				
				vecNormal.Set(-1, 0, 0);
				vecCentreToContact.Set(-*m_vecMaxFromCentre.x, 0, 0);		
				vecContactToOtherCentre.Set(-*rBox.m_vecMaxFromCentre.x, 0, 0);	
			}	
		}
		else if (fYRatioAbs >= fXRatioAbs && fYRatioAbs >= fZRatioAbs)
		{
			if (fYRatio >= 0)
			{
				vecNormal.Set(0, 1, 0);
				vecCentreToContact.Set(0, *m_vecMaxFromCentre.y, 0);
				vecContactToOtherCentre.Set(0, *rBox.m_vecMaxFromCentre.y, 0);
			}
			else
			{
				vecNormal.Set(0, -1, 0);
				vecCentreToContact.Set(0, -*m_vecMaxFromCentre.y, 0);
				vecContactToOtherCentre.Set(0, -*rBox.m_vecMaxFromCentre.y, 0);
			}
		}
		else if (fZRatioAbs >= fXRatioAbs && fZRatioAbs >= fYRatioAbs)
		{
			if (fZRatio >= 0)
			{
				vecNormal.Set(0, 0, 1);
				vecCentreToContact.Set(0, 0, *m_vecMaxFromCentre.z);
				vecContactToOtherCentre.Set(0, 0, *rBox.m_vecMaxFromCentre.z);
			}
			else
			{
				vecNormal.Set(0, 0, -1);
				vecCentreToContact.Set(0, 0, -*m_vecMaxFromCentre.z);
				vecContactToOtherCentre.Set(0, 0, -*rBox.m_vecMaxFromCentre.z);
			}
		}
		else
		{
			cerr << "Logic error in AABB collision data: couldn't work out a contact normal." << endl;
			exit(EXIT_FAILURE);
		}

		// Construct the data object with the point and normal.
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = vecNormal;
		pData->m_vecPoint = m_vecCentre + vecCentreToContact;

		// Penetration - how much further the box has moved into this one beyond contact.
		GLfloat fHitDist = (vecCentreToContact + vecContactToOtherCentre).Length();
		GLfloat fActualDist = vecBetween.Dot(vecNormal);

		if (fHitDist < fActualDist)
		{
			fHitDist = fActualDist;
		}
		pData->m_vecPenetration = -(vecNormal * (fHitDist-fActualDist));
		
	
		return pData;
		
	}
	else // Otherwise, no hit.
	{
		return NULL;
	}
}


/**
 * DeflectorSpringForce code
 */
void DeflectorSpringForce::Update(Particle *pParticle, int iTime)
{
	// Check against the maximum distance, if we've got it set.
	if (!InRange(pParticle->m_vecPos, pParticle->m_vecVel.Length()))
		return;
	
	Vector3f vecTarget (*m_pAnchor);
	*vecTarget.z = m_fRestZ;
	//cout << "rest z: " << m_fRestZ << " target: " << vecTarget.GetString() << endl;

	Vector3f vecForce = vecTarget - pParticle->m_vecPos;

	GLfloat fMagnitude = vecForce.Length();
	fMagnitude = abs(fMagnitude);
	fMagnitude *= m_fSpringConst;
	
	vecForce.Normalise();
	vecForce *= fMagnitude;
	pParticle->AddForce(vecForce);

}



/**
 * Particle code
 */

/**
 * Update function for a particle: calculate the new acceleration, velocity and position after the
 * given amount of time.
 */
void Particle::Update(int iTime)
{
	assert(iTime > 0);

	// Add any accumulated force to our acceleration, then clear the accumulator.
	Vector3f vecResultingAccel = m_vecAccel;
	vecResultingAccel += m_vecForceAccum * m_fInvMass;
	

	// We do the integration here the more simple/efficient way.
	m_vecVel +=  vecResultingAccel * iTime;

	//if (m_vecForceAccum.Length() != 0) cout << m_vecVel.Length() << " > " << m_fMaxSpeed <<  "?" << endl;
	if (m_vecVel.Length() > m_fMaxSpeed)
		m_vecVel = (m_vecVel.GetUnit() * m_fMaxSpeed);
	
	// If we have a contact normal, we're touching a surface. Don't move further in that direction.
	// NOTE: There are issues with this. Needs debugging & testing before bringing back.
	/*if (!m_vContactNormals.empty())
	{
		//cout << "size: " << m_vContactNormals.size() << endl;
		for (vector<Vector3f>::iterator it = m_vContactNormals.begin();
			 it != m_vContactNormals.end(); ++it)
		{
			Vector3f& rNormal = *it;
			assert(rNormal.Length() == 1);

			//cout << "direction: " << v.GetString() << ", normal: " << rNormal.GetString() << endl;

			GLfloat fNormComp = m_vecVel.Dot(-rNormal);
			//cout << "amount going through wall: " << fNormComp << endl;
			if (fNormComp > 0) m_vecVel += rNormal * fNormComp;

		}
	}*/

	// Apply damping.	
	m_vecVel *= pow(m_fDamping, iTime);
	// Update the position with the averaged motion vector.
	m_vecPos += m_vecVel * iTime;

	// Clear the collision normals and force accumulator.
	m_vContactNormals.clear();
	//m_oCollisionCont.Update();
	m_vecForceAccum.Zero();
}


/**
 * CollisionDetect code
 */

// Detecting collisions between spheres is trivial.
CollisionData* CollisionDetect::SphereAndSphere(const Sphere &rOne, const Sphere &rTwo)
{
	Vector3f vecTwoToOne = (rOne.m_vecPos - rTwo.m_vecPos);
	GLfloat fDistSq = vecTwoToOne.LengthSquared();
	GLfloat fRadSum = rOne.m_fRadius + rTwo.m_fRadius;
	if (fDistSq > fRadSum*fRadSum)
	{
		return NULL; // No hit
	}
	else
	{
		// Hit, so make the data object and return it.
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = vecTwoToOne.GetUnit();
		pData->m_vecPoint = rTwo.m_vecPos + pData->m_vecNormal*rTwo.m_fRadius;
		// How much further sphere 1 has passed into sphere 2 than it should have.
		GLfloat fPenetration = fRadSum - sqrtf(fDistSq);
		assert(fPenetration >= 0);
		pData->m_vecPenetration = vecTwoToOne.GetUnit() * -fPenetration;
		return pData;
	}
}

// Detecting collisions between a sphere and a plane is pretty easy.
CollisionData* CollisionDetect::SphereAndPlane(const Sphere &rSphere, const Plane &rPlane)
{
	assert(rPlane.N.Length() == 1);
	GLfloat fDist = rPlane.GetDist(rSphere.m_vecPos);
	if (fDist > rSphere.m_fRadius)
	{
		return NULL; // No hit
	}
	else
	{
		// Hit, so make the data object and return it.
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = rPlane.N;
		pData->m_vecPoint = rSphere.m_vecPos - (rPlane.N * fDist);
		GLfloat fPenetration = rSphere.m_fRadius - fDist;
		assert(fPenetration >= 0);
		pData->m_vecPenetration = rPlane.N * -fPenetration;
		return pData;
	}
}

// This involves first checking against the quad's plane, then finding the contact point 
// and seeing if that is within the 2D coordinates of the quad.
CollisionData* CollisionDetect::SphereAndQuad(const Sphere& rSphere, const Quad& rQuad)
{
	// First, check against the plane.	
	Plane p (rQuad.N, rQuad.TL);
	GLfloat fPlDist = abs(p.GetDist(rSphere.m_vecPos));
	if (fPlDist > rSphere.m_fRadius) return NULL;

	// Second, get the point of impact and check if that's within the face.
	assert(rQuad.N.Length() == 1);
	Vector3f vecContactPoint = rSphere.m_vecPos - rQuad.N*fPlDist;

	// Normal vectors for each plane we're checking against - pointing inwards from each edge.
	//cout << "Normal: " << rQuad.N.GetString() << endl;
	Vector3f TN = (rQuad.TR - rQuad.TL).Cross(rQuad.N);
	Vector3f BN = (rQuad.BL - rQuad.BR).Cross(rQuad.N);
	Vector3f LN = (rQuad.TL - rQuad.BL).Cross(rQuad.N);
	Vector3f RN = (rQuad.BR - rQuad.TR).Cross(rQuad.N);
	// Planes to check against - if the point's positive for all of them, then it's within the face.
	Plane T (TN, rQuad.TL);
	Plane B (BN, rQuad.BR);
	Plane L (LN, rQuad.BL);
	Plane R (RN, rQuad.TR);

	// See if it's within the quad, while at the same time finding the closest plane and its distance
	// in case it's not within the quad but still hitting it.
	GLfloat fSmallest = T.GetDist(rSphere.m_vecPos);
	Plane& rClosest = T;

	GLfloat fBDist = B.GetDist(rSphere.m_vecPos);
	if (fSmallest > fBDist) 
	{
		fSmallest = fBDist;
		rClosest = B;
	}

	GLfloat fLDist = L.GetDist(rSphere.m_vecPos);
	if (fSmallest > fLDist) 
	{
		fSmallest = fLDist;
		rClosest = L;
	}

	GLfloat fRDist = R.GetDist(rSphere.m_vecPos);
	if (fSmallest > fRDist) 
	{
		fSmallest = fRDist;
		rClosest = R;
	}

	if (fSmallest > 0) 
	{
		// Landed within the quad - return impact point on plane.
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = p.N;
		pData->m_vecPoint = vecContactPoint;
		GLfloat fPenetration = rSphere.m_fRadius - fPlDist;
		assert(fPenetration >= 0);
		pData->m_vecPenetration = p.N * -fPenetration;
		return pData;
	}

	// Didn't, but could still be hitting the edge.
	Vector3f vecDown = vecContactPoint - rSphere.m_vecPos;
	Vector3f vecIn = rClosest.N * fSmallest;
	Vector3f vecToEdge = vecDown + vecIn;
	if (vecToEdge.Length() > rSphere.m_fRadius)
	{
		return NULL; // No hit.
	}
	else
	{
		// Hit with the edge.
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = -vecToEdge.GetUnit();
		pData->m_vecPoint = rSphere.m_vecPos + vecToEdge;
		GLfloat fPenetration = rSphere.m_fRadius - vecToEdge.Length();
		assert(fPenetration >= 0);
		pData->m_vecPenetration = vecToEdge.GetUnit() * fPenetration;
		return pData;
	}
}

CollisionData* CollisionDetect::SphereAndAABB(const Sphere& rSphere, const AxisAlignedBoundingBox& rBox)
{	
	const GLfloat& rRadius = rSphere.m_fRadius;
	const Vector3f& rSphereC = rSphere.m_vecPos;
	
	// First, check against an AABB for the sphere. Only continue if they overlap.
	Vector3f vecSphereMin (*rSphereC.x - rRadius, 
						   *rSphereC.y - rRadius, 
						   *rSphereC.z - rRadius);
	Vector3f vecSphereMax (*rSphereC.x + rRadius, 
						   *rSphereC.y + rRadius, 
						   *rSphereC.z + rRadius);
	AxisAlignedBoundingBox oSphereBox (vecSphereMin, vecSphereMax);

	// Simple if it's hitting the AABB - return data for hitting a flat side.
	CollisionData* pBoxHit = rBox.CheckAABB(oSphereBox);
	if (pBoxHit) return pBoxHit;

	// Check if it's touching at a corner, by finding the closest one and checking their distance.
	// Note that as the boxes are axis-aligned, if we haven't already returned they can only overlap
	// at the corners.
	Vector3f* pClosest = NULL;
	const Vector3f& rMax = rBox.GetMax();
	const Vector3f& rMin = rBox.GetMin();
	const Vector3f& rBoxC = rBox.GetCentre();
	if (*rSphereC.x > *rBoxC.x)
	{
		if (*rSphereC.y > *rBoxC.y)
		{
			if  (*rSphereC.z > *rBoxC.z)
			{
				pClosest = new Vector3f(rMax);
			}
			else // (*rSphereC.z <= *rBoxC.z)
			{
				pClosest = new Vector3f(*rMax.x, *rMax.y, *rMin.z);
			}
		}
		else // (*rSphereC.y <= *rBoxC.y)
		{
			if  (*rSphereC.z > *rBoxC.z)
			{
				pClosest = new Vector3f(*rMax.x, *rMin.y, *rMax.z);
			}
			else // (*rSphereC.z <= *rBoxC.z)
			{
				pClosest = new Vector3f(*rMax.x, *rMin.y, *rMin.z);
			}
		}
	}
	else // (*rSphereC.x <= *rBoxC.x)
	{
		if (*rSphereC.y > *rBoxC.y)
		{
			if  (*rSphereC.z > *rBoxC.z)
			{
				pClosest = new Vector3f(*rMin.x, *rMax.y, *rMax.z);
			}
			else // (*rSphereC.z <= *rBoxC.z)
			{
				pClosest = new Vector3f(*rMin.x, *rMax.y, *rMin.z);
			}
		}
		else // (*rSphereC.y <= *rBoxC.y)
		{
			if  (*rSphereC.z > *rBoxC.z)
			{
				pClosest = new Vector3f(*rMin.x, *rMin.y, *rMax.z);
			}
			else // (*rSphereC.z <= *rBoxC.z)
			{
				pClosest = new Vector3f(rMin);
			}
		}
	}
	assert(pClosest != NULL);

	Vector3f vecCornerToCentre = rSphereC - *pClosest;
	GLfloat fDistSq = vecCornerToCentre.LengthSquared();
	if (fDistSq > rRadius * rRadius)
	{
		return NULL;
	}
	else
	{
		//cout << "hit edge!" << endl;
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = vecCornerToCentre.GetUnit();
		pData->m_vecPoint = *pClosest;
		GLfloat fPenetration = sqrtf(fDistSq) - rRadius;
		assert(fPenetration >= 0);
		pData->m_vecPenetration = pData->m_vecNormal * -fPenetration;

		return pData;
	}
}

CollisionData* CollisionDetect::AABBAndQuad(const AxisAlignedBoundingBox& rBox, const Quad& rQuad)
{
	// To check a box against a quad: first check if it's even close enough to collide.
	// Then first check every line against the quad's plane.
	// if one crosses the plane, get the point at which it crosses. Check if that point
	// lies within the quad's coordinates.

	// Get the plane and check the distance is within the maximum.
	Plane p (rQuad.N, rQuad.TL);
	GLfloat fPlDist = abs(p.GetDist(rBox.GetCentre()));
	bool bPositiveSide = fPlDist > 0;
	const Vector3f& rMaxFromCentre = rBox.GetMaxFromCentre();
	if (fPlDist*fPlDist > rMaxFromCentre.LengthSquared()) return NULL;

	// Check that each corner of the box is on the same side; if not, they're overlapping.
	// TODO: Optimise by keeping all this static stuff as members,
	// instead of constructing every time.
	const Vector3f& rMax = rBox.GetMax();
	const Vector3f& rMin = rBox.GetMin();
	Vector3f vecTTT (rMax);
	Vector3f vecTTB (*rMax.x, *rMax.y, *rMin.z);
	Vector3f vecTBT (*rMax.x, *rMin.y, *rMax.z);
	Vector3f vecTBB (*rMax.x, *rMin.y, *rMin.z);
	Vector3f vecBTT (*rMin.x, *rMax.y, *rMax.z);
	Vector3f vecBTB (*rMin.x, *rMax.y, *rMin.z);
	Vector3f vecBBT (*rMin.x, *rMin.y, *rMax.z);
	Vector3f vecBBB (rMin);

	// See if they're overlapping, and keep track of the one that penetrates the most.
	bool bOverlapping = false;
	float fFarthest = 0;
	Vector3f* pFarthestCorner = NULL;
	GLfloat fTTTDist = p.GetDist(vecTTT);
	GLfloat fTTBDist = p.GetDist(vecTTB);
	GLfloat fTBTDist = p.GetDist(vecTBT);
	GLfloat fTBBDist = p.GetDist(vecTBB);
	GLfloat fBTTDist = p.GetDist(vecBTT);
	GLfloat fBTBDist = p.GetDist(vecBTB);
	GLfloat fBBTDist = p.GetDist(vecBBT);
	GLfloat fBBBDist = p.GetDist(vecBBB);

	if (bPositiveSide != (fTTTDist > 0))
	{
		bOverlapping = true;
		if (abs(fTTTDist) >= abs(fFarthest))
		{
			fFarthest = fTTTDist;
			pFarthestCorner = &vecTTT;
		}
	}
	if (bPositiveSide != (p.GetDist(vecTTB) > 0))
	{
		bOverlapping = true;
		if (abs(fTTBDist) >= abs(fFarthest))
		{
			fFarthest = fTTBDist;
			pFarthestCorner = &vecTTB;
		}
	}
	if (bPositiveSide != (p.GetDist(vecTBT) > 0))
	{
		bOverlapping = true;
		if (abs(fTBTDist) >= abs(fFarthest))
		{
			fFarthest = fTBTDist;
			pFarthestCorner = &vecTBT;
		}
	}
	if (bPositiveSide != (p.GetDist(vecTBB) > 0))
	{
		bOverlapping = true;
		if (abs(fTBBDist) >= abs(fFarthest))
		{
			fFarthest = fTBBDist;
			pFarthestCorner = &vecTBB;
		}
	}
	if (bPositiveSide != (p.GetDist(vecBTT) > 0))
	{
		bOverlapping = true;
		if (abs(fBTTDist) >= abs(fFarthest))
		{
			fFarthest = fBTTDist;
			pFarthestCorner = &vecBTT;
		}
	}
	if (bPositiveSide != (p.GetDist(vecBTB) > 0))
	{
		bOverlapping = true;
		if (abs(fBTBDist) >= abs(fFarthest))
		{
			fFarthest = fBTBDist;
			pFarthestCorner = &vecBTB;
		}
	}
	if (bPositiveSide != (p.GetDist(vecBBT) > 0))
	{
		bOverlapping = true;
		if (abs(fBBTDist) >= abs(fFarthest))
		{
			fFarthest = fBBTDist;
			pFarthestCorner = &vecBBT;
		}
	}
	if (bPositiveSide != (p.GetDist(vecBBB) > 0))
	{
		bOverlapping = true;
		if (abs(fBBBDist) >= abs(fFarthest))
		{
			fFarthest = fBBBDist;
			pFarthestCorner = &vecBBB;
		}
	}

	if (!bOverlapping) return NULL; // All points on the same side of the plane.
	assert(pFarthestCorner);

	// Finally, get the point of impact and check if that's within the face.
	assert(rQuad.N.Length() == 1);
	Vector3f vecContactPoint = rBox.GetCentre() - rQuad.N*fPlDist;

	// Normal vectors for each plane we're checking against - pointing inwards from each edge.
	Vector3f TN = (rQuad.TR - rQuad.TL).Cross(rQuad.N);
	Vector3f BN = (rQuad.BL - rQuad.BR).Cross(rQuad.N);
	Vector3f LN = (rQuad.TL - rQuad.BL).Cross(rQuad.N);
	Vector3f RN = (rQuad.BR - rQuad.TR).Cross(rQuad.N);
	// Planes to check against - if the point's positive for all of them, then it's within the face.
	Plane T (TN, rQuad.TL);
	Plane B (BN, rQuad.BR);
	Plane L (LN, rQuad.BL);
	Plane R (RN, rQuad.TR);

	if (T.GetDist(vecContactPoint) < 0 ||
		B.GetDist(vecContactPoint) < 0 ||
		L.GetDist(vecContactPoint) < 0 ||
		R.GetDist(vecContactPoint) < 0)
	{
		return NULL;
	}
	else
	{
		// We're hitting! Take the contact normal using the normal
		// of the plane and the centre of the box.	
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = bPositiveSide ? p.N : -p.N;
		pData->m_vecPoint = vecContactPoint;
		// Penetration - using the deepest penetrating corner.
		Vector3f vecFromFarthest = rBox.GetCentre() - *pFarthestCorner;
		GLfloat fPenetration = vecFromFarthest.Length() - fPlDist;
		assert(fPenetration >= 0);
		pData->m_vecPenetration = vecFromFarthest.GetUnit() * fPenetration;
		
		return pData;
	}
}
