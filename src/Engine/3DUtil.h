/**
 * 3DUtil.h: Some utility classes for 3D stuff, including classes for Vectors and Colours. There
 * are also a few global functions that are kept here for convenience and reusability.
 */

// TODO: Put global functions under a namespace or class.
// TODO: Split this into a separate file for each class.

#ifndef DUTIL_H
#define DUTIL_H

#include <iostream>
#include <math.h>
#include <vector>
#include <string>
#include <sstream>
#include <assert.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>

using namespace std;

/**
 * A class to represent a Vector with 3 floating-point values. This makes use of operator
 * overloading to make convenient the common vector arithmetic operations, as well as some
 * other common things like doing the dot product, cross product, and rotation given an axis and
 * angle. As vectors are so widely used throughout 3D applications, having a class to represent
 * one seemed to be the only real way to do it. Heavy use of operator overloading, particularly
 * for vector arithmetic, is made here, to make it as straightforward to use as possible.
 */
struct Vector3f
{
	// Some useful constants for mathematical operations.
	static const GLfloat PI;
	static const GLfloat DEGREES_TO_RADIANS;
	static const GLfloat RADIANS_TO_DEGREES;

	// Array containing the values of the vector. tTis is useful to provide as an argument to 
	// various OpenGL functions.
	GLfloat arrVals[3];

	// Pointers to the values within arrVals.
	GLfloat* x;
	GLfloat* y;
	GLfloat* z;	

	// The default constructor makes a zero-length vector.
	Vector3f()
	: x(&arrVals[0]), y(&arrVals[1]), z(&arrVals[2])
	{ Zero(); }

	// Initialise to parameters
	Vector3f(GLfloat fX, GLfloat fY, GLfloat fZ)
	: x(&arrVals[0]), y(&arrVals[1]), z(&arrVals[2])
	{ Set(fX, fY, fZ); }

	// Copy constructor
	Vector3f(const Vector3f& rVec)
	: x(&arrVals[0]), y(&arrVals[1]), z(&arrVals[2])
	{ Set(rVec); }

	// Manually set the values
	void Set(const Vector3f& rVec)
	{
		*x = *rVec.x;
		*y = *rVec.y;
		*z = *rVec.z;
	}

	// Set the values to the provided ones.
	void Set(GLfloat fX, GLfloat fY, GLfloat fZ)
	{
		*x = fX;
		*y = fY;
		*z = fZ;
	}

	// Check equality between vectors.
	bool operator==(const Vector3f& rVec) const 
	{
		return (*x == *rVec.x &&
				*y == *rVec.y &&
				*z == *rVec.z);
	}

	// Set the vector to zero.
	void Zero()
	{
		*x = 0;
		*y = 0;
		*z = 0;
	}

	// Assignment operator: just set the values.
	Vector3f& operator=(const Vector3f& rVec)
	{
		*x = *rVec.x;
		*y = *rVec.y;
		*z = *rVec.z;
		return *this;
	}

	// Overloaded operators for various vector arithmetic operations

	// Vector addition
	Vector3f operator+(const Vector3f& rVec) const
	{
		return Vector3f(*x + *rVec.x, *y + *rVec.y, *z + *rVec.z);
	}

	Vector3f& operator+=(const Vector3f rVec)
	{
		*x += *rVec.x;
		*y += *rVec.y;
		*z += *rVec.z;
		return *this;
	}

	// Vector subtraction
	Vector3f operator-(const Vector3f& rVec) const
	{
		return Vector3f(*x - *rVec.x, *y - *rVec.y, *z - *rVec.z);
	}

	Vector3f& operator-=(const Vector3f& rVec)
	{
		*x -= *rVec.x;
		*y -= *rVec.y;
		*z -= *rVec.z;
		return *this;
	}


	// Scaling

	Vector3f operator*(GLfloat fMult) const
	{
		return Vector3f(*x * fMult, *y * fMult, *z * fMult);
	}

	Vector3f operator/(GLfloat fDiv) const
	{
		return Vector3f(*x / fDiv, *y / fDiv, *z / fDiv);
	}

	Vector3f& operator*=(GLfloat iMult) 
	{
		*x *= iMult;
		*y *= iMult;
		*z *= iMult;
		return *this;
	}

	Vector3f& operator/=(GLfloat iDiv)
	{
		*x /= iDiv;
		*y /= iDiv;
		*z /= iDiv;
		return *this;
	}

	// Unary -: invert the vector.
	Vector3f operator-()
	{
		return Vector3f(-(*x), -(*y), -(*z));
	}

	// Cross product (vector product)
	Vector3f Cross(const Vector3f& rVec) const
	{
		return Vector3f(
			(*y) * (*rVec.z) - (*z) * (*rVec.y), 
			(*z) * (*rVec.x) - (*x) * (*rVec.z),
			(*x) * (*rVec.y) - (*y) * (*rVec.x));
	}

	// Dot product (scalar product) - can also use *.
	float Dot(const Vector3f& rVec) const
	{
		return (*x)*(*rVec.x) + (*y)*(*rVec.y) + (*z)*(*rVec.z);
	}

	float operator*(const Vector3f& rVec) const
	{
		return Dot(rVec);
	}

	// Calculate the length (magnitude) of the vector.
	GLfloat Length() const
	{
		return sqrtf((*x)*(*x) + (*y)*(*y) + (*z)*(*z));
	}		   		

	// Squared magnitude for when we want to avoid inefficient square rooting.
	GLfloat LengthSquared() const
	{
		return (*x)*(*x) + (*y)*(*y) + (*z)*(*z);
	}

	// Normalise the vector (make it length one).
	Vector3f& Normalise()
	{
		GLfloat fLength = Length();

		if (fLength == 0)
			return *this;

		*x /= fLength;
		*y /= fLength;
		*z /= fLength;
		return *this;
	}

	// Rotate this vector about an axis by an angle in degrees.
	Vector3f& Rotate(const Vector3f& rAxis, float fAngle)
	{
		fAngle *= DEGREES_TO_RADIANS;

		float len = Length();

		// Basically a matrix multiplication operation.
		float c = cos(fAngle);
		float s = sin(fAngle);
		float t = 1 - c;
		float X = *rAxis.x;
		float Y = *rAxis.y;
		float Z = *rAxis.z;

		float newX, newY, newZ;
		
		newX = *x * (t * X*X + c) +
			   *y * (t * X*Y + s*Z) +
			   *z * (t * X*Z - s*Y); 

		newY = *x * (t * X*Y - s*Z) +
			   *y * (t * Y*Y + c) +
			   *z * (t * Y*Z + s*X); 

		newZ = *x * (t * X*Y + s*Y) +
			   *y * (t * Y*Z - s*X) +
			   *z * (t * Z*Z + c);

		Set(newX, newY, newZ);

		// Normalise and make it the proper length again.
		Normalise();
		*this *= len;

		return *this;
	}

	// Get a rotated copy of this vector.
	Vector3f GetRotated(const Vector3f& rAxis, float fAngle) const
	{
		Vector3f vRotated (*this);
		vRotated.Rotate(rAxis, fAngle);
		return vRotated;
	}

	// Get the normalised (length one) version of this vector.
	Vector3f GetUnit() const
	{
		Vector3f oVec (*this);
		oVec.Normalise();
		return oVec;
	}

	// Get a string representation, e.g. for debugging.
	string GetString() const
	{
		stringstream sout;
		sout << "Vector3f { " << *x << ", " << *y << ", " << *z << " }";
		return sout.str();
	}

	// Get any arbitrary vector that is orthogonal (at a right angle) to this one.
	Vector3f GetNormal() const
	{
		// Pick X as our arbitrary vector, then get some vector that's orthogonal to ours.
		// note that we may need to try again if this vector is exactly parallel to X.
		Vector3f vecCalc (1, 0, 0);
		vecCalc = Cross(vecCalc); 
		if (vecCalc.Length() == 0) vecCalc = Cross(Vector3f(0, 1, 0));
		vecCalc.Normalise();
		return vecCalc;		
	}

	// Return a random vector with values between the two provided.
	// For the time being the C random number generator should be suitable for our purposes, but
	// it may be a good idea to use some library like Boost or Newran for "more random" random
	// numbers.
	// (Remember to seed srand!)
	static Vector3f RandomBetween(const Vector3f& rOne, const Vector3f& rTwo);	
	// Get the mid point between two vectors.
	static Vector3f MidPoint(const Vector3f& rOne, const Vector3f& rTwo);

};


/** 
 * Quaternion: This class represents a Quaternion, generally for the purposes of representing
 * arbitrary rotations. The justification for having this as a class like below are very much the
 * same as for Vector3f and Colour4f.
 */
struct Quaternion
{
	GLfloat arrVals[4];

	GLfloat* r;
	GLfloat* i;
	GLfloat* j;
	GLfloat* k;

	// Default constructor: zero rotation.
	Quaternion() 
	: r(&arrVals[0]), i(&arrVals[1]), j(&arrVals[2]), k(&arrVals[3]) 
	{ Set(1, 0, 0, 0); }

	// Constructor from the given values.
	Quaternion(GLfloat fR, GLfloat fI, GLfloat fJ, GLfloat fK)
	: r(&arrVals[0]), i(&arrVals[1]), j(&arrVals[2]), k(&arrVals[3]) 
	{ Set(fR, fI, fJ, fK); }
	
	// Copy constructor.
	Quaternion(const Quaternion& rQuat)
	: r(&arrVals[0]), i(&arrVals[1]), j(&arrVals[2]), k(&arrVals[3]) 
	{ Set(rQuat); }

	// Set the values of this quaternion to those in the provided one.
	void Set(const Quaternion& rQuat)
	{
		*r = *rQuat.r;
		*i = *rQuat.i;
		*j = *rQuat.j;
		*k = *rQuat.k;
	}

	// Set the values of this quaternion to the provided ones.
	void Set(GLfloat fR, GLfloat fI, GLfloat fJ, GLfloat fK)
	{
		*r = fR;
		*i = fI;
		*j = fJ;
		*k = fK;
	}

    void Normalise()
    {
        GLfloat d = (*r)*(*r) + (*i)*(*i) + (*j)*(*j) + (*k)*(*k);

        // If the length is zero, return a no-rotation quaternion.
        if (d == 0) 
		{ 
            *r = 1.0f; 
            return;
        }

        d = (GLfloat)1.0f / sqrt(d);
        *r *= d;
        *i *= d;
        *j *= d;
        *k *= d;
    }

	// Quaternion multiplication of this one by a given quaternion.
	Quaternion operator*(const Quaternion& rQuat) const
	{
		Quaternion qResult (*this);
        *qResult.r = (*r)*(*rQuat.r) - (*i)*(*rQuat.i) - 
					 (*j)*(*rQuat.j) - (*k)*(*rQuat.k);
        *qResult.i = (*r)*(*rQuat.i) + (*i)*(*rQuat.r) + 
					 (*j)*(*rQuat.k) - (*k)*(*rQuat.j);
        *qResult.j = (*r)*(*rQuat.j) + (*j)*(*rQuat.r) + 
					 (*k)*(*rQuat.i) - (*i)*(*rQuat.k);
        *qResult.k = (*r)*(*rQuat.k) + (*k)*(*rQuat.r) + 
					 (*i)*(*rQuat.j) - (*j)*(*rQuat.i);

		return qResult;
	}

    void operator*=(const Quaternion& rQuat)
    {
		Set(*this * rQuat);
    }

	// Quaternion multiplication by a scalar
	Quaternion operator*(GLfloat fScale) const
	{
		Quaternion qResult (*this);
        *qResult.r *= fScale;
        *qResult.i *= fScale;
        *qResult.j *= fScale;
        *qResult.k *= fScale;

		return qResult;
	}

	void operator*=(GLfloat fScale)
	{
		Set(*this * fScale);
	}

	// Add a vector to this quaternion, used to update orientation.
	Quaternion operator+(const Vector3f& rVec) const
	{
        Quaternion q(0, *rVec.x, *rVec.y, *rVec.z);
        q *= *this;
        *r += (*q.r) * 0.5f;
        *i += (*q.i) * 0.5f;
        *j += (*q.j) * 0.5f;
        *k += (*q.k) * 0.5f;
		return q;
	}

	void operator+=(const Vector3f& rVec)
	{
		Set(*this + rVec);
	}

	// Add a quaternion to this quaternion.
	Quaternion operator+(const Quaternion& rQuat) const
	{
		Quaternion q(*this);
		*q.r += *rQuat.r;
		*q.i += *rQuat.i;
		*q.j += *rQuat.j;
		*q.k += *rQuat.k;
		return q;
	}

	void operator+=(const Quaternion& rQuat)
	{
		Set(*this + rQuat);
	}

	// Rotate by vector: do this by putting the given vector into a quaternion then multiplying.
    void RotateByVector(const Vector3f& rVec)
    {
		Quaternion q(0, *rVec.x, *rVec.y, *rVec.z);
		*this *= q;
    }

	// Get an axis/angle out of this quaternion.
	void GetAxisAngle(Vector3f* pAxis, GLfloat* pAngle) const
	{
		// Need a normalised quaternion for this to work properly.
		Quaternion q (*this);
		if (*q.r > 1) q.Normalise();
		*pAngle = 2 * acosf(*q.r);
		// Always positive, as the quaternion is normalised.
		double fScale = sqrtf(1 - (*q.r)*(*q.r));
		
		// Set the axis.
		// Can't have any dividing by zero..
		if (fScale < 0.001) 
		{ 
			*pAxis->x = *q.i;
			*pAxis->y = *q.j;
			*pAxis->z = *q.k;
		} 
		else 
		{
			*pAxis->x = *q.i / fScale;
			*pAxis->y = *q.j / fScale;
			*pAxis->z = *q.k / fScale;
		}

		*pAngle *= Vector3f::RADIANS_TO_DEGREES;
	}
};
 

/**
 * Colour4f: This class works very much like the Vector3f class in the way that it stores a
 * set of floating point numbers. It also, however, contains some useful functions for common
 * operations on colours. The reasoning for using this class is the same as that for Vector3f -
 * colours are used often when dealing with OpenGL, and it makes sense to use a class for it.
 */
struct Colour4f
{
	// Array for GL calls and the like.
	GLfloat arrVals[4];

	// These pointers are basically aliases for the values above.
	GLfloat* r;
	GLfloat* g;
	GLfloat* b;
	GLfloat* a;

	// Constructor for the default colour (zero - transparent black)
	Colour4f()
	: r(&arrVals[0]), g(&arrVals[1]), b(&arrVals[2]), a(&arrVals[3]) 
	{ 
		Zero(); 
	}
	Colour4f(GLfloat iR, GLfloat iG, GLfloat iB, GLfloat iA)
	: r(&arrVals[0]), g(&arrVals[1]), b(&arrVals[2]), a(&arrVals[3])
	{		
		Set(iR, iG, iB, iA); 
	}
	Colour4f(const Colour4f& rCol)
	: r(&arrVals[0]), g(&arrVals[1]), b(&arrVals[2]), a(&arrVals[3]) 
	{
		Set(rCol);
	}
	~Colour4f() { }

	void InBounds()
	{
		if (*r > 1.0) *r = 1.0;
		if (*g > 1.0) *g = 1.0;
		if (*b > 1.0) *b = 1.0;
		if (*a > 1.0) *a = 1.0;
		if (*r < 0.0) *r = 0.0;
		if (*g < 0.0) *g = 0.0;
		if (*b < 0.0) *b = 0.0;
		if (*a < 0.0) *a = 0.0;
	}

	void Set(const Colour4f& rCol)
	{
		*r = *rCol.r;
		*g = *rCol.g;
		*b = *rCol.b;
		*a = *rCol.a;
		InBounds();
	}

	void Set(GLfloat fR, GLfloat fG, GLfloat fB, GLfloat fA)
	{
		*r = fR;
		*g = fG;
		*b = fB;
		*a = fA;
		InBounds();
	}

	// Check equality
	bool operator==(const Colour4f& rCol) const 
	{
		return (*r == *rCol.r &&
				*g == *rCol.g &&
				*b == *rCol.b &&
				*a == *rCol.a);
	}

	// Assignment operator: just set the values.
	Colour4f& operator=(const Colour4f& rCol)
	{
		Set(rCol);
		return *this;
	}

	// Arithmetic operators are pretty intuitive, in that they do the same thing to all the values.

	Colour4f operator+(const Colour4f& rCol) const
	{
		return Colour4f(*r + *rCol.r, *g + *rCol.g, *b + *rCol.b, *a + *rCol.a);
	}

	Colour4f operator-(const Colour4f& rCol) const
	{
		return Colour4f(*r - *rCol.r, *g - *rCol.g, *b - *rCol.b, *a - *rCol.a);
	}

	Colour4f& operator+=(const Colour4f rCol)
	{
		*r += *rCol.r;
		*g += *rCol.g;
		*b += *rCol.b;
		*a += *rCol.a;
		InBounds();
		return *this;
	}

	Colour4f& operator-=(const Colour4f& rCol)
	{
		*r -= *rCol.r;
		*g -= *rCol.g;
		*b -= *rCol.b;
		*a -= *rCol.a;
		InBounds();
		return *this;
	}

	// Scaling

	Colour4f operator*(GLfloat fMult) const
	{
		return Colour4f(*r * fMult, *g * fMult, *b * fMult, *a * fMult);
	}

	Colour4f operator/(GLfloat fDiv) const
	{
		return Colour4f(*r / fDiv, *g / fDiv, *b / fDiv, *a / fDiv);
	}

	Colour4f& operator*=(GLfloat iMult) 
	{
		*r *= iMult;
		*g *= iMult;
		*b *= iMult;
		*a *= iMult;
		InBounds();
		return *this;
	}

	Colour4f& operator/=(GLfloat iDiv)
	{
		*r /= iDiv;
		*g /= iDiv;
		*b /= iDiv;
		*a /= iDiv;
		InBounds();
		return *this;
	}

	// -vec: inverse of this one.
	Colour4f operator-()
	{
		return Colour4f(-(*r), -(*g), -(*b), -(*a));
	}


	// Get a string for printing.
	string GetString() const
	{
		stringstream sout;
		sout << "Colour4f { " << *r << ", " << *g << ", " << *b << ", " << *a << " }";
		sout << endl;
		return sout.str();
	}

	// Brighten: increase each r/g/b value by this amount, not going over the max.
	void Brighten(GLfloat fAmount)
	{
		*r += fAmount;
		*g += fAmount;
		*b += fAmount;
		InBounds();
	}

	// Set all values to zero.
	void Zero()
	{
		arrVals[0] = 0;
		arrVals[1] = 0;
		arrVals[2] = 0;
		arrVals[3] = 0;
	}


	// Return a random colour with values between the two provided. Again, the basic C RNG is used
	// here; some library may be better.
	// (Remember to seed srand!)
	static Colour4f RandomBetween(const Colour4f& rOne, const Colour4f& rTwo);

};

/**
 * Represents a plane in 3D space.
 */
struct Plane
{
	// Equation: N . (r - p) = 0, where N is the normal, r is any given point on the plane and p is 
	// the provided point on the plane.
	Vector3f N;
	Vector3f p;

	// Constructor.
	Plane(Vector3f vecNorm, Vector3f vecP)
	: N(vecNorm.GetUnit())
	, p(vecP) {}

	// Check a point against this plane. Positive if on the side of the normal.
	GLfloat GetDist(const Vector3f& r) const
	{
		Vector3f vecCalc = r - p;
		return N.Dot(vecCalc);
	}

	// Do the above, but square the distance too.
	GLfloat GetDistSquared(const Vector3f& r) const
	{
		GLfloat fDist = GetDist(r);
		return fDist*fDist;
	}

	// Get a string for debugging.
	string GetString() const
	{
		stringstream sout;
		sout << "Plane { Normal: " << N.GetString() << ", Point: " << p.GetString() << " }" << endl;
		return sout.str();
	}

};

enum PRIMITIVE_TYPE { SPHERE, QUAD };

// Superclass for the primitives below.
class Primitive
{
private:
	PRIMITIVE_TYPE m_iType;

public:
	Primitive(PRIMITIVE_TYPE iType) : m_iType(iType) {}
	virtual ~Primitive() {}

	PRIMITIVE_TYPE GetType() const { return m_iType; }
};

// Simple Primitive struct for a quad.
struct Quad : public Primitive
{
	Vector3f TL, TR, BR, BL, N; // Corners and normal

	// Blank constructor
	Quad() : Primitive(QUAD) { } 
	// Constructor from values
	Quad(Vector3f vecTL, Vector3f vecTR, Vector3f vecBR, Vector3f vecBL, Vector3f vecN)
	: Primitive(QUAD) 
	, TL(vecTL), TR(vecTR), BR(vecBR), BL(vecBL), N(vecN) { }

	// Translate all the vectors in this quad by a given vector.
	void Translate(const Vector3f& rVec)
	{
		TL += rVec;
		TR += rVec;
		BR += rVec;
		BL += rVec;
	}

	// Rotate this quad.
	void Rotate(const Vector3f& rAxis, GLfloat fAngle)
	{
		TL.Rotate(rAxis, fAngle);
		TR.Rotate(rAxis, fAngle);
		BR.Rotate(rAxis, fAngle);
		BL.Rotate(rAxis, fAngle);
		N.Rotate(rAxis, fAngle);
	}

	Quad GetRotated(const Vector3f& rAxis, GLfloat fAngle) const
	{
		Quad qRotated (*this);
		qRotated.Rotate(rAxis, fAngle);
		return qRotated;
	}

	void Render()
	{
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_LIGHTING);

		glColor4f(0.9, 0.9, 0.9, 1);

		glBegin(GL_LINES);
			
			glVertex3fv(TL.arrVals);
			glVertex3fv(TR.arrVals);

			glVertex3fv(TR.arrVals);
			glVertex3fv(BR.arrVals);

			glVertex3fv(BR.arrVals);
			glVertex3fv(BL.arrVals);

			glVertex3fv(BL.arrVals);
			glVertex3fv(TL.arrVals);

		glEnd();

		glEnable(GL_TEXTURE_2D);
		glEnable(GL_LIGHTING);
	}

};

// Simple Primitive struct for a sphere.
struct Sphere : public Primitive
{
	Vector3f	m_vecPos;
	GLfloat		m_fRadius;

	// Blank constructor
	Sphere() : Primitive(SPHERE), m_fRadius(0.0f) {}
	// Constructor from values
	Sphere(Vector3f vecPos, GLfloat fRadius)
	: Primitive(SPHERE)
	, m_vecPos(vecPos), m_fRadius(fRadius) {}

};


// Global functions
// Draw a quad made up of lots of smaller quads, optionally binding textures too.
void DrawQuadMesh(const Vector3f& rTL, const Vector3f& rTR,
				  const Vector3f& rBR, const Vector3f& rBL, const Vector3f& rN,
				  unsigned int iHRes, unsigned int iVRes);

// Return a random number between the first and the second.
GLint RandomBetween(const GLint& rOne, const GLint& rTwo);
GLfloat RandomBetween(const GLfloat& rOne, const GLfloat& rTwo);


#endif /* 3DUTIL_H */
