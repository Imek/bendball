/**
 * GameSettings.h: All the default settings for the game, implementing the settings for Future Pong.
 */

#include "GameSettings.h"

/**
 * ShaderConfig code
 */

bool ShaderConfig::s_bInitialised = false;
bool ShaderConfig::s_bSupported = false;

// Read some text from a file (i.e. source code for a shader) by line into an array of strings.
GLcharARB* ShaderConfig::ReadShaderSrc(const char* szFilePath, 
									   GLint* pLength)
{
	ifstream fin (szFilePath);
	string strFile;
	string strLine;
	while (getline(fin, strLine))
	{
		strLine.append("\n");
		strFile += strLine;
	}

	// Convert our nice STL string into the annoying fiddly GL ARB cstring thingy
	*pLength = (unsigned int)strFile.size();
	GLcharARB* szFile = new GLcharARB[(*pLength)+1];
	for (unsigned int i = 0; i < strFile.size(); ++i)
		szFile[i] = (GLcharARB)(strFile[i]);
	szFile[(*pLength)] = (GLcharARB)'\0';

	return szFile;
}

// Constructor for ShaderConfig.
ShaderConfig::ShaderConfig(const char *szVertPath, const char *szFragPath)
{
	// Initialise the system if it hasn't been done already - generally, this should have 
	// been called in the game's Init method.
	if (!s_bInitialised) InitSystem();

	// Don't bother doing anything if shaders weren't supported.
	if (!s_bSupported) return;

	// Read source file and make/compile the shader.
	// Read the vertex shader.
	GLint arrLengths[1];
	const GLcharARB* szVertSrc = ReadShaderSrc(szVertPath, &arrLengths[0]);

	GLhandleARB iVertObj = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
	glShaderSourceARB(iVertObj, 1, &szVertSrc, arrLengths);
	delete szVertSrc;
	glCompileShaderARB(iVertObj);

	// Read the fragment shader too.
	const GLcharARB* szFragSrc = ReadShaderSrc(szFragPath, &arrLengths[0]);

	GLhandleARB iFragObj = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
	glShaderSourceARB(iFragObj, 1, &szFragSrc, arrLengths);
	delete szFragSrc;
	glCompileShaderARB(iFragObj);

	// KEEP THIS DEBUG CODE: Check if it compiled correctly.
	/*int iCompiled = 0, iLength = 0, iLaux = 0;
	glGetObjectParameterivARB(iVertObj, GL_OBJECT_COMPILE_STATUS_ARB, &iCompiled);
	glGetObjectParameterivARB(iVertObj, GL_OBJECT_INFO_LOG_LENGTH_ARB, &iLength);
	GLcharARB* szLog = new GLcharARB[iLength];
	glGetInfoLogARB(iVertObj, iLength, &iLaux, szLog);
	
	cout << "compiled: " << iCompiled << endl;
	cout << "log: " << szLog << endl;*/

	// Finally, put it into a program object and link it.
	GLhandleARB iShaderObj = glCreateProgramObjectARB();
	glAttachObjectARB(iShaderObj, iVertObj);
	glAttachObjectARB(iShaderObj, iFragObj);
	glLinkProgramARB(iShaderObj);

	// KEEP THIS DEBUG CODE: Check if it linked correctly.
	/*int iLinked = 0, iLength = 0, iLaux = 0;
	glGetObjectParameterivARB(iShaderObj, GL_OBJECT_LINK_STATUS_ARB, &iLinked);
	glGetObjectParameterivARB(iShaderObj, GL_OBJECT_INFO_LOG_LENGTH_ARB, &iLength);
	GLcharARB* szLog = new GLcharARB[iLength];
	glGetInfoLogARB(iShaderObj, iLength, &iLaux, szLog);	
	cout << "linked: " << iLinked << endl;
	cout << "log: " << szLog << endl;*/

	m_iShaderObj = iShaderObj;
}

void ShaderConfig::SwitchOn() const
{ 
	if (s_bSupported)
		glUseProgramObjectARB(m_iShaderObj); 
}

void ShaderConfig::SwitchOff() const
{ 
	if (s_bSupported)
		glUseProgramObjectARB(0); 
}

void ShaderConfig::SetUniform1f(const char *szName, GLfloat fVal) const
{
	int iLoc = glGetUniformLocationARB(m_iShaderObj, szName);
	glUniform1fARB(iLoc, fVal);
}

/**
 * InitSystem
 */
void ShaderConfig::InitSystem()
{
	/**
	 * Initialise GLEW, checking if it worked.
	 */
	cout << "Initialising GLEW..." << endl;
	GLenum err = glewInit();
	if (err != GLEW_OK)
	{
		cerr << "Glew didn't init properly for some reason." << endl;
		exit(EXIT_FAILURE);
	}

	// See if our shader stuff is supported.
	if (!(GL_ARB_vertex_shader &&
		  GL_ARB_fragment_shader &&
		  GL_ARB_shader_objects &&
		  GL_ARB_shading_language_100))
	{
		cout << "Boo, shaders not supported." << endl;
		s_bSupported = false;
	}
	else
	{
		// If so, we can load them up.
		cout << "Huzzah, shaders supported!" << endl;
		s_bSupported = true;
	}

	s_bInitialised = true;
}


/**
 * TextureManager code
 */

// Set the instance of the TextureManager singleton as NULL, so that we know to instatniate it.
TextureManager* TextureManager::m_pInstance = NULL;

// Load up some textures from provided file paths, returning the ID numbers (names) to recall them.
void TextureManager::LoadTextures(unsigned int iNo, char **pPaths, unsigned int *pNames)
{
	if (iNo <= 0) return;
	// Get the IDs.
	glGenTextures(iNo, pNames);
	
	// For each one...
	for (unsigned int i = 0; i < iNo; i++)
	{	
		//cout << "pNames[i]: " << pNames[i] << endl;
		// Load the BMP from the file..
		BITMAPINFO* pInfo;
		GLubyte* pTex = LoadDIBitmap(pPaths[i], &pInfo);
		if (!pTex)
		{
			cerr << "Failed reading texture file: " << pPaths[i] << endl;
			exit(EXIT_FAILURE);
		}
		// .. and get OpenGL to load up the texture.
		glBindTexture(GL_TEXTURE_2D, pNames[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, 
			pInfo->bmiHeader.biWidth, pInfo->bmiHeader.biHeight, 
			GL_RGB, GL_UNSIGNED_BYTE, pTex);
	}

}


/**
 * GameSettings stuff
 */

GameSettings* GameSettings::m_pInstance = NULL;

GameSettings::GameSettings()
{
	m_iTimerDelay = 5;

	m_iXResolution = 800;
	m_iYResolution = 400;
	m_fAspectRatio = (GLfloat) 1.4;

	m_pWindowTitle = "BendBall 3000!";

	m_pFont = GLUT_BITMAP_8_BY_13;
}

// Get the instance of the GameSettings singleton.
const GameSettings& GameSettings::Get()
{
	if (m_pInstance == NULL)
		m_pInstance = new GameSettings();

	return *m_pInstance;
}
