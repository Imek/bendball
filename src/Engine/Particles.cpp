/**
 * Particles.cpp: Implementations for the different types of particles declared in Particles.h.
 */

#include "Particles.h"

/**
 * Render member functions for the different types of particle object.
 */

/**
 * Render a PointP: Just draw a point with the given position and colour (m_fSize is not used.)
 */
void PointP::Render()
{
	// Disable textures and lighting. It may be desirable at some point to have particles
	// affected by lighting, but not for now.
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);

	glBegin(GL_POINTS);
		
		glColor4fv(m_colColour.arrVals);
		glVertex3fv(m_vecPos.arrVals);

	glEnd();

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
}

/**
 * Render a QuadP: Very similar to PointP, except that we draw a quad with the given size.
 */
void QuadP::Render()
{
	// Disable textures and lighting. It may be desirable at some point to have particles
	// affected by lighting, but not for now.
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);

	glBegin(GL_QUADS);
		
		glColor4fv(m_colColour.arrVals);
		glVertex3f(*m_vecPos.x - m_fSize, *m_vecPos.y + m_fSize, *m_vecPos.z);
		glVertex3f(*m_vecPos.x + m_fSize, *m_vecPos.y + m_fSize, *m_vecPos.z);
		glVertex3f(*m_vecPos.x + m_fSize, *m_vecPos.y - m_fSize, *m_vecPos.z);
		glVertex3f(*m_vecPos.x - m_fSize, *m_vecPos.y - m_fSize, *m_vecPos.z);

	glEnd();

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
}

/**
 * Render a SphereP: just use glutSolidSphere to draw it.
 */
void SphereP::Render()
{
	// Disable textures and lighting. It may be desirable at some point to have particles
	// affected by lighting, but not for now.
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);

	// Translate, set colour, and draw a simple sphere.

	glPushMatrix();

	glColor4fv(m_colColour.arrVals);
	glTranslatef(*m_vecPos.x, *m_vecPos.y, *m_vecPos.z);
	
	glutSolidSphere(m_fSize, 8, 8);

	glPopMatrix();

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
}


/**
 * ParticleGenerator code
 */

/**
 * Update function for ParticleGenerator: called in the main game update loop when appropriate.
 * the purpose of the generator is to manage the creation and destruction of particles, based
 * on the appropriate settings (creation rate, life span, and so on).
 */
void ParticleGenerator::Update(int iTime)
{
	// First, update all of the particles in the game. Free them if they've expired.
	for (list< ParticleController* >::iterator it = m_lParticles.begin();
		 it != m_lParticles.end(); /* Incremented below */)
	{
		if ((*it)->Expired())
		{
			// Remove from the particle list and add to the free list.
			m_lFree.push_back(*it);
			it = m_lParticles.erase(it);
		}
		else
		{
			(*it)->Update(iTime);
			it++;
		}			
	}

	// If we're switched off, don't make any new particles.
	if (!m_bOn) return;

	// Particle creation rate is per millisecond.	
	// Whatever is left in fParts needs to be kept in m_fLeftOver for the next iteration.
	m_fLeftOver = (GLfloat)iTime*m_oPConf.m_fParticleRate + m_fLeftOver;
	GLuint iNoToCreate = (GLuint)m_fLeftOver;
	m_fLeftOver -= iNoToCreate;

	// Create as many new particles as we can.
	// If there's not enough free space, then that means the rate is higher than we can manage -
	// just drop the number we can't make.
	for (unsigned int i = 0; i < iNoToCreate; i++)
	{
		if (!m_lFree.empty() && m_oPConf.m_iTrailNoMax == 1) // If we're not doing trails..
		{
			// Move the free particle to the particle list and reset it.
			ParticleController* pNewPart = m_lFree.back();
			m_lFree.pop_back();
			pNewPart->Reset(m_oPConf);
			m_lParticles.push_back(pNewPart);
		}
		else if (m_lFree.size() >= m_oPConf.m_iTrailNoMin) // We're doing trails.
		{
			// Work out values for fading/interpolating the particle trail.
			GLuint iTrailNo = m_oPConf.GetRandTrailNo();				
			GLfloat fTrailSizeEnd = m_oPConf.GetRandTrailSizeEnd();
			Colour4f colTrailColEnd = m_oPConf.GetRandTrailColEnd();				
			GLuint iDelay = m_oPConf.GetRandTrailDelay();
			GLuint iThisDelay = 0;

			// Make the first particle, then interpolate the size and colour towards the end of the trail.
			ParticleController* pNewPart = m_lFree.back();
			m_lFree.pop_back();
			pNewPart->Reset(m_oPConf);
			m_lParticles.push_back(pNewPart);

			Colour4f colThis = pNewPart->GetConstColour();
			GLfloat fThisSize = pNewPart->GetSize();
			Colour4f colDiff = (colTrailColEnd - colThis) / (GLfloat)(iTrailNo);
			GLfloat fSizeDiff = (fTrailSizeEnd - fThisSize) / (GLfloat)(iTrailNo);

			const Vector3f& rPos = pNewPart->GetConstParticle().m_vecPos;
			const Vector3f& rVel = pNewPart->GetConstParticle().m_vecVel;
			
			for (unsigned int i = 1; i < iTrailNo; ++i)
			{
				iThisDelay += iDelay;
				fThisSize += fSizeDiff;					
				colThis += colDiff;					

				ParticleController* pNewPart = m_lFree.back();
				m_lFree.pop_back();
				pNewPart->Reset(m_oPConf, iThisDelay);
				pNewPart->SetSize(fThisSize);
				pNewPart->GetColour().Set(colThis);
				pNewPart->GetParticle().m_vecPos.Set(rPos);
				pNewPart->GetParticle().m_vecVel.Set(rVel);
				m_lParticles.push_back(pNewPart);
			}
		}
		else // Free list is empty, or not enough to make a full trail.
		{
			if (m_bFireOnce)
			{
				m_bFireOnce = false;
				m_bOn = false;
			}
			break;
		}
	}

}

/**
 * Render function for ParticleGenerator, rendered along with the rest of the game. To render a
 * particle generator is essentially to render every individual particle managed by the generator.
 */
void ParticleGenerator::Render()
{
	for (list< ParticleController* >::iterator it = m_lParticles.begin();
		 it != m_lParticles.end(); ++it)
	{
		(*it)->SetEntityAtts(m_pParticleEntity);
		m_pParticleEntity->Render();
	}
}
