#include "FPGame.h"

/**
 * FPGame code
 */

void FPGame::UpdateCollisions(int iTime)
{
	assert(m_vBallControllers.size() == m_vBalls.size());

	// Check collisions for each ball.
	for (unsigned int i = 0; i < m_vBallControllers.size(); ++i)
		UpdateBallCollisions(iTime, i);

	// Collisions for deflectors
	CollisionData* pData;
	vector<Quad> vWallQuads = m_pField->GetWallQuads();	
	AxisAlignedBoundingBox oDef1Box = m_pDef1->GetBoundingBox();
	AxisAlignedBoundingBox oDef2Box = m_pDef2->GetBoundingBox();

	for (vector<Quad>::const_iterator it = vWallQuads.begin();
		 it != vWallQuads.end(); ++it)
	{
		const Quad& rThisOne = *it;

		// Deflectors		
		pData = CollisionDetect::AABBAndQuad(oDef1Box, rThisOne);
		if (pData) 
			m_pDefCont1->UpdateCollisions(pData->m_vecNormal);

		pData = CollisionDetect::AABBAndQuad(oDef2Box, rThisOne);
		if (pData)
			m_pDefCont2->UpdateCollisions(pData->m_vecNormal);
	}

}


void FPGame::UpdateBallCollisions(int iTime, int iBall)
{
	assert (iBall >= 0 && iBall < (int)m_vBalls.size() && m_vBalls.size() == m_vBallControllers.size());
	Ball* pBall = m_vBalls[iBall];
	BallController* pBallController = m_vBallControllers[iBall];
	Sphere oBall (pBall->GetConstPosition(), pBall->GetSize());
	CollisionData* pData = NULL;

	// First, check collisions between this ball and other balls (note that we check only balls with an ID 
	// above our own, so that we only have one collision event per actual collision)
	for (unsigned int i = iBall+1; i < m_vBalls.size(); ++i)
	{
		Ball* pOtherBall = m_vBalls[i];
		BallController* pOtherCont = m_vBallControllers[i];
		Sphere oOther (pOtherBall->GetConstPosition(), pOtherBall->GetSize());
		pData = CollisionDetect::SphereAndSphere(oBall, oOther);
		if (pData)
		{
			m_oEventHandler.PushEvent( new BallCollisionEvent( 
				ParticleCollision(&pBallController->GetParticle(), 
				&pOtherCont->GetParticle(), 1.0f, *pData)));

			// Resolving of angular velocity is a bit hacky for now, but should look okay.
			Vector3f& rAngVel1 = pBallController->GetAngularVelocity();
			Vector3f& rAngVel2 = pOtherCont->GetAngularVelocity();

			Vector3f vecAverage = Vector3f::MidPoint(rAngVel1, rAngVel2);
			rAngVel1 = vecAverage;
			rAngVel2 = -vecAverage;
		}
	}

	// Ignore any collisions for the ball if it's sinking into lava.
	bool bBallSinking = (m_vTimesTillRespawn[iBall] > 0);

	const FPSettings& rFPSettings = FPSettings::Get();
	
	AxisAlignedBoundingBox oDef1Box = m_pDef1->GetBoundingBox();
	AxisAlignedBoundingBox oDef2Box = m_pDef2->GetBoundingBox();

	// Check if the ball is colliding with the lava.
	if (!bBallSinking)
	{
		bool bP1Lava = true;
		vector<Plane> vLavaPlanes = m_pField->GetLavaPlanes();
		for (vector<Plane>::const_iterator it = vLavaPlanes.begin();
			 it != vLavaPlanes.end(); ++it)
		{
			const Plane& rThisOne = *it;
			pData = CollisionDetect::SphereAndPlane(oBall, rThisOne);
			if (pData)
			{
				pBallController->Kill();
				m_bP1LastScored = !bP1Lava;
				if (bP1Lava)
					m_iP2Score++;
				else
					m_iP1Score++;
				// TODO: Currently P1 always serves. Should make it so that the one who scored gets to serve.
				m_pDefCont1->SetFiring(true);
				m_vTimesTillRespawn[iBall] = rFPSettings.GetBallRespawnDelay();
				pBallController->Flash(m_vTimesTillRespawn[iBall], 0.003f, Colour4f(1.0f, 0.1f, 0.2f, 1.0f));
				

				delete pData;
			}
			bP1Lava = false;
		}

		// Check if the ball is colliding with a deflector.
		pData = CollisionDetect::SphereAndAABB(oBall, oDef1Box);
		if (pData)
		{
			// Switch off firing if it's on.
			m_pDefCont1->SetFiring(false);

			//DetachBall(iBall);
			// Get the particle to represent the Deflector's motion.
			m_oEventHandler.PushEvent( new BallCollisionEvent( 
				ParticleCollision(&pBallController->GetParticle(), 
				&m_pDefCont1->GetParticle(), 1.0f, *pData)));
			// Add some spin to the ball.
			Vector3f vecSpinAdd = pData->m_vecNormal.Cross(-m_pDefCont1->GetMotion()*3);
			Vector3f& rAngVel = pBallController->GetAngularVelocity();
			rAngVel = -(rAngVel -vecSpinAdd);
		}

		pData = CollisionDetect::SphereAndAABB(oBall, oDef2Box);
		if (pData)
		{
			// Switch off firing if it's on.
			m_pDefCont2->SetFiring(false);

			//DetachBall(iBall);
			// Get the particle to represent the Deflector's motion.
			m_oEventHandler.PushEvent( new BallCollisionEvent( 
				ParticleCollision(&pBallController->GetParticle(), 
				&m_pDefCont2->GetParticle(), 1.0f, *pData )));
			// Add some spin to the ball.
			Vector3f vecSpinAdd = pData->m_vecNormal.Cross(-m_pDefCont2->GetMotion()*3);
			Vector3f& rAngVel = pBallController->GetAngularVelocity();
			rAngVel = -(rAngVel -vecSpinAdd);
		}
	}
		
	// Check if the ball and/or deflectors are colliding with any of the GameField walls
	vector<Quad> vWallQuads = m_pField->GetWallQuads();	
	int iWall = 0;

	for (vector<Quad>::const_iterator it = vWallQuads.begin();
		 it != vWallQuads.end(); ++it)
	{
		const Quad& rThisOne = *it;

		// Ball
		if (bBallSinking) break;
		pData = CollisionDetect::SphereAndQuad(oBall, rThisOne);
		if (pData)
		{
			pBallController->GetContactNormal().Set(pData->m_vecNormal);
			const SurfaceConfig& rWallConfig = (iWall <= 1) ?
				rFPSettings.GetMetal1Config() : rFPSettings.GetMetal2Config();

			// Bounce off the wall.
			Particle& rParticle = pBallController->GetParticle();
			m_oEventHandler.PushEvent( new BallCollisionEvent( 
				ParticleCollision(&rParticle, 
				NULL, rWallConfig.m_fRestitution, *pData) ) );
			
			
			Vector3f& rVel = rParticle.m_vecVel;
			Vector3f& rAngVel = pBallController->GetAngularVelocity();


			// Do some manual collision resolution concerning the spinning of the wall.
			if (!pBallController->IsRolling())
			{
				// Create the particle effect only for a single impact.
				pBall->FireParticles(*pData);

				GLfloat fRestitution = rWallConfig.m_fRestitution;
				
				// We add the velocity of the wall manually here.
				Vector3f vecWallVel = 
					Vector3f(0, 0, -1).Cross(pData->m_vecNormal) * rFPSettings.GetWallSpeed();
				rVel += vecWallVel * fRestitution;

				// Alter the spin based on the restitution and rotation of the wall.
				
				Vector3f vecAddSpin = Vector3f(0, 0, -1) * rFPSettings.GetGameFieldRotateRate();
				rAngVel += vecAddSpin / 5;
				rAngVel *= -fRestitution;
			}
			else
			{
				// Nullify any velocity in the direction of the contact normal.
				GLfloat fComp = rVel.Dot(pData->m_vecNormal);
				Vector3f vecUpVel = pData->m_vecNormal * fComp;
				rVel -= vecUpVel;

				// Make sure that the spin in the direction of movement is the same as that velocity.
				Vector3f vecDesiredRoll = -rVel.Cross(pData->m_vecNormal);
				vecDesiredRoll = (vecDesiredRoll * 10) / rFPSettings.GetBallCircumference();

				// Allow sideways spin too, but the component in the rolling direction must be correct.
				Vector3f vecRollToReplace = 
					vecDesiredRoll.GetUnit() *
					rAngVel.Dot(vecDesiredRoll.GetUnit());
				rAngVel -= vecRollToReplace;
				rAngVel += vecDesiredRoll;

				// Apply frictional force (works like damping).
				rVel *= pow(rWallConfig.m_fFriction, iTime);

			}

			// Keep track of when the ball is "rolling" (repeatedly colliding) on a surface.
			pBallController->Collide();
			

			delete pData;
		}

		++iWall;

	}
}

/**
 * Update function for the game events system, called whenever Update is called. For the time
 * being, events are just for resolution of collisions. In the future, if a game involving some 
 * form of AI was to be made, the events system would see further use for that purpose.
 */
void FPGame::UpdateEvents(int iTime)
{
	while (!m_oEventHandler.Empty())
	{
		Event* pEvent = m_oEventHandler.PopEvent();
		switch (pEvent->GetType())
		{
		case BALL_COLLISION_EVENT:
			{
			//cout << "Ball collision event!! Resolving.." << endl;
			BallCollisionEvent* pColEvent = (BallCollisionEvent*)pEvent;
			pColEvent->Resolve(iTime);
			break;
			}
		default:
			cerr << "Error: unimplemented event handled" << endl;
		}
	}
}

/**
 * UpdateGrabAction: the update code for the "grab" action of players, which allows them to pick
 * up and serve the ball.
 */
void FPGame::UpdateGrabAction(int iTime, int iBall)
{
	assert (iBall >= 0 && iBall < (int)m_vBalls.size() && m_vBalls.size() == m_vBallControllers.size());
	const FPSettings& rFPSettings = FPSettings::Get();
	Ball* pBall = m_vBalls[iBall];
	BallController* pBallController = m_vBallControllers[iBall];
	DeflectorSpringForce* pP1GrabForce = m_vP1GrabForces[iBall];
	DeflectorSpringForce* pP2GrabForce = m_vP2GrabForces[iBall];
	const FPSettings& rSettings = FPSettings::Get();	

	// See if the player is firing - we need to add a limited spring force if so.
	Particle& rBallParticle = pBallController->GetParticle();
	GLfloat fBallSpeed = rBallParticle.m_vecVel.Length();
	GLfloat fMaxDist = rSettings.GetGrabMaxDist();
	// We don't want to grab the ball so that it comes to a halt too far for us to move forward and hit it.
	GLfloat fP1RestZMin = 
		-(rSettings.GetGameFieldDistFromCam() + rSettings.GetDeflectorMaxZ());
	GLfloat fP2RestZMax = 
		-(rSettings.GetGameFieldDistFromCam() + rSettings.GetGameFieldLength() 
		- rSettings.GetDeflectorMaxZ());
	
	// XXX: Argh, code duplication! Deflectors could be an array and this would be half the size.
	// Do the same for deflector 1.
	bool bAttached = false;
	if (m_pDefCont1->GetFiring())
	{
		m_oForceManager.Remove(&rBallParticle, pP1GrabForce);
		GLfloat fRestZ = *m_pDef1->GetConstPosition().z - rFPSettings.GetServingZDist();
		if (!m_vP1SpringOn[iBall])
		{
			m_vP1SpringOn[iBall] = true;
			pP1GrabForce->SetRestZ(fRestZ);
			//cout << "new rest z: " << *m_pDef1->GetConstPosition().z - rFPSettings.GetServingZDist() << endl;
		}
		// We can grab the ball at further forward than we can move, but the ball will follow us 
		// back a bit so that we can hit it.
		if (pP1GrabForce->GetRestZ() < fP1RestZMin && fRestZ > pP1GrabForce->GetRestZ())
			pP1GrabForce->SetRestZ(fRestZ);
		
		m_oForceManager.Add(&rBallParticle, pP1GrabForce);
		bAttached = bAttached || (pP1GrabForce->InRange(rBallParticle.m_vecPos, fBallSpeed));

	}
	else
	{
		m_vP1SpringOn[iBall] = false;
		m_oForceManager.Remove(&rBallParticle, pP1GrabForce);
	}

	// Pretty much the same thing, but for the second deflector.
	if (m_pDefCont2->GetFiring())
	{
		m_oForceManager.Remove(&rBallParticle, pP2GrabForce);
		GLfloat fRestZ = *m_pDef2->GetConstPosition().z + rFPSettings.GetServingZDist();
		if (!m_vP2SpringOn[iBall])
		{
			m_vP2SpringOn[iBall] = true;
			pP2GrabForce->SetRestZ(fRestZ);
		}	
		if (pP2GrabForce->GetRestZ() > fP2RestZMax && fRestZ < pP2GrabForce->GetRestZ())
			pP2GrabForce->SetRestZ(fRestZ);


		m_oForceManager.Add(&rBallParticle, pP2GrabForce);
		bAttached = bAttached || (pP2GrabForce->InRange(rBallParticle.m_vecPos, fBallSpeed));

	}
	else// ((!m_pDefCont2->GetFiring() || pBall->GetDying()) && m_vP2SpringOn[iBall])
	{
		m_vP2SpringOn[iBall] = false;
		m_oForceManager.Remove(&rBallParticle, pP2GrabForce);
	}


	// Need to apply proper damping depending on whether it's attached or not.
	if (bAttached)
	{			
		rBallParticle.m_fDamping = rSettings.GetAttachedDamping();
		pBallController->GetAngularVelocity().Zero();
	}
	else
	{
		rBallParticle.m_fDamping = rSettings.GetBallDamping();
	}
}

/**
 * ResetDeflectorZ: resets the Z position of each deflector, for reattaching the ball to a player
 * to be served at the start of a round. this is done so that the ball doesn't hit the deflector 
 * immediately.
 */
void FPGame::ResetDeflectorZ()
{
	const FPSettings& rFPSettings = FPSettings::Get();

	GLfloat fDefOneStartZ =
		-rFPSettings.GetGameFieldDistFromCam();
	GLfloat fDefTwoStartZ =
		-(rFPSettings.GetGameFieldDistFromCam()+rFPSettings.GetGameFieldLength());

	*m_pDef1->GetPosition().z = fDefOneStartZ;
	*m_pDef2->GetPosition().z = fDefTwoStartZ;
}


/**
 * The constructor for FPGame. This initialises all the WorldEntities and Controllers in the game
 * world, passing into them the appropriate settings. It also calls Init() to set up OpenGL first,
 * and sets any member variables associated with the game itself such as scoring. This could have
 * all arguably been put into Init() and achieved the same thing, though I tried to keep Init as
 * a function pertaining mainly to OpenGL-related initialisation calls and the FPGame constructor
 * more for calls related to WorldEntities and Controllers.
 */
FPGame::FPGame()
: m_iP1Score(0)
, m_iP2Score(0)
, m_bP1LastScored(true)
, m_fRotation(0)
{
	// Init OpenGL settings and textures.
	Init();

	const FPSettings& rSettings = FPSettings::Get();

	// Set up the deflectors and their controllers.
	// Player One
	Vector3f vecDefOneStartPos (
		rSettings.GetDeflectorXStartPos(),
		rSettings.GetDeflectorYStartPos(),
		-rSettings.GetGameFieldDistFromCam() );
	Vector3f vecDefTwoStartPos (
		rSettings.GetDeflectorXStartPos(),
		rSettings.GetDeflectorYStartPos(),
		-(rSettings.GetGameFieldDistFromCam()+rSettings.GetGameFieldLength()) );
	const Vector3f& rDefSize = rSettings.GetDeflectorSize();

	m_pDef1 = 
		new Deflector(true, vecDefOneStartPos, rDefSize, rSettings.GetP1Colour());
	m_vEntities.push_back(m_pDef1);

	KbdDefController* pControllerOne =
		new KbdDefController(m_pDef1, true);
	m_vControllers.push_back(pControllerOne);
	m_pDefCont1 = (DefController*)pControllerOne;

	// Player one starts off grabbing the ball.
	pControllerOne->SetFiring(true);

	// Player two
	m_pDef2 =
		new Deflector(false, vecDefTwoStartPos, rDefSize, rSettings.GetP2Colour());
	m_vEntities.push_back(m_pDef2);

	KbdDefController* pControllerTwo =
		new KbdDefController(m_pDef2, false);
	m_vControllers.push_back(pControllerTwo);
	m_pDefCont2 = (DefController*)pControllerTwo;
	
	// Set up the balls and controllers.
	const Vector3f& rBallStartPos = rSettings.GetBallStartPos();
	Vector3f vecBall1StartPos (*rBallStartPos.x, *rBallStartPos.y, *rBallStartPos.z + 10);

	Ball* pBall = new Ball(vecBall1StartPos);
	m_vEntities.push_back(pBall);
	m_vBalls.push_back(pBall);
	BallController* pBallController =
		new BallController(pBall);
	m_vControllers.push_back(pBallController);
	m_vBallControllers.push_back(pBallController);
	
	m_vTimesTillRespawn.push_back(0);

	// Uncomment for a second ball - twice the fun!!!
	/*
	Vector3f vecBall2StartPos (*rBallStartPos.x, *rBallStartPos.y, *rBallStartPos.z - 10);

	*pBall = new Ball(vecBall2StartPos);
	m_vEntities.push_back(pBall);
	m_vBalls.push_back(pBall);
	pBallController =
		new BallController(pBall);
	m_vControllers.push_back(pBallController);
	m_vBallControllers.push_back(pBallController);

	m_vTimesTillRespawn.push_back(0);
	*/

	// Set up the game field.
	m_pField = new GameField(m_vBalls, m_iWaveShader);
	m_vEntities.push_back(m_pField);

	// Set up the physics.
	m_pGravityForce = new GravityForce( rSettings.GetGravityAccel() );
	for (unsigned int i = 0; i < m_vBallControllers.size(); ++i)
	{
		m_oForceManager.Add(&m_vBallControllers[i]->GetParticle(), m_pGravityForce);

		// Set up the grabbing springs, though we don't use them until activated.
		DeflectorSpringForce* pP1GrabForce = new DeflectorSpringForce(
			&m_pDef1->GetConstPosition(), rSettings.GetServingSpringConst(), 
			*m_pDef1->GetConstPosition().z - rSettings.GetServingZDist(), true, 
			rSettings.GetGrabMaxDist());
		DeflectorSpringForce* pP2GrabForce = new DeflectorSpringForce(
			&m_pDef2->GetConstPosition(), rSettings.GetServingSpringConst(), 
			*m_pDef2->GetConstPosition().z + rSettings.GetServingZDist(), false, 
			rSettings.GetGrabMaxDist());

		m_vP1GrabForces.push_back(pP1GrabForce);
		m_vP2GrabForces.push_back(pP2GrabForce);

		m_vP1SpringOn.push_back(false);
		m_vP1SpringOn.push_back(false);
		m_vP2SpringOn.push_back(false);
		m_vP2SpringOn.push_back(false);
	}
}


// Destructor for FPGame - all the destruction of entities and controllers are done by the
// superclass AbstractGame.
FPGame::~FPGame() 
{
	delete m_pGravityForce;
	for (unsigned int i = 0; i < m_vBalls.size(); ++i)
	{
		delete m_vP1GrabForces[i];
		delete m_vP2GrabForces[i];
	}

}

/**
 * Init() - this implements the Init() virtual abstract function in AbstractGame. It handles all
 * the calling of OpenGL stuff that only needs to be done once at the start of the game, as well
 * as any initialisation of different extra features like textures.
 */
void FPGame::Init()
{
	// Getting FPSettings also does any initialisation it needs to do.
	const FPSettings& rFPSettings = FPSettings::Get();

	/**
	 * Initialise all the OpenGL stuff
	 */
	cout << "Setting up OpenGL settings..." << endl;
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

	glEnable(GL_LIGHTING);
	//glEnable(GL_CULL_FACE);

	// Blending, textures, other bits
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_NORMALIZE);
	glEnable(GL_POLYGON_SMOOTH);

	/**
	 * Initialise textures from the texture config, just by getting the instance once.
	 */
	FPTextureManager::Get();	

}

/**
 * This is the main render loop of the game, implementing the Render() virtual abstract function
 * in AbstractGame. This is directly linked to the render function for GLUT, so couldn't really
 * have been done in any other way.
 */
void FPGame::Render()
{	
	RenderViewPortOne();
	RenderViewPortTwo();
	RenderHUD();
}

/**
 * The HUD - Heads Up Display - of the game is just the text that is shown in the corners of the
 * screen to describe the state of the game. It is kept here, as it is meant to be separate in
 * purpose from the drawing of the actual game field. This code could have been put in Render() or
 * one of the RenderViewPort functions and achieved the same result.
 */
void FPGame::RenderHUD()
{
	const FPSettings& rFPSettings = FPSettings::Get();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glLoadIdentity();
	glColor3f(1.0f, 1.0f, 1.0f);

	int iWinner = GetWinner();
	if (iWinner)
	{
		if (iWinner == 1)
			PrintText(0.00f, -2.85f, -9.0f, "Player 1 is the winner!");
		else if (iWinner == 2)
			PrintText(0.00f, -2.85f, -9.0f, "Player 2 is the winner!");
	}
	else
	{
		stringstream sout;

		sout << "Player 1 score: " << m_iP1Score;
		string strP1Score = sout.str();
		sout.str(""); sout.clear();
		sout << "Player 2 score: " << m_iP2Score;
		string strP2Score = sout.str();
		sout.str(""); sout.clear();
		
		PrintText(0.5f, 2.75f, -8.0f, strP1Score);
		PrintText(0.5f, 2.50f, -8.0f, strP2Score);

		GLfloat fYPos = -2.85f;
		GLfloat fIncrement = -0.25f;
		for (unsigned int i = 0; i < m_vBallControllers.size(); ++i)
		{
			if (m_vTimesTillRespawn[i] > 0)
			{	
				GLfloat fZMin = rFPSettings.GetBallMinZ();
				GLfloat fZMax = rFPSettings.GetBallMaxZ();

				
				if (*m_vBalls[i]->GetPosition().z <= fZMin)
					PrintText(0.5f, fYPos, -8.0f, "Player 1 scores!");
				else// if (*m_pBall->GetPosition().z >= fZMax)
					PrintText(0.5f, fYPos, -8.0f, "Player 2 scores!");
			}
			fYPos += fIncrement;
		}

	}

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
}

/**
 * Do all the light in the game, which does not change depending on the viewport being
 * drawn. What we basically do here is get the pertinent LightConfig references from FPSettings
 * and switch them on, though one niggle is that we don't want to draw the ball's light if it
 * is sinking into the lava. It is arguably not completely necessary to split this out from the
 * Render function, as it is only called once; it evolved from earlier versions where there was
 * only one light whose position depended on the viewport being drawn.
 */
void FPGame::DoLight()
{
	const FPSettings& rFPSettings = FPSettings::Get();

	// Light for lava behind player one
	rFPSettings.GetP1LavaLight().SwitchOn();
	// Light for lava behind player two
	rFPSettings.GetP2LavaLight().SwitchOn();
	// Light for the ball(s), if not sinking.
	if (m_vTimesTillRespawn[0] <= 0)
	{
		LightConfig lsBall1 = rFPSettings.GetBallLight1();
		lsBall1.SetPosition(m_vBalls[0]->GetPosition());
		lsBall1.SwitchOn();
	}
	// XXX: Yeah I know this isn't nice. Needs a refactor of how
	// lights work (see comment above LightConfig)
	if (m_vTimesTillRespawn.size() > 1)
	{
		if (m_vTimesTillRespawn[1] <= 0)
		{
			LightConfig lsBall2 = rFPSettings.GetBallLight2();
			lsBall2.SetPosition(m_vBalls[1]->GetPosition());
			lsBall2.SwitchOn();
		}
	}
}

/**
 * RenderViewPortOne - renders the game from the point of view of the first player. 
 */
void FPGame::RenderViewPortOne()
{
	const FPSettings& rFPSettings = FPSettings::Get();
	const GameSettings& rSettings = GameSettings::Get();
	GLfloat fWidth = rSettings.GetXResolution();
	GLfloat fHeight = rSettings.GetYResolution();

    glViewport(1, 1, (fWidth / 2) - 2, (fHeight - 2));

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();       
    gluPerspective(45, 1, 0.1f, 1000.0f);
    glMatrixMode(GL_MODELVIEW); 
	glLoadIdentity();
    gluLookAt(*rFPSettings.GetP1Eye().x, *rFPSettings.GetP1Eye().y, *rFPSettings.GetP1Eye().z,
			  *rFPSettings.GetP1Centre().x, *rFPSettings.GetP1Centre().y, *rFPSettings.GetP1Centre().z,
			  *rFPSettings.GetP1Up().x, *rFPSettings.GetP1Up().y, *rFPSettings.GetP1Up().z);

	// Do the lights.
	DoLight();
	
	// Render all of our entities - opaque stuff first so that transparency works.
	for (unsigned int i = 0; i < m_vBalls.size(); ++i)
	{
		m_vBalls[i]->Render();
	}	
	m_pField->SetVisibleLava(false);
	m_pField->Render();
	m_pDef2->Render();
	m_pDef1->Render();	
}

/**
 * RenderViewPortTwo - works exactly like RenderViewPortOne, except that this viewport appears
 * on the right from the perspective of the second player.
 */
// XXX: This is an almost complete duplication of RenderViewPortOne.
// If P1/P2 members were structured as arrays/objects, this could easily
// be a single function called once for each player.
void FPGame::RenderViewPortTwo()
{
	const FPSettings& rFPSettings = FPSettings::Get();
	const GameSettings& rSettings = GameSettings::Get();
	GLfloat fWidth = rSettings.GetXResolution();
	GLfloat fHeight = rSettings.GetYResolution();

	glViewport((fWidth / 2) + 1, 1, (fWidth / 2) - 2, (fHeight - 2));

	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, 1, 0.1f, 1000.0f);    
    glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    gluLookAt(*rFPSettings.GetP2Eye().x, *rFPSettings.GetP2Eye().y, *rFPSettings.GetP2Eye().z,
			  *rFPSettings.GetP2Centre().x, *rFPSettings.GetP2Centre().y, *rFPSettings.GetP2Centre().z,
			  *rFPSettings.GetP2Up().x, *rFPSettings.GetP2Up().y, *rFPSettings.GetP2Up().z);

	// Do the lights.
	DoLight();

	// Render all of our entities.
	for (unsigned int i = 0; i < m_vBalls.size(); ++i)
	{
		m_vBalls[i]->Render();
	}	
	m_pField->SetVisibleLava(true);
	m_pField->Render();
	m_pDef1->Render();
	m_pDef2->Render();
	
}

/**
 * Main update loop for the game; this updates any Controller, i.e. any class that has an Update
 * function that depends on the time that has passed since the last update was called. Some things
 * that need to be checked for the overall game are done in UpdateCollisions, and the GameField
 * itself also has an update function.
 */
void FPGame::Update(int iTime)
{
	//cout << "no forces: " << m_oForceManager.GetString() << endl;
	const FPSettings& rSettings = FPSettings::Get();

	assert(m_vBallControllers.size() == m_vBalls.size());

	// Do the updates for each ball.
	for (unsigned int i = 0; i < m_vBallControllers.size(); ++i)
	{
		Ball* pBall = m_vBalls[i];
		BallController* pBallController = m_vBallControllers[i];

		// Count down to respawn.
		if (m_vTimesTillRespawn[i] > 0)
		{
			//cout << "time till respawn: " << m_iTimeTillRespawn << endl;
			m_vTimesTillRespawn[i] -= iTime;
			// Reposition the ball if that was the last decrement.
			if (m_vTimesTillRespawn[i] <= 0)
			{
				m_vTimesTillRespawn[i] = 0;
				pBallController->ResetBall();
			}
		}

		pBall->SetMarkerRotation(m_fRotation);

		// Update the grabbing (fire effects) for each player.	
		UpdateGrabAction(iTime, i);
	}
	

	// Increment the rotation of the game field.
	m_fRotation = (m_fRotation + rSettings.GetGameFieldRotateRate());
	if (m_fRotation >= 360) m_fRotation -= 360;
	assert(m_fRotation < 360);

	m_pField->SetRotation(m_fRotation);
	
	m_pDef1->SetMarkerRotation(m_fRotation);
	m_pDef2->SetMarkerRotation(m_fRotation);

	// Update the forces.
	m_oForceManager.Update(iTime);
	
	// Update all of our controllers.
	for (vector<AbstractController*>::iterator it = m_vControllers.begin();
			 it != m_vControllers.end(); it++)
	{
		(*it)->Update(iTime);
	}

	// Update the field itself, which handles particles and other stuff.
	m_pField->Update(iTime);

	// Check collisions.
	UpdateCollisions(iTime);
	UpdateEvents(iTime);

}

/**
 * Callback function for when the mouse is moved with a button pressed. This fits with the 
 * AbstractGame design in that it loops through the provided vector of controllers and checks 
 * if any of them take mouse input.
 */
void FPGame::ActiveMouseMotion(int iX, int iY)
{
	// Send the mouse input to any mouse controllers.
	for (vector<AbstractController*>::iterator it = m_vControllers.begin();
		 it != m_vControllers.end(); it++)
	{
		if ((*it)->GetType() == MOUSE_CONTROLLER)
		{
			MouseDefController* pController = (MouseDefController*)(*it);
			pController->ActiveMouseMotion(iX, iY);
		}
	}
}

/**
 * Callback function for when the mouse is moved without a button pressed. This fits with the 
 * AbstractGame design in that it loops through the provided vector of controllers and checks 
 * if any of them take mouse input.
 */
void FPGame::PassiveMouseMotion(int iX, int iY)
{
	// As above, send the mouse input to the mouse controllers.
	for (vector<AbstractController*>::iterator it = m_vControllers.begin();
		 it != m_vControllers.end(); it++)
	{
		if ((*it)->GetType() == MOUSE_CONTROLLER)
		{
			MouseDefController* pController = (MouseDefController*)(*it);
			pController->PassiveMouseMotion(iX, iY);
		}
	}
}

/**
 * Callback function for when a key is pressed in the game. This fits with the AbstractGame
 * design in that it loops through the provided vector of controllers and checks if any of them
 * take keyboard controls.
 */
void FPGame::KeyDown(int iKey)
{
	const FPSettings& rFPSettings = FPSettings::Get();

	// Send the keyboard input to any keyboard controllers.
	for (vector<AbstractController*>::iterator it = m_vControllers.begin();
		 it != m_vControllers.end(); it++)
	{
		if ((*it)->GetType() == KEYBOARD_CONTROLLER)
		{
			KbdDefController* pController = (KbdDefController*)(*it);
			pController->KeyDown(iKey);
		}
	}
	
}

/**
 * Callback function for when a key is lifted in the game. This fits with the AbstractGame
 * design in that it loops through the provided vector of controllers and checks if any of them
 * take keyboard controls.
 */
void FPGame::KeyUp(int iKey)
{
	// Send the keyboard input to any keyboard controllers.
	for (vector<AbstractController*>::iterator it = m_vControllers.begin();
		 it != m_vControllers.end(); it++)
	{
		if ((*it)->GetType() == KEYBOARD_CONTROLLER)
		{
			KbdDefController* pController = (KbdDefController*)(*it);
			pController->KeyUp(iKey);
		}
	}
}
