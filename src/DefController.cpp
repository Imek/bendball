#include "DefController.h"

/**
 * MouseDefController code
 */

/**
 * ActiveMouseMotion(int iX, int iY) - callback function for active mouse motion, i.e. motion of
 * the mouse when a button is pressed. iX and iY mark the absolute position of the mouse pointer.
 * Because of how the game is linked with GLUT, this is the most intuitive way to have the controls 
 * work.
 *
 * The mouse controller class hasn't been updated in ages, and likely needs a lot of modification to
 * get working with the new game.
 */
void MouseDefController::ActiveMouseMotion(int iX, int iY)
{
	const GameSettings& rSettings = GameSettings::Get();
	const FPSettings& rFPSettings = FPSettings::Get();

	GLfloat fFieldWidth = rFPSettings.GetGameFieldWidth()/2;
	GLfloat fFieldHeight = rFPSettings.GetGameFieldHeight();

	GLfloat fX = iX * fFieldWidth / rSettings.GetXResolution() - fFieldWidth/2;
	GLfloat fY = -iY * fFieldHeight / rSettings.GetYResolution() + fFieldHeight/2;

	// Move our target position to the appropriate position.
	*m_vecTargetPos.x = fX;
	*m_vecTargetPos.y = fY;
}

/**
 * PassiveMouseMotion(int iX, int iY) - callback for passive mouse motion, i.e. motion of the mouse
 * with no buttons pressed. Because of the nature of the controls, this just calls the active
 * motion function with the given arguments.
 */
void MouseDefController::PassiveMouseMotion(int iX, int iY)
{
	// Same as active
	ActiveMouseMotion(iX, iY);
}

/**
 * Update(int iTime) - update function for the deflector controller, called every time the main
 * Update function is called. Although the position of the deflector could have been set
 * directly via the mouse motion function, the use of an update function allows the deflector
 * to have a maximum speed and move towards wherever the target (mouse pointer) is at some preset
 * maximum speed.
 */
void MouseDefController::Update(int iTime)
{
	DefController::Update(iTime); // update superclass

	const FPSettings& rFPSettings = FPSettings::Get();

	// Move the actual position of the deflector towards our target position.
	Vector3f& rDefPos = m_pControlled->GetPosition();
	Vector3f vecDiff = m_vecTargetPos - rDefPos;

	GLfloat fDist = vecDiff.Length();
	GLfloat fMaxDist = rFPSettings.GetDeflectorSpeed() * ((GLfloat) iTime / 1000);

	if (fDist <= fMaxDist)
	{
		// We can do the move in this update, so just do it.
		rDefPos += vecDiff;
	}
	else
	{
		// Move as far as we can.
		Vector3f vecMaxDiff = vecDiff.GetUnit() * fMaxDist;
		rDefPos += vecMaxDiff;
	}

	// Make sure it doesn't go over the edge.
	GLfloat fMaxX = rFPSettings.GetDeflectorMaxX();
	GLfloat fMinX = rFPSettings.GetDeflectorMinX();
	GLfloat fMaxY = rFPSettings.GetDeflectorMaxY();
	GLfloat fMinY = rFPSettings.GetDeflectorMinY();	
	if (*rDefPos.x > fMaxX)	*rDefPos.x = fMaxX;
	if (*rDefPos.x < fMinX)	*rDefPos.x = fMinX;
	if (*rDefPos.y > fMaxY)	*rDefPos.y = fMaxY;
	if (*rDefPos.y < fMinY)	*rDefPos.y = fMinY;
}


/**
 * KbdDefController code
 */

// Handle collisions with the walls, making sure we rebound.
void KbdDefController::UpdateRebound()
{
	if (m_oCollisionCont.m_pContNormLastFrame &&
		m_oCollisionCont.m_pContNormThisFrame)
	{
		Vector3f vecAverage = Vector3f::MidPoint(
			*m_oCollisionCont.m_pContNormLastFrame,
			*m_oCollisionCont.m_pContNormThisFrame);

		if (*vecAverage.x > 0)
		{
			m_bRightRebound = m_bP1;
			m_bLeftRebound = !m_bP1;
		}
		else if (*vecAverage.x < 0)
		{
			m_bLeftRebound = m_bP1;
			m_bRightRebound = !m_bP1;
		}

		if (*vecAverage.y > 0)
		{
			m_bUpRebound = true;
			m_bDownRebound = false;
		}
		else if (*vecAverage.y < 0)
		{
			m_bDownRebound = true;
			m_bUpRebound = false;
		}

	}
	else
	{
		 m_bUpRebound = m_bLeftRebound = m_bDownRebound = m_bRightRebound = false;
	}
}



/**
 * KbdDefController constructor - instantiate a keyboard deflector controller, with the given
 * set of key configurations. Generally, between zero and two of these should be set (depending
 * on what other types of DefController are wanted/available)
 */
KbdDefController::KbdDefController(Deflector* pControlled, bool bP1)
: DefController(KEYBOARD_CONTROLLER, pControlled), m_bP1(bP1)
, m_bUp(false), m_bLeft(false), m_bDown(false)
, m_bRight(false), m_bBack(false), m_bForward(false), m_bFireHeld(false)
, m_bUpRebound(false), m_bLeftRebound(false), m_bDownRebound(false), m_bRightRebound(false)
{
	const FPSettings& rFPSettings = FPSettings::Get();

	// Set controls based on which player we are.
	if (m_bP1)
	{
		m_iUpKey = rFPSettings.GetP1UpKey();
		m_iDownKey = rFPSettings.GetP1DownKey();
		m_iLeftKey = rFPSettings.GetP1LeftKey();
		m_iRightKey = rFPSettings.GetP1RightKey();
		m_iBackKey = rFPSettings.GetP1BackKey();
		m_iForwardKey = rFPSettings.GetP1ForwardKey();
		m_iFireKey = rFPSettings.GetP1FireKey();
	}
	else // P2
	{
		m_iUpKey = rFPSettings.GetP2UpKey();
		m_iDownKey = rFPSettings.GetP2DownKey();
		m_iLeftKey = rFPSettings.GetP2LeftKey();
		m_iRightKey = rFPSettings.GetP2RightKey();
		m_iBackKey = rFPSettings.GetP2BackKey();
		m_iForwardKey = rFPSettings.GetP2ForwardKey();
		m_iFireKey = rFPSettings.GetP2FireKey();
	}
}

/**
 * KeyDown(int iKey) - callback function for keyboard control, called when GLUT detects keyboard
 * input. Given how GLUT works, this is the only real way to handle keyboard input.
 */
void KbdDefController::KeyDown(int iKey)
{
	// TODO: If these key configs were const, this could be a switch statement.
	// Needs an improvement of the construction based on which player this is.
	if(iKey == m_iUpKey)
		m_bUp = true;
	else if(iKey == m_iLeftKey)
		m_bLeft = true;
	else if(iKey == m_iDownKey)
		m_bDown = true;
	else if(iKey == m_iRightKey)
		m_bRight = true;
	else if(iKey == m_iBackKey)
		m_bBack = true;
	else if(iKey == m_iForwardKey)
		m_bForward = true;

	// Fire key is a toggle
	// NOTE: Manual grabber control currently turned off since it's kind of game-breaking.
	/*if(iKey == m_iFireKey && !m_bFireHeld)
	{
		m_bFiring = !m_bFiring;
		m_bFireHeld = true;
	}*/
}

/**
 * KeyUp(int iKey) - callback function for GLUT keyboard control when a key is lifted, generally
 * switching off some specific action or condition. See above.
 */
void KbdDefController::KeyUp(int iKey)
{
	if(iKey == m_iUpKey)
		m_bUp = false;
	if(iKey == m_iLeftKey)
		m_bLeft = false;
	if(iKey == m_iDownKey)
		m_bDown = false;
	if(iKey == m_iRightKey)
		m_bRight = false;
	if(iKey == m_iBackKey)
		m_bBack = false;
	if(iKey == m_iForwardKey)
		m_bForward = false;
	
	// NOTE: Manual grabber control currently turned off since it's kind of game-breaking.
	//if(iKey == m_iFireKey)
	//	m_bFireHeld = false;
}

/**
 * Update(int iTime) - update function for the keyboard deflector controller, called every time
 * the main game loop updates. Handles fire controls and resolution of the keyboard input into
 * a motion vector for the deflector.
 */
void KbdDefController::Update(int iTime)
{
	const FPSettings& rFPSettings = FPSettings::Get();

	UpdateRebound();

	DefController::Update(iTime); // Call superclass
	
	// Update speed from acceleration
	if (m_bBack || m_bForward || m_bUp || m_bDown || m_bLeft || m_bRight)
	{
		m_fSpeed += m_fAccel;
		if (m_fSpeed > m_fMaxSpeed) m_fSpeed = m_fMaxSpeed;
	}
	else
	{
		m_fSpeed = 0;
	}	

	// Get the velocity and work with it.
	Vector3f vecMotion = GetMotion() * iTime;
	m_vecTargetPos = vecMotion + m_pControlled->GetConstPosition();

	// Add it to the position of the controlled object.
	Vector3f& rPos = m_pControlled->GetPosition();
	rPos += vecMotion;

	if (*rPos.z < m_fZMin)	*rPos.z = m_fZMin;
	if (*rPos.z > m_fZMax)	*rPos.z = m_fZMax;

	// Finally, update the firing amount.
	Deflector* pControlled = (Deflector*)m_pControlled;
	if (m_bFiring)
	{
		GLfloat fGlowRate = rFPSettings.GetDefGlowRate();
		GLfloat fMaxGlow = rFPSettings.GetDefMaxGlow();
		GLfloat fDefGlow = pControlled->GetGlow();
		fDefGlow += fGlowRate * iTime;
		if (fDefGlow > fMaxGlow) fDefGlow = fMaxGlow;
		pControlled->SetGlow(fDefGlow);
	}
	else
	{
		pControlled->SetGlow(0);
	}

}

/**
 * GetMotion() - Work out the motion for the deflector, based on the keyboard input.
 * This is required to be public so that spin can be calculated on the ball when it impacts with
 * the deflector. It could have been a reasonable idea to make this a "CalculateSpin" function
 * and move code from BallController to DefController, but either way it would have resulted in
 * the two classes being coupled. Some code could have been moved up the hierarchy into FPGame,
 * but as GetMotion is already necessary for controlling the deflector itself this seemed the best
 * way.
 */
Vector3f KbdDefController::GetMotion()
{
	const FPSettings& rFPSettings = FPSettings::Get();
	Vector3f vecMotion;

	// First, tell the deflector what engines are on.
	Deflector* pControlled = (Deflector*)m_pControlled;
	pControlled->SetEngineR(m_bLeft);
	pControlled->SetEngineL(m_bRight);
	pControlled->SetEngineB(m_bUp);
	pControlled->SetEngineT(m_bDown);

	// Motion starts off as zero. Then, take the sum of all keys that are down.
	if (m_bUp)
		vecMotion += Vector3f (0, 1, 0);
	if (m_bLeft)
		vecMotion += Vector3f (m_bP1 ? -1 : 1, 0, 0);
	if (m_bDown)
		vecMotion += Vector3f(0, -1, 0);
	if (m_bRight)
		vecMotion += Vector3f(m_bP1 ? 1 : -1, 0, 0);
	if (m_bBack)
		vecMotion += Vector3f(0, 0, m_bP1 ? 1 : -1);
	if (m_bForward)
		vecMotion += Vector3f(0, 0, m_bP1 ? -1 : 1);
	// Then make the length of the vector our desired speed.
	if (m_fSpeed > m_fMaxSpeed) m_fSpeed = m_fMaxSpeed;
	vecMotion.Normalise();
	vecMotion *= m_fSpeed;

	// Apply rebounding independent of speed.
	if (m_bUpRebound || m_bLeftRebound || m_bDownRebound || m_bRightRebound)
	{
		if (m_bUpRebound)
			vecMotion += Vector3f (0, 1, 0);
		if (m_bLeftRebound)
			vecMotion += Vector3f (m_bP1 ? -1 : 1, 0, 0);
		if (m_bDownRebound)
			vecMotion += Vector3f(0, -1, 0);
		if (m_bRightRebound)
			vecMotion += Vector3f(m_bP1 ? 1 : -1, 0, 0);
		vecMotion.Normalise();
		vecMotion *= 0.005f;
	}

	return vecMotion;
}
