/**
 * Core.h - this header file specifies both the main acces point to the program, and the interface
 * between it and GLUT. The objective here is to separate the details of dealing with GLUT from the
 * workings of our own game classes.
 *
 * Author: Joe Forster
 */

#ifndef CORE_H
#define CORE_H

#include "FPGame.h"
#include <ctime>


// Our game.
FPGame* pGame;

// reshape: adjust the size of the viewing area to that of the new window size.
void reshape(int iWidth, int iHeight);
// render(): refresh the contents of the window.
void render();
// update(): provided as the timer function for glut
void update(int iVal);
// keyboard callback functions
void keyDown(unsigned char iKey, int iX, int iY);
void keyUp(unsigned char iKey, int iX, int iY);
// mouse callback functions
void mouse(int iButton, int iState, int iX, int iY);
void activeMotion(int iX, int iY); 
void passiveMotion(int iX, int iY);

// Main function.
int main(int argc, char **argv);

#endif /* CORE_H */
