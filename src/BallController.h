#ifndef BALLCONTROLLER_H
#define BALLCONTROLLER_H

#include "Engine/Controller.h"
#include "Engine/Particles.h"
#include "Ball.h"

/**
 * The BallController manages the physical simulation of ball movement based on when it collides
 * with the sides and with the deflectors. It also works with various effects like flashing on
 * impact and updating the particle system. Another thing handled is "spin" on the ball, which
 * occurs when the deflector moves upon impacting the ball. 
 *
 * A point worth noting here is that the ball controller should ideally be simulated as a Particle
 * in a proper physics engine; this method is somewhat more basic, and does not include spinning
 * and collisions as realistic as they could be. A good future extension would be to expand the
 * Particle class (possibly moving that into a Physics.h file), and try to make it so that a
 * BallController can be completely replaced by an appropriately-set Particle. Another point is 
 * that collision detection is very primitive in here, and a proper collision/bouncing system with
 * bounding volumes and spin calculation would be another desirable feature of a physics system.
 *
 */
class BallController: public AbstractController
{
private:
	// The particle for this ball, simulated by the physics system.
	Particle m_oParticle;

	// Starting position for the ball to reset to.
	Vector3f m_vecStartPos;

	// Stuff for flashing the ball
	int m_iFlashTime;
	GLfloat m_fFlashRate;
	GLuint m_iOrigFlashTime;
	Colour4f m_colFlashMax;

	// Angular velocity for the ball (orientation is kept in the Ball class).
	Vector3f m_vecAngularVel; // Direction is axis, magnitude is rate.
	GLfloat m_fAngularDamping;	
	bool m_bCollidedLast;
	bool m_bCollidedThis;

	bool m_bRolling;
	Vector3f m_vecContactNormal;

	// Update the flashing/glowing effect on the ball.
	void UpdateFlash(int iTime);

public:
	BallController(Ball* pControlled);

	// Do motion
	void Update(int iTime);
	
	// Kill the ball
	void Kill();
	// Reset the ball to the waiting mode in the middle of the field.
	void ResetBall();
	// Flash the ball for a given amount of time towards the given colour.
	void Flash(GLuint iTime, GLfloat fFlashRate, const Colour4f& colMax);
	
	Vector3f& GetAngularVelocity() { return m_vecAngularVel; }

	Particle& GetParticle() { return m_oParticle; }

	// Tell the ball we hit in this turn.
	void Collide() { m_bCollidedThis = true; }
	// See if the ball is rolling on a surface.
	bool IsRolling() const { return m_bRolling; }
	Vector3f& GetContactNormal() { return m_vecContactNormal; }
};

#endif
