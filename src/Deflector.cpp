#include "Deflector.h"

#include "FPSettings.h"

/**
 * Deflector code
 */

/**
 * The Init() function for the Deflector class sets up the display lists for the deflector.
 * This was moved from the constructor in an effort to split up clutter of code.
 */
void Deflector::Init()
{
	const FPSettings& rFPSettings = FPSettings::Get();

	// The position of the deflector is its middle point.
	GLfloat fHalfX = m_fXSize/2;
	GLfloat fHalfY = m_fYSize/2;
	GLfloat fDiff = m_fXSize/10;
	GLfloat fBackDiff = m_fXSize/14;
	GLfloat fBackInDiff = fDiff - fBackDiff;
	// Values for engine positions
	GLfloat fThirdOY = m_fYSize/3;
	GLfloat fThirdIY = (m_fYSize-fBackDiff*2)/3;	
	Vector3f vecToBack(0, 0, m_fZSize);
	// Lots and lots of vectors for the corners of the shape.
	Vector3f vecFTLOuter (-fHalfX, fHalfY, 0);
	Vector3f vecFTROuter (fHalfX, fHalfY, 0);
	Vector3f vecFBROuter (fHalfX, -fHalfY, 0);
	Vector3f vecFBLOuter (-fHalfX, -fHalfY, 0);
	Vector3f vecFTLInner (-fHalfX+fDiff, fHalfY-fDiff, 0);
	Vector3f vecFTRInner (fHalfX-fDiff, fHalfY-fDiff, 0);
	Vector3f vecFBRInner (fHalfX-fDiff, -fHalfY+fDiff, 0);
	Vector3f vecFBLInner (-fHalfX+fDiff, -fHalfY+fDiff, 0);
	Vector3f vecBTLOuter (*vecFTLOuter.x+fBackDiff, *vecFTLOuter.y-fBackDiff, *vecFTLOuter.z+m_fZSize);
	Vector3f vecBTROuter (*vecFTROuter.x-fBackDiff, *vecFTROuter.y-fBackDiff, *vecFTROuter.z+m_fZSize);
	Vector3f vecBBROuter (*vecFBROuter.x-fBackDiff, *vecFBROuter.y+fBackDiff, *vecFBROuter.z+m_fZSize);
	Vector3f vecBBLOuter (*vecFBLOuter.x+fBackDiff, *vecFBLOuter.y+fBackDiff, *vecFBLOuter.z+m_fZSize);
	Vector3f vecBTLInner (vecFTLInner+vecToBack);
	Vector3f vecBTRInner (vecFTRInner+vecToBack);
	Vector3f vecBBRInner (vecFBRInner+vecToBack);
	Vector3f vecBBLInner (vecFBLInner+vecToBack);

	// Set up the display lists: a metal frame with a forcefield thingy	
	m_iFrameDL = glGenLists(6);
	m_iFieldDL = m_iFrameDL+1;
	m_iLEngineDL = m_iFieldDL+1;
	m_iREngineDL = m_iLEngineDL+1;
	m_iTEngineDL = m_iREngineDL+1;
	m_iBEngineDL = m_iBEngineDL+1;
	

	// Make the list
	glNewList(m_iFrameDL, GL_COMPILE);		

		// Bind the texture (loaded in the Init method of FPGame) and set the colour
		glBindTexture(GL_TEXTURE_2D, FPTextureManager::Get().GetTexture(DEF_METAL));
		glColor4f(0.7f, 0.7f, 0.7f, 1.0f);
		
		// Make the model.
		glBegin(GL_QUADS);

			// Front faces
			glNormal3f(0, 0, -1);
			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFTLOuter.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3f(*vecFTLOuter.x+fDiff, *vecFTLOuter.y, *vecFTLOuter.z);
			glTexCoord2f(1.0f, 0.0f); glVertex3f(*vecFBLOuter.x+fDiff, *vecFBLOuter.y, *vecFBLOuter.z);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecFBLOuter.arrVals);

			glTexCoord2f(0.0f, 1.0f); glVertex3f(*vecFTLOuter.x+fDiff, *vecFTLOuter.y, *vecFTLOuter.z);
			glTexCoord2f(1.0f, 1.0f); glVertex3fv(vecFTROuter.arrVals);
			glTexCoord2f(1.0f, 0.0f); glVertex3f(*vecFTROuter.x, *vecFTROuter.y-fDiff, *vecFTROuter.z);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecFTLInner.arrVals);

			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFTRInner.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3f(*vecFTRInner.x+fDiff, *vecFTRInner.y, *vecFTRInner.z);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecFBROuter.arrVals);
			glTexCoord2f(0.0f, 0.0f); glVertex3f(*vecFBROuter.x-fDiff, *vecFBROuter.y, *vecFBROuter.z);

			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFBRInner.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3f(*vecFBRInner.x, *vecFBRInner.y-fDiff, *vecFBRInner.z);
			glTexCoord2f(0.0f, 0.0f); glVertex3f(*vecFBLInner.x, *vecFBLInner.y-fDiff, *vecFBLInner.z);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecFBLInner.arrVals);

			// Back faces
			glNormal3f(0, 0, 1);
			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecBTLOuter.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3f(*vecBTLOuter.x+fBackInDiff, *vecBTLOuter.y, *vecBTLOuter.z);
			glTexCoord2f(1.0f, 0.0f); glVertex3f(*vecBBLOuter.x+fBackInDiff, *vecBBLOuter.y, *vecBBLOuter.z);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecBBLOuter.arrVals);

			glTexCoord2f(0.0f, 1.0f); glVertex3f(*vecBTLOuter.x+fBackInDiff, *vecBTLOuter.y, *vecBTLOuter.z);
			glTexCoord2f(1.0f, 1.0f); glVertex3fv(vecBTROuter.arrVals);
			glTexCoord2f(1.0f, 0.0f); glVertex3f(*vecBTRInner.x+fBackInDiff, *vecBTRInner.y, *vecBTRInner.z);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecBTLInner.arrVals);

			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecBTRInner.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3f(*vecBTRInner.x+fBackInDiff, *vecBTRInner.y, *vecBTRInner.z);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecBBROuter.arrVals);
			glTexCoord2f(0.0f, 0.0f); glVertex3f(*vecBBROuter.x-fBackInDiff, *vecBBROuter.y, *vecBBROuter.z);

			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecBBRInner.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3f(*vecBBRInner.x, *vecBBRInner.y-fBackInDiff, *vecBBRInner.z);
			glTexCoord2f(0.0f, 0.0f); glVertex3f(*vecBBLInner.x, *vecBBLInner.y-fBackInDiff, *vecBBLInner.z);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecBBLInner.arrVals);

			// Outer side faces
			glNormal3f(-1, 0, 1);
			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFTLOuter.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3fv(vecBTLOuter.arrVals);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecBBLOuter.arrVals);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecFBLOuter.arrVals);
			
			glNormal3f(0, 1, 1);
			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFTLOuter.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3fv(vecBTLOuter.arrVals);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecBTROuter.arrVals);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecFTROuter.arrVals);

			glNormal3f(1, 0, 1);
			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFTROuter.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3fv(vecBTROuter.arrVals);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecBBROuter.arrVals);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecFBROuter.arrVals);

			glNormal3f(0, -1, 1);
			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFBLOuter.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3fv(vecBBLOuter.arrVals);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecBBROuter.arrVals);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecFBROuter.arrVals);

			// Inner side faces
			glNormal3f(1, 0, 0);
			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFTLInner.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3fv(vecBTLInner.arrVals);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecBBLInner.arrVals);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecFBLInner.arrVals);

			glNormal3f(0, -1, 0);
			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFTLInner.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3fv(vecBTLInner.arrVals);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecBTRInner.arrVals);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecFTRInner.arrVals);

			glNormal3f(-1, 0, 0);
			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFTRInner.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3fv(vecBTRInner.arrVals);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecBBRInner.arrVals);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecFBRInner.arrVals);

			glNormal3f(0, 1, 0);
			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFBRInner.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3fv(vecBBRInner.arrVals);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecBBLInner.arrVals);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecFBLInner.arrVals);

		glEnd();

	glEndList();




				

	// A separate list for the forcefield (need to specify colour and texture in Update() )
	glNewList(m_iFieldDL, GL_COMPILE);

		glBegin(GL_QUADS);

			glNormal3f(0, 0, -1);
			glTexCoord2f(1.0f, 0.0f); glVertex3fv(vecFTLInner.arrVals);
			glTexCoord2f(1.0f, 1.0f); glVertex3fv(vecFTRInner.arrVals);
			glTexCoord2f(0.0f, 1.0f); glVertex3fv(vecFBRInner.arrVals);
			glTexCoord2f(0.0f, 0.0f); glVertex3fv(vecFBLInner.arrVals);

		glEnd();

	glEndList();


	// Vertices for the engines.
	// Left engine: other engines are just rotations of this one.
	Vector3f vecLOFTop (*vecFTLOuter.x, *vecFTLOuter.y - fThirdOY, *vecFTLOuter.z);
	Vector3f vecLOFBottom (*vecFTLOuter.x, *vecFTLOuter.y - fThirdOY*2, *vecFTLOuter.z);
	Vector3f vecLOBTop (*vecBTLOuter.x, *vecLOFTop.y, *vecBTLOuter.z);
	Vector3f vecLOBBottom (*vecBTLOuter.x, *vecLOFBottom.y, *vecBTLOuter.z);
	Vector3f vecLEngRTop (*vecLOFTop.x, *vecLOFTop.y, *vecLOBTop.z);
	Vector3f vecLEngRBottom (*vecLOFBottom.x, *vecLOFBottom.y, *vecLOBBottom.z);

	// Display lists for the engines.

	glNewList(m_iLEngineDL, GL_COMPILE);

		glBegin(GL_QUADS);

			glVertex3fv(vecLEngRBottom.arrVals);
			glVertex3fv(vecLEngRTop.arrVals);
			glVertex3fv(vecLOBTop.arrVals);
			glVertex3fv(vecLOBBottom.arrVals);

			glVertex3fv(vecLOFBottom.arrVals);
			glVertex3fv(vecLOFTop.arrVals);
			glVertex3fv(vecLEngRTop.arrVals);
			glVertex3fv(vecLEngRBottom.arrVals);

		glEnd();

		glBegin(GL_TRIANGLES);

			glVertex3fv(vecLOBTop.arrVals);
			glVertex3fv(vecLEngRTop.arrVals);
			glVertex3fv(vecLOFTop.arrVals);

			glVertex3fv(vecLOBBottom.arrVals);
			glVertex3fv(vecLOFBottom.arrVals);
			glVertex3fv(vecLEngRBottom.arrVals);

		glEnd();
	
	glEndList();

	// Right engine is just the left one flipped
	glNewList(m_iREngineDL, GL_COMPILE);

		glPushMatrix();

		glRotatef(180, 0, 0, 1);
		glCallList(m_iLEngineDL);

		glPopMatrix();

	glEndList();


	GLfloat fHalfYDiff = (m_fYSize - m_fXSize) / 2;

	// Top engine is it rotated 90 degrees and translated down by half the y difference
	glNewList(m_iTEngineDL, GL_COMPILE);

		glPushMatrix();

		glRotatef(90, 0, 0, -1);
		glTranslatef(-fHalfYDiff, 0, 0);
		glCallList(m_iLEngineDL);

		glPopMatrix();

	glEndList();

	// Bottom is just the top flipped.
	glNewList(m_iBEngineDL, GL_COMPILE);
	
		glPushMatrix();

		glRotatef(180, 0, 0, 1);
		glCallList(m_iTEngineDL);

		glPopMatrix();

	glEndList();

}

/**
 * Constructor for the Deflector class, which sets up all the particle configurations.
 * A separate particle configuration could have been made for each engine, but this seems pointless
 * when they are all basically the same effects rotated.
 */
Deflector::Deflector(bool bP1, const Vector3f &vecPos, const Vector3f &vecSize, const Colour4f &rColour)
: WorldEntity(vecPos, 1.0f, rColour)
, m_fXSize(*vecSize.x)
, m_fYSize(*vecSize.y)
, m_fZSize(*vecSize.z)
, m_rColour(rColour)
, m_iFrameDL(0) // 0 until Init() called
, m_iFieldDL(0)
, m_bP1(bP1)
, m_iFieldTexNow(FPTextureManager::Get().GetTexture(DEF_FIELD1))
, m_fGlow(0)
, m_bEngL(false)
, m_bEngR(false)
, m_bEngT(false)
, m_bEngB(false)
, m_oLEngParticles(FPSettings::Get().GetEngineParticles(), new SphereP())
, m_oREngParticles(FPSettings::Get().GetEngineParticles(), new SphereP())
, m_oTEngParticles(FPSettings::Get().GetEngineParticles(), new SphereP())
, m_oBEngParticles(FPSettings::Get().GetEngineParticles(), new SphereP())
, m_oBoundingBox(Vector3f(-*FPSettings::Get().GetDeflectorSize().x/2, 
						  -*FPSettings::Get().GetDeflectorSize().y/2, 
						  -*FPSettings::Get().GetDeflectorSize().z/2),
				 Vector3f(*FPSettings::Get().GetDeflectorSize().x/2, 
						  *FPSettings::Get().GetDeflectorSize().y/2, 
						  *FPSettings::Get().GetDeflectorSize().z/2))
, m_fRotation(0)
{  
	const FPSettings& rFPSettings = FPSettings::Get();

	Vector3f vecVertAdj (0, *rFPSettings.GetDeflectorSize().x/2 - *rFPSettings.GetDeflectorSize().y/2, 0);

	// Set up the position/rotation of each engine's particles appropriately.
	ParticleConfig& rRConf = m_oREngParticles.GetConfig();
	rRConf.m_vecOriginMin.Rotate(Vector3f(0, 0, 1), 180);
	rRConf.m_vecOriginMax.Rotate(Vector3f(0, 0, 1), 180);
	rRConf.m_vecStartVelMin.Rotate(Vector3f(0, 0, 1), 180);
	rRConf.m_vecStartVelMax.Rotate(Vector3f(0, 0, 1), 180);

	ParticleConfig& rTConf = m_oTEngParticles.GetConfig();
	rTConf.m_vecOriginMin.Rotate(Vector3f(0, 0, 1), 90);
	rTConf.m_vecOriginMin -= vecVertAdj;
	rTConf.m_vecOriginMax.Rotate(Vector3f(0, 0, 1), 90);
	rTConf.m_vecOriginMax -= vecVertAdj;
	rTConf.m_vecStartVelMin.Rotate(Vector3f(0, 0, 1), 90);
	rTConf.m_vecStartVelMax.Rotate(Vector3f(0, 0, 1), 90);

	ParticleConfig& rBConf = m_oBEngParticles.GetConfig();
	rBConf.m_vecOriginMin.Rotate(Vector3f(0, 0, -1), 90);
	rBConf.m_vecOriginMin += vecVertAdj;
	rBConf.m_vecOriginMax.Rotate(Vector3f(0, 0, -1), 90);
	rBConf.m_vecOriginMax += vecVertAdj;
	rBConf.m_vecStartVelMin.Rotate(Vector3f(0, 0, -1), 90);
	rBConf.m_vecStartVelMax.Rotate(Vector3f(0, 0, -1), 90);
}



/**
 * Render() - this, as part of the GLUT render loop, draws the deflector on the screen.
 */
void Deflector::Render()
{
	const FPSettings& rFPSettings = FPSettings::Get();

	// Initialise the display list if this is the first time.
	if (m_iFrameDL == 0) Init();	

	glPushMatrix();

	// Set up the material attributes
	glMaterialfv(GL_FRONT, GL_AMBIENT, rFPSettings.GetDefAmb().arrVals);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, rFPSettings.GetDefDif().arrVals);
	glMaterialfv(GL_FRONT, GL_SPECULAR, rFPSettings.GetDefSpe().arrVals);
	glMaterialfv(GL_FRONT, GL_EMISSION, rFPSettings.GetDefEmi().arrVals);
	glMaterialf(GL_FRONT, GL_SHININESS, 10.0f);


	// Border
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	// An easier-to-see version of our player colour for the border.
	Colour4f colBorder = m_bP1 ? rFPSettings.GetP1Colour() : rFPSettings.GetP2Colour();
	colBorder.Brighten(0.3f);
	glColor4fv(colBorder.arrVals);


	glPushMatrix();
	glRotatef(m_fRotation, 0, 0, 1);

	// Border to show depth of deflector with relation to the ball.
	float fGap = 0.1f;
	glBegin(GL_LINES);

		glVertex3f(*rFPSettings.GetFTL().x+fGap, *rFPSettings.GetFTL().y-fGap, *m_vecPos.z);
		glVertex3f(*rFPSettings.GetFTR().x-fGap, *rFPSettings.GetFTR().y-fGap, *m_vecPos.z);
		
		glVertex3f(*rFPSettings.GetFTR().x-fGap, *rFPSettings.GetFTR().y-fGap, *m_vecPos.z);
		glVertex3f(*rFPSettings.GetFBR().x-fGap, *rFPSettings.GetFBR().y+fGap, *m_vecPos.z);
		
		glVertex3f(*rFPSettings.GetFBR().x-fGap, *rFPSettings.GetFBR().y+fGap, *m_vecPos.z);
		glVertex3f(*rFPSettings.GetFBL().x+fGap, *rFPSettings.GetFBL().y+fGap, *m_vecPos.z);
		
		glVertex3f(*rFPSettings.GetFBL().x+fGap, *rFPSettings.GetFBL().y+fGap, *m_vecPos.z);
		glVertex3f(*rFPSettings.GetFTL().x+fGap, *rFPSettings.GetFTL().y-fGap, *m_vecPos.z);

	glEnd();

	glPopMatrix();


	// Translate to the right position, then call the display lists.
	glTranslatef(*m_vecPos.x, *m_vecPos.y, *m_vecPos.z);
	// Flip it if it's player two's deflector.
	if (!m_bP1) glRotatef(180, 0, 1.0f, 0);

	// Engines
	Colour4f colEngineOn (m_rColour);
	colEngineOn.Brighten(0.5f);
	
	if (m_bEngL) 
		glColor4fv(colEngineOn.arrVals);
	else
		glColor4fv(m_rColour.arrVals);
	glCallList(m_iLEngineDL);

	if (m_bEngR) 
		glColor4fv(colEngineOn.arrVals);
	else
		glColor4fv(m_rColour.arrVals);
	glCallList(m_iREngineDL);

	if (m_bEngT) 
		glColor4fv(colEngineOn.arrVals);
	else
		glColor4fv(m_rColour.arrVals);
	glCallList(m_iTEngineDL);

	if (m_bEngB) 
		glColor4fv(colEngineOn.arrVals);
	else
		glColor4fv(m_rColour.arrVals);
	glCallList(m_iBEngineDL);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);


	// Metal frame
	glCallList(m_iFrameDL);

	// Forcefield: Have it "glow" if firing
	Colour4f colGlowed (m_rColour);
	colGlowed.Brighten(m_fGlow);
	glColor4fv(colGlowed.arrVals);
	glBindTexture(GL_TEXTURE_2D, m_iFieldTexNow);
	glCallList(m_iFieldDL);


	// Render particles.
	m_oLEngParticles.Render();
	m_oREngParticles.Render();
	m_oTEngParticles.Render();
	m_oBEngParticles.Render();

	glPopMatrix();


}

/**
 * This goes to the next frame of the animated texture on the deflector. This is a rather primitive
 * hard-coded way of doing things, and would not be much help if more animated texture effects are
 * to be added.
 */
void Deflector::NextFrame()
{
	const FPTextureManager& rTexConf = FPTextureManager::Get();

	if (m_iFieldTexNow == rTexConf.GetTexture(DEF_FIELD1))
	{
		m_iFieldTexNow = rTexConf.GetTexture(DEF_FIELD2);
	}
	else if (m_iFieldTexNow == rTexConf.GetTexture(DEF_FIELD2))
	{
		m_iFieldTexNow = rTexConf.GetTexture(DEF_FIELD3);
	}
	else if (m_iFieldTexNow == rTexConf.GetTexture(DEF_FIELD3))
	{
		m_iFieldTexNow = rTexConf.GetTexture(DEF_FIELD4);
	}
	else if (m_iFieldTexNow == rTexConf.GetTexture(DEF_FIELD4))
	{
		m_iFieldTexNow = rTexConf.GetTexture(DEF_FIELD5);
	}
	else if (m_iFieldTexNow == rTexConf.GetTexture(DEF_FIELD5))
	{
		m_iFieldTexNow = rTexConf.GetTexture(DEF_FIELD6);
	}
	else if (m_iFieldTexNow == rTexConf.GetTexture(DEF_FIELD6))
	{
		m_iFieldTexNow = rTexConf.GetTexture(DEF_FIELD1);
	}
	else
	{
		cerr << "Deflector::NextFrame(): m_iFieldTexNow is a weird value." << endl;
		exit(EXIT_FAILURE);
	}
}

/**
 * UpdateParticles function for the Deflector. Although this kind of breaks the distinction between
 * a drawable WorldEntity and an updatable AbstractController, moving this to the controller would
 * have required a render function for the particles there.
 */
void Deflector::UpdateParticles(int iTime)
{ 
	m_oLEngParticles.SetOn(m_bEngL);
	m_oREngParticles.SetOn(m_bEngR);
	m_oTEngParticles.SetOn(m_bEngT);
	m_oBEngParticles.SetOn(m_bEngB);

	m_oLEngParticles.Update(iTime); 
	m_oREngParticles.Update(iTime); 
	m_oTEngParticles.Update(iTime); 
	m_oBEngParticles.Update(iTime); 
}
