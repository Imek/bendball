#ifndef DEFCONTROLLER_H
#define DEFCONTROLLER_H

#include "Engine/Controller.h"
#include "Engine/Particles.h"
#include "Engine/Physics.h"

#include "FPSettings.h"
#include "Deflector.h"

/**
 * This abstract class represents a Controller specific for controlling a deflector. This includes
 * handling the deflector's particles and target position. Any control method works by setting this
 * target position, though the specific sub class decided how exactly the motion is calculated to
 * move deflector towards the target position. The deflector always has some maximum speed, which is
 * specified in FPSettings.
 */
class DefController : public AbstractController
{
protected:
	// The position of some pointer or whatever made by the control method.
	Vector3f m_vecTargetPos;

	// Values for motion of the deflector.
	GLfloat m_fSpeed;
	GLfloat m_fAccel;
	GLfloat m_fMaxSpeed;

	// How many milliseconds until the next frame of the texture animation.
	int m_iTimeTillFrame;

	// A particle object for resolving collisions (though we don't simulate this as a particle)
	Particle m_oParticle;

	// Z bounds for this deflector, depending on which player we are.
	GLfloat m_fZMin;
	GLfloat m_fZMax;

	// To manage collision.
	CollisionController m_oCollisionCont;

	// Whether the "firing" action is on or not.
	bool m_bFiring;

public:

	DefController(CONTROLLER_TYPE iType, Deflector* pControlled)
	: AbstractController(iType, pControlled)
	, m_vecTargetPos(pControlled->GetConstPosition())
	, m_iTimeTillFrame(FPSettings::Get().GetFieldAnimTime())
	, m_fZMin(*pControlled->GetConstPosition().z + FPSettings::Get().GetDeflectorMinZ())
	, m_fZMax(*pControlled->GetConstPosition().z + FPSettings::Get().GetDeflectorMaxZ()) 
	, m_fSpeed(0)
	, m_fAccel(FPSettings::Get().GetDeflectorAccel())
	, m_fMaxSpeed(FPSettings::Get().GetDeflectorSpeed())
	, m_bFiring(false)
	{
		m_oParticle.m_vecAccel = Vector3f(0, 0, 0);
		m_oParticle.m_fInvMass = 0.0;
		m_oParticle.m_fDamping = 1.0;
	}

	Vector3f& GetTargetPos() { return m_vecTargetPos; }
	// Work out what the current motion is for this deflector, to calculate spin on bouncing.
	virtual Vector3f GetMotion()=0;

	// Update (child classes should call this before their own)
	virtual void Update(int iTime)
	{
		Deflector* pControlled = (Deflector*)m_pControlled;

		// Update the animations
		//if (pControlled->GetIsP1()) cout << m_oCollisionCont.GetString() << endl << endl;
		m_iTimeTillFrame -= iTime;
		if (m_iTimeTillFrame <= 0)
		{
			m_iTimeTillFrame = FPSettings::Get().GetFieldAnimTime();
			pControlled->NextFrame();
		}

		pControlled->UpdateParticles(iTime);

		m_oCollisionCont.Update();
	}

	Particle& GetParticle() 
	{ 
		m_oParticle.m_vecPos.Set(m_pControlled->GetConstPosition());
		m_oParticle.m_vecVel.Set(GetMotion());
		
		return m_oParticle;
	}

	// Make sure the deflector doesn't go through the walls.
	void UpdateCollisions(const Vector3f& rContactNormal)
	{
		m_oCollisionCont.m_pContNormThisFrame = new Vector3f(rContactNormal);
	}

	// Get the CollisionController object that manages repeating collisions for this deflector.
	CollisionController& GetCollisionController()
	{
		return m_oCollisionCont;
	}

	// Can get/set the firing value from outside.
	bool GetFiring() const { return m_bFiring; }
	void SetFiring(bool bVal) { m_bFiring = bVal; }
};

/**
 * This is a basic (not completely finished) mouse controller for the deflector. The position
 * of the mouse pointer represents where the deflector should gradually move towards. I'm
 * pretty sure this doesn't work any more, as it has been fairly low priority; keyboard controls
 * are the primary ones.
 */
class MouseDefController : public DefController
{
private:
	// Where the imaginary pointer is located.
	Vector3f m_vecTargetPos;

public:
	MouseDefController(Deflector* pControlled)
	: DefController(MOUSE_CONTROLLER, pControlled) {}
	~MouseDefController() {}

	// Callback for mouse motion. It is necessary to do it this way, as these are indirectly
	// linked to the GLUT mouse callback functions.
	void ActiveMouseMotion(int iX, int iY);
	void PassiveMouseMotion(int iX, int iY);

	// Update function calculates the new position of the deflector.
	void Update(int iTime);

	// TODO: This needs to be implemented, if time allows.
	Vector3f GetMotion(int iTime);
};


/**
 * Keyboard controls for a deflector. This uses four keys and a fire key, which are specified for
 * each player in FPSettings. These are managed by turning flags on when keys are pressed down and
 * off when keys are lifted. Although in general we should keep a target position and move towards
 * it, it works out better to just have motion react directly to whatever keys are pressed by
 * getting the product of the vectors.
 */
class KbdDefController : public DefController
{
private:
	// Keys for different actions.
	int m_iUpKey;
	int m_iLeftKey;
	int m_iDownKey;
	int m_iRightKey;
	int m_iBackKey;
	int m_iForwardKey;
	int m_iFireKey;

	// Movement state of the thing with respect to keybard input.
	bool m_bUp;
	bool m_bLeft;
	bool m_bDown;
	bool m_bRight;
	bool m_bBack;
	bool m_bForward;
	bool m_bFireHeld;

	// If we collide, corrective controls take over.
	bool m_bUpRebound;
	bool m_bLeftRebound;
	bool m_bDownRebound;
	bool m_bRightRebound;

	// Are we player one, or player two?
	bool m_bP1;

	// Make sure it doesn't fly through the walls.
	void UpdateRebound();

public:
	KbdDefController(Deflector* pControlled, bool bP1);

	// Do keyboard motion
	void KeyDown(int iKey);
	void KeyUp(int iKey);

	// Update the motion.
	void Update(int iTime);

	// Work out motion, so that we can calculate spin on impact with the ball too.
	Vector3f GetMotion();

};

#endif /* DEFCONTROLLER_H */
