#ifndef FPGAME_H
#define FPGAME_H

#include <map>
#include <algorithm>

#include "Engine/3DUtil.h"
#include "Engine/AbstractGame.h"
#include "Engine/Particles.h"

#include "FPSettings.h"
#include "Ball.h"
#include "BallController.h"
#include "BallCollisionEvent.h"
#include "Deflector.h"
#include "DefController.h"
#include "GameField.h"

/**
 * FPGame class - this is our FuturePong (BendBall 2000) class, implementing Abstract game. It
 * embodies both the update loop and the render loop of the game, as well as initialisation
 * and management of upper details like scoring.
 */
// XXX: A lot of this could be decomposed into more classes. Specifically, physics objects
// and code for each player. (in fact the e.g. deflector entity/physics/controller should just
// be kept together in a wrapper class or on the entity)
// XXX: As well as OpenGL init/drawing code should also be separated from game logic.
class FPGame : public AbstractGame
{
private:
	// Convenient pointers to some things (note that they're still kept in the vectors too)
	vector<Ball*> m_vBalls;
	vector<BallController*> m_vBallControllers;
	DefController* m_pDefCont1;
	DefController* m_pDefCont2;
	Deflector* m_pDef1;
	Deflector* m_pDef2;
	GameField* m_pField;

	// Forces for physics.
	ParticleForceManager m_oForceManager;
	GravityForce* m_pGravityForce;

	// Springs created by firing effects
	vector<bool> m_vP1SpringOn;
	vector<bool> m_vP2SpringOn;
	vector<DeflectorSpringForce*> m_vP1GrabForces;
	vector<DeflectorSpringForce*> m_vP2GrabForces;

	// Values concerning the game scores and so on.
	int m_iP1Score;
	int m_iP2Score;
	vector<int> m_vTimesTillRespawn;
	bool m_bP1LastScored;

	// Angle for game field (and other stuff) rotation.
	GLfloat m_fRotation;

	// Returns 0 if no winner yet.
	int GetWinner()
	{
		const FPSettings& rFPSettings = FPSettings::Get();
		if (m_iP1Score >= (int)rFPSettings.GetMaxScore()) return 1;
		else if (m_iP2Score >= (int)rFPSettings.GetMaxScore()) return 2;
		return 0;
	}

	// Handles for shaders used by the game.
	GLhandleARB m_iWaveShader;

	// UpdateCollisions handles all the collision events in the game.
	void UpdateCollisions(int iTime);
	void UpdateBallCollisions(int iTime, int iBall);
	void UpdateEvents(int iTime);
	void UpdateGrabAction(int iTime, int iBall);

	// Reset the deflectors to their default Z coordinates (when the ball is reset).
	void ResetDeflectorZ();

	// Do all the lighting.
	void DoLight();

	// Draw the scene from both viewports.
	void RenderHUD();
	void RenderViewPortOne();
	void RenderViewPortTwo();

public:
	// Constructor sets up all the entities
	FPGame();
	// Destructor
	~FPGame();
	
	// We need to implement these methods.
	void Init();
	void Render();
	void Update(int iTime);
	void ActiveMouseMotion(int iX, int iY);
	void PassiveMouseMotion(int iX, int iY);
	void KeyDown(int iKey);
	void KeyUp(int iKey);	
};

#endif /* FPGAME_H */
