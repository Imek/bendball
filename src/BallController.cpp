#include "BallController.h"

#include "FPSettings.h"

/**
 * BallController constructor - this just takes the ball being controlled, which is passed up the
 * hierarchy to the AbstractController constructor, as all "Controller" classes need some
 * WorldEntity to control at least. ResetBall is called to put the ball in the middle of the field
 * in the Waiting mode.
 */
BallController::BallController(Ball* pControlled)
: AbstractController(PHYSICS_CONTROLLER, (WorldEntity*)pControlled)
, m_fAngularDamping(FPSettings::Get().GetBallAngularDamping())
, m_bCollidedLast(false)
, m_bCollidedThis(false)
, m_bRolling(false)
, m_vecStartPos(pControlled->GetPosition())
{
	ResetBall();
}


// Do any flashing effects.
void BallController::UpdateFlash(int iTime)
{	
	const FPSettings& rFPSettings = FPSettings::Get();
	Ball* pControlled = (Ball*)m_pControlled;

	if (m_iFlashTime > 0)
	{		
		Colour4f& rCol = pControlled->GetColour();
		// Move each value towards their respective "max" colour value.
		if (*rCol.r < *m_colFlashMax.r)
		{
			*rCol.r += m_fFlashRate;
			if (*rCol.r > *m_colFlashMax.r) *rCol.r = *m_colFlashMax.r;	
		}
		else if (*rCol.r > *m_colFlashMax.r)
		{
			*rCol.r -= m_fFlashRate;
			if (*rCol.r < *m_colFlashMax.r) *rCol.r = *m_colFlashMax.r;	
		}

		if (*rCol.g < *m_colFlashMax.g)
		{
			*rCol.g += m_fFlashRate;
			if (*rCol.g > *m_colFlashMax.g) *rCol.g = *m_colFlashMax.g;	
		}
		else if (*rCol.g > *m_colFlashMax.g)
		{
			*rCol.g -= m_fFlashRate;
			if (*rCol.g < *m_colFlashMax.g) *rCol.g = *m_colFlashMax.g;	
		}

		if (*rCol.b < *m_colFlashMax.b)
		{
			*rCol.b += m_fFlashRate;
			if (*rCol.b > *m_colFlashMax.b) *rCol.b = *m_colFlashMax.b;	
		}
		else if (*rCol.b > *m_colFlashMax.b)
		{
			*rCol.b -= m_fFlashRate;
			if (*rCol.b < *m_colFlashMax.b) *rCol.b = *m_colFlashMax.b;	
		}

		m_iFlashTime -= iTime;

		// If this was the last, flash us back.
		if (m_iFlashTime <= 0)
		{
			m_iFlashTime = m_iOrigFlashTime;
			m_colFlashMax = rFPSettings.GetBallColour();
		}
	}

}



/**
 * Update function for the BallController: the control of the ball is separated here from the
 * details of drawing it. In the future, when more accurate physical simulation is added, it may be
 * necessary to somehow merge the functionality of Particle with BallController, as they both
 * concern physical simulation of a point mass in the game world.
 */
void BallController::Update(int iTime)
{
	//cout << "ball speed: " << m_oParticle.m_vecVel.Length() << endl;

	const FPSettings& rFPSettings = FPSettings::Get(); 
	Ball* pControlled = (Ball*)m_pControlled;
	// If collided in both frames, assume we're rolling on a surface.
	m_bRolling = m_bCollidedLast && m_bCollidedThis;
	assert(!m_bRolling || m_vecContactNormal.Length() == 1);

	// Apply angular damping. 
	m_vecAngularVel *= pow(m_fAngularDamping, iTime);
	
	// Update the orientation from the angular velocity.
	Quaternion& rOrientation = pControlled->GetOrientation();
	Quaternion qMult (0, *m_vecAngularVel.x, *m_vecAngularVel.y, *m_vecAngularVel.z);
	rOrientation += qMult * rOrientation;
	rOrientation.Normalise();

	// Call update on the particle
	m_oParticle.Update(iTime);

	// Alter the new position slightly to reflect the curving of the ball.
	Vector3f vecCurve = m_oParticle.m_vecVel.GetUnit().Cross(m_vecAngularVel);
	vecCurve *= rFPSettings.GetCurveFactor();
	// Do not roll in the direction of the contact normal
	if (m_bRolling)
	{
		GLfloat fComp = vecCurve.Dot(m_vecContactNormal);
		vecCurve -= m_vecContactNormal * fComp;
	}
	m_oParticle.m_vecPos += vecCurve;

	m_pControlled->GetPosition().Set(m_oParticle.m_vecPos);

	// Update the flash/glow effect on the ball.
	UpdateFlash(iTime);
	// Update the particles for the ball.
	pControlled->UpdateParticles(iTime);	

	// Keep track of repeating collisions.
	m_bCollidedLast = false;
	if (m_bCollidedThis)
	{
		m_bCollidedLast = true;
		m_bCollidedThis = false;
	}	
}

/**
 * Kill() - this function causes the ball to begin dying, which initiates any relevant visual
 * effects associated with the ball's death. The ball is actually reset manually once a set amount
 * of time has passed.
 */
void BallController::Kill()
{
	const FPSettings& rFPSettings = FPSettings::Get();

	Ball* pControlled = (Ball*)m_pControlled;	
	pControlled->SetDying(true);

	// Make the ball move slower through lava.
	m_oParticle.m_fMaxSpeed = rFPSettings.GetBallSinkSpeed();

	// Stop ball spinning.
	//m_vecSpin.Zero();
}

/**
 * ResetBall() - Resets the ball to its starting position in the middle of the game field. Also
 * sets m_bWaiting to true, meaning that the space bar needs to be pressed to set off the ball in
 * random motion (though that initial motion is calculated here too).
 */
void BallController::ResetBall()
{
	const FPSettings& rFPSettings = FPSettings::Get(); 

	// Reset everything to its default state.
	Ball* pBall = (Ball*)m_pControlled;
	pBall->GetPosition().Set(m_vecStartPos);
	pBall->GetColour().Set(rFPSettings.GetBallColour());
	pBall->SetDying(false);
	
	// Reset the particle details
	m_oParticle.m_vecPos.Set(m_vecStartPos);
	m_oParticle.m_vecVel.Zero();
	m_oParticle.m_vecAccel.Zero();
	m_oParticle.m_fInvMass = rFPSettings.GetBallInvMass();
	m_oParticle.m_fDamping = rFPSettings.GetBallDamping();
	m_oParticle.m_fMaxSpeed = rFPSettings.GetBallMaxSpeed();

	// Stop spin
	m_vecAngularVel.Zero();

}


/**
 * Flash function: this causes the ball to "flash" towards a particular colour, by fading
 * gradually over the given time frame. Once it has reached that colour, it should fade
 * back. This is used for various effects, such as that for the collision of the ball
 * with the wall.
 */
void BallController::Flash(GLuint iTime, GLfloat fFlashRate, const Colour4f& colMax)
{
	m_iFlashTime = m_iOrigFlashTime = iTime;
	m_fFlashRate = fFlashRate;
	m_colFlashMax = colMax;
}
