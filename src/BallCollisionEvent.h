#ifndef BALLCOLLISIONEVENT_H
#define BALLCOLLISIONEVENT_H

#include "Engine/Event.h"
#include "Engine/Physics.h"

/**
 * Collision between the ball and some obstacle like a wall or deflector.
 */
class BallCollisionEvent : public Event
{
protected:
	ParticleCollision m_oCollision;

public:
	BallCollisionEvent(ParticleCollision oCollision)
	: Event(BALL_COLLISION_EVENT)
	, m_oCollision(oCollision) {}
	virtual ~BallCollisionEvent() {}

	string GetString() const;

	void Resolve(int iTime);
};

#endif /* BALLCOLLISIONEVENT_H */
