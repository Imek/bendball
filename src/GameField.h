#ifndef GAMEFIELD_H
#define GAMEFIELD_H

#include <vector>

#include "Engine/3DUtil.h"
#include "Engine/WorldEntity.h"
#include "Engine/Particles.h"

#include "Ball.h"

using namespace std;

/**
 * This implementation of WorldEntity represents the game field itself; in our basic version of
 * the game, it's just a cuboid for the boundaries of the game that the ball can collide with.
 * As the game was extended, particle and shader effects were added, both of which rely on timing.
 * If time allowed, it may have been a good idea to split the GameField class into an entity and
 * a controller.
 *
 * Note that the GameField is drawn differently depending on which player is viewing it (i.e. which
 * viewport is being drawn); this is because of how the lava and its particles tends to obscure
 * the camera view and is generally unnecessary to be drawn twice anyway.
 */
class GameField : public WorldEntity
{
private:
	// Draw a border for the depth of the ball.
	vector<Ball*> m_vBalls;

	// IDs for the display lists.
	GLuint m_iWallDL;
	GLuint m_iLavaDL;

	// Time variable for the lava shader.
	int m_iTime;

	// Rotation value.
	GLfloat m_fRotation;

	// Which lava to draw, depending on the viewport.
	bool m_bP1Lava;

	// Particles.
	ParticleGenerator m_oLavaParticles1, m_oLavaParticles2;

	// We store the quad for each wall for collision detection.
	Quad m_qLWall, m_qTWall, m_qRWall, m_qBWall; 
	Quad m_qP1Lava, m_qP2Lava;
	Plane m_plnP1Lava, m_plnP2Lava;

public:
	GameField(vector<Ball*> vBalls, GLhandleARB iWaveShader);		
	~GameField() {}

	void SetVisibleLava(bool bP1) { m_bP1Lava = bP1; }
	void Render();
	void Update(int iTime);

	// Get the quads for collision detection
	vector<Quad> GetWallQuads() const 
	{ 
		vector<Quad> vWallQuads;
		vWallQuads.push_back(m_qLWall.GetRotated(Vector3f(0, 0, -1), m_fRotation));
		vWallQuads.push_back(m_qRWall.GetRotated(Vector3f(0, 0, -1), m_fRotation));
		vWallQuads.push_back(m_qTWall.GetRotated(Vector3f(0, 0, -1), m_fRotation));
		vWallQuads.push_back(m_qBWall.GetRotated(Vector3f(0, 0, -1), m_fRotation));
		return vWallQuads; 	
	}

	// Lava is done as planes to simplify collision detection.
	vector<Plane> GetLavaPlanes() const 
	{ 
		vector<Plane> vLavaPlanes;
		vLavaPlanes.push_back(m_plnP1Lava);
		vLavaPlanes.push_back(m_plnP2Lava);
		return vLavaPlanes; 	
	}

	void SetRotation(GLfloat fRotation)
	{
		m_fRotation = fRotation;
	}
};

#endif /* GAMEFIELD_H */
