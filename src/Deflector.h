#ifndef DEFLECTOR_H
#define DEFLECTOR_H

#include "Engine/WorldEntity.h"
#include "Engine/Particles.h"

/**
 * Deflector class: this specialisation of WorldEntity represents a Deflector, i.e. one of the
 * paddles controlled by the player. DeflectorController handles the actual controlling of this
 * object, as well as the animated texture; here we handle the display lists for the models.
 * The animated texture could have been handled here (or ideally with an AnimatedTexture class),
 * which may have seemed more appropriate, but as the texture relies on timing that would mean 
 * adding an Update(int iTime) function to the Deflector class and blurring the distinction
 * between a WorldEntity and an AbstractController. A similar problem is with the particles, 
 * which need to be both updated and rendered; in the end, I decided to put it in this class and
 * create an UpdateParticles(int iTime) class.
 */
class Deflector : public WorldEntity
{
protected:
	// Physical attributes
	GLfloat m_fXSize;
	GLfloat m_fYSize;
	GLfloat m_fZSize;
	const Colour4f& m_rColour;

	GLfloat m_fRotation;

	// Information for pre-loaded display lists / textures.
	GLuint m_iFrameDL;
	GLuint m_iFieldDL;	
	GLuint m_iLEngineDL;
	GLuint m_iREngineDL;
	GLuint m_iTEngineDL;
	GLuint m_iBEngineDL;

	GLuint m_iFieldTexNow;

	float m_fGlow;

	// Are we player one, or player two?
	bool m_bP1;

	// Which engines are on?
	bool m_bEngL;
	bool m_bEngR;
	bool m_bEngT;
	bool m_bEngB;
		
	// Init the display list & textures..
	void Init();

	// Particles for engines.
	ParticleGenerator m_oLEngParticles;
	ParticleGenerator m_oREngParticles;
	ParticleGenerator m_oTEngParticles;
	ParticleGenerator m_oBEngParticles;

	// Stuff for collision detection.
	AxisAlignedBoundingBox m_oBoundingBox;
	

public:
	// Constructor from vectors and values
	Deflector(bool bP1, const Vector3f& vecPos, const Vector3f& vecSize,
			  const Colour4f& rColour);
	// Destructor
	virtual ~Deflector() { }

	// Draw it
	virtual void Render();

	// Manipulate the glow value
	GLfloat GetGlow() { return m_fGlow; }
	void SetGlow(GLfloat fNewVal) { m_fGlow = fNewVal; }

	bool GetFiring() const { return m_fGlow > 0; }

	// Animate the texture
	void NextFrame();
	// Switch an engine effect and particles on/off
	void SetEngineL(bool bOn) { m_bEngL = bOn; }
	void SetEngineR(bool bOn) { m_bEngR = bOn; }
	void SetEngineT(bool bOn) { m_bEngT = bOn; }
	void SetEngineB(bool bOn) { m_bEngB = bOn; }

	// Update particles.
	void UpdateParticles(int iTime);

	// Get the quads for collision detection (need to translate!)
	AxisAlignedBoundingBox GetBoundingBox() 
	{ 
		AxisAlignedBoundingBox oBox (m_oBoundingBox);
		oBox.Translate(m_vecPos);
		return oBox; 
	}

	bool GetIsP1() const
	{
		return m_bP1;
	}

	// Set the rotation of the marker to match that of the game field.
	void SetMarkerRotation(GLfloat fRotation)
	{
		m_fRotation = fRotation;
	}

};

#endif /* DEFLECTOR_H */
