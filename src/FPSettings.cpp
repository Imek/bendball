#include "FPSettings.h"

// Initialise the instance of each singleton to NULL, so that it can be checked whether
// they have been instantiated yet when their respective Get methods are called.
FPSettings* FPSettings::m_pInstance = NULL;

// Texture manager constructor - loads up all the textures needed for my game.
FPTextureManager::FPTextureManager()
{
	/**
	 * Load up all the textures, putting their IDs into the map.
	 */
	cout << "Loading textures..." << endl;
	char* arrTexPaths[12] = { 
		"./data/forcefield1.bmp", "./data/forcefield2.bmp", "./data/forcefield3.bmp", 
		"./data/forcefield4.bmp", "./data/forcefield5.bmp", "./data/forcefield6.bmp",
		"./data/metal.bmp", "./data/ball.bmp", "./data/wall.bmp", "./data/wall2.bmp", 
		"./data/lava.bmp", "./data/particle.bmp" };
	GLuint arrTexNames[12];
	LoadTextures(11, arrTexPaths, arrTexNames);
	m_mTextures[DEF_FIELD1] = arrTexNames[0];
	m_mTextures[DEF_FIELD2] = arrTexNames[1];
	m_mTextures[DEF_FIELD3] = arrTexNames[2];
	m_mTextures[DEF_FIELD4] = arrTexNames[3];
	m_mTextures[DEF_FIELD5] = arrTexNames[4];
	m_mTextures[DEF_FIELD6] = arrTexNames[5];
	m_mTextures[DEF_METAL] = arrTexNames[6];
	m_mTextures[BALL] = arrTexNames[7];
	m_mTextures[WALL] = arrTexNames[8];
	m_mTextures[WALL2] = arrTexNames[9];
	m_mTextures[LAVA] = arrTexNames[10];
	m_mTextures[PARTICLE] = arrTexNames[11];
}


/**
 * Here in the FPSettings constructor, all of the pertinent globally-accessible values are set.
 * For safety, none of the set values can be altered by any other code. This allows various
 * attributes of the game to be altered without picking through all of the code. This could have
 * been done in a few different ways, such as with variables in the global scope or with static
 * member variables instead of a singleton, but this method makes best use of const-correctness
 * and the singleton pattern to keep global config values safely in one place.
 */
// XXX: This could be split up into separate classes.
FPSettings::FPSettings()
: m_pP1LavaLight(NULL)
, m_pP2LavaLight(NULL)
, m_pBallLight1(NULL)
, m_pBallLight2(NULL)
, m_pLavaShader(NULL)
{
	// Deflector settings
	m_vecDeflectorSize.Set(2.0f, 2.0f, 0.2f);
	m_fDeflectorXStartPos = 0;
	m_fDeflectorYStartPos = 0;
	m_fDeflectorSpeed = 0.032f;	
	m_fDeflectorAccel = 0.003f;
	m_iFieldAnimTime = 58;

	m_colDefAmb.Set(0.1f, 0.1f, 0.1f, 1.0f);
	m_colDefDif.Set(0.3f, 0.3f, 0.3f, 1.0f);
	m_colDefSpe.Set(0.9f, 0.9f, 0.9f, 1.0f);
	m_colDefEmi.Set(0.0f, 0.0f, 0.0f, 1.0f);
	m_colP1.Set(0.2f, 0.2f, 0.6f, 0.6f);
	m_colP2.Set(0.6f, 0.2f, 0.2f, 0.6f);
	m_fDefGlowRate = 0.004f;
	m_fDefMaxGlow = 0.2f;

	// Ball settings
	m_colBall.Set(0.6f, 0.6f, 0.8f, 1.0f);
	m_fBallRadius = 0.35f;
	m_iBallSlices = 20;
	m_iBallStacks = 20;
	// NOTE: Currently due to collision detection limitations this max speed has
	// to be a bit lower than I'd like. It could be made higher if collision detect
	m_fBallMaxSpeed = 0.06f;
	m_fBallSinkSpeed = 0.004f;
	m_fBallFireMult = 1.5f;
	m_iBallRespawnDelay = 3000;
	m_colBallAmb.Set(0.2f, 0.2f, 0.2f, 1.0f);
	m_colBallDif.Set(0.2f, 0.2f, 0.2f, 1.0f);
	m_colBallSpe.Set(0.9f, 0.9f, 0.9f, 1.0f);
	m_iBallShi = 75;
	// Probably shouldn't bother with the emission stuff
	m_colBallEmi.Set(0.0f, 0.0f, 0.0f, 1.0f);

	// Game field settings
	// NOTE: Can't currently make game field length smaller than this without
	// the ball spawning  behind the deflector. Ball positioning logic needs
	// improving if you want a smaller game field.
	m_fGameFieldLength = 27.0f;
	m_fGameFieldWidth = 8.0f;
	m_fGameFieldHeight = 8.0f;
	m_fGameFieldDistFromCam = 10.0f;
	m_iNoFieldDivisions = 10;
	m_colWallAmb.Set(0.2f, 0.2f, 0.2f, 1.0f);
	m_colWallDif.Set(0.4f, 0.35f, 0.35f, 1.0f);
	m_colWallSpe.Set(0.5f, 0.5f, 0.5f, 1.0f);
	m_colWallEmi.Set(0.0f, 0.0f, 0.0f, 0.0f);
	m_colLavaAmb.Set(0.3f, 0.1f, 0.1f, 1.0f);
	m_colLavaDif.Set(0.5f, 0.3f, 0.3f, 1.0f);
	m_colLavaSpe.Set(0.2f, 0.2f, 0.2f, 1.0f);
	m_colLavaEmi.Set(0.0f, 0.0f, 0.0f, 0.0f); // Do lava light with a point light

	m_fGameFieldRotateRate = 0.15f;

	// Precalculated stuff for efficiency
	m_fDeflectorMaxX = m_fGameFieldWidth/2 - *m_vecDeflectorSize.x / 2;
	m_fDeflectorMinX = -m_fDeflectorMaxX;
	m_fDeflectorMaxY = m_fGameFieldHeight/2 - *m_vecDeflectorSize.y / 2;
	m_fDeflectorMinY = -m_fDeflectorMaxY;
	// Relative, depending on P1 or P2.
	m_fDeflectorMinZ = -3.0f;
	m_fDeflectorMaxZ = 3.0f;
	m_fBallMinX = -m_fGameFieldWidth/2 + m_fBallRadius;
	m_fBallMaxX = -m_fBallMinX;
	m_fBallMinY = -m_fGameFieldHeight/2 + m_fBallRadius;
	m_fBallMaxY = -m_fBallMinY;
	m_fBallMinZ = -(m_fGameFieldDistFromCam + m_fGameFieldLength) + m_fBallRadius;
	m_fBallMaxZ = -m_fGameFieldDistFromCam - m_fBallRadius;
	m_vBallStartPos.Set(0, 0, 
		-(m_fGameFieldDistFromCam + m_fGameFieldLength/2));
	m_fBallCircum = Vector3f::PI * 2 * m_fBallRadius;

	// A vector for each corner of the field, as we're going to re-use those points a lot..
	m_vecFTL.Set(-m_fGameFieldWidth/2, m_fGameFieldHeight/2, 0);
	m_vecFTR.Set(m_fGameFieldWidth/2, m_fGameFieldHeight/2, 0);
	m_vecFBR.Set(m_fGameFieldWidth/2, -m_fGameFieldHeight/2, 0);
	m_vecFBL.Set(-m_fGameFieldWidth/2, -m_fGameFieldHeight/2, 0);
	m_vecBTL.Set(-m_fGameFieldWidth/2, m_fGameFieldHeight/2, -(m_fGameFieldDistFromCam*2+m_fGameFieldLength));
	m_vecBTR.Set(m_fGameFieldWidth/2, m_fGameFieldHeight/2, -(m_fGameFieldDistFromCam*2+m_fGameFieldLength));
	m_vecBBR.Set(m_fGameFieldWidth/2, -m_fGameFieldHeight/2, -(m_fGameFieldDistFromCam*2+m_fGameFieldLength));
	m_vecBBL.Set(-m_fGameFieldWidth/2, -m_fGameFieldHeight/2, -(m_fGameFieldDistFromCam*2+m_fGameFieldLength));
	m_vecP1Eye.Set(0, 0, 0);
	m_vecP1Centre.Set(0, 0, -(m_fGameFieldDistFromCam + m_fGameFieldLength/2));
	m_vecP1Up.Set(0, 1, 0);
	m_vecP2Eye.Set(0, 0, -(m_fGameFieldDistFromCam*2 + m_fGameFieldLength));
	m_vecP2Centre.Set(m_vecP1Centre);
	m_vecP2Up.Set(m_vecP1Up);

	// Keyboard controls
	m_iStartKey = ' ';
	m_iP1UpKey = 'w';
	m_iP1LeftKey = 'a';
	m_iP1DownKey = 's';
	m_iP1RightKey = 'd';
	m_iP1BackKey = 'g';
	m_iP1ForwardKey = 't';
	m_iP1FireKey = ' ';

	m_iP2UpKey = '8';
	m_iP2LeftKey = '4';
	m_iP2DownKey = '2';
	m_iP2RightKey = '6';
	m_iP2BackKey = '1';
	m_iP2ForwardKey = '7';
	m_iP2FireKey = '5';

	m_iMaxScore = 10;

	// Lights
	// TODO: Light instances should be part of entity code, not here in config!
	// Particularly for balls.
	m_pP1LavaLight = new LightConfig(GL_LIGHT1,
			 Colour4f(0.1f, 0.1f, 0.1f, 1.0f), // Ambient
			 Colour4f(0.5f, 0.3f, 0.3f, 1.0f), // Diffuse
			 Colour4f(0.5f, 0.1f, 0.1f, 1.0f), // Specular
			 Vector3f(0, 0, 0), // Position
			 0.9f, 0.0001f, 0.00001f); // Attenuation (const/linear/quad)

	m_pP2LavaLight = new LightConfig(GL_LIGHT2,
			 Colour4f(0.1f, 0.1f, 0.1f, 1.0f), // Ambient
			 Colour4f(0.5f, 0.3f, 0.3f, 1.0f), // Diffuse
			 Colour4f(0.5f, 0.1f, 0.1f, 1.0f), // Specular
			 Vector3f(0, 0, -(m_fGameFieldDistFromCam*2 + m_fGameFieldLength)), // Position
			 0.9f, 0.0001f, 0.00001f); // Attenuation (const/linear/quad)

	// Ball light's position is set every frame (one for each ball)
	m_pBallLight1 = new LightConfig(GL_LIGHT3,
			Colour4f(0.0f, 0.0f, 0.0f, 1.0f), // Ambient
			Colour4f(0.0f, 0.2f, 0.3f, 1.0f), // Diffuse
			Colour4f(0.3f, 0.2f, 0.3f, 1.0f), // Specular
			Vector3f(0, 0, 0), // Position
			0.85f, 0.00001f, 0.00002f); // Attenuation (const/linear/quad)

	m_pBallLight2 = new LightConfig(GL_LIGHT4,
			Colour4f(0.0f, 0.0f, 0.0f, 1.0f), // Ambient
			Colour4f(0.0f, 0.2f, 0.3f, 1.0f), // Diffuse
			Colour4f(0.3f, 0.2f, 0.3f, 1.0f), // Specular
			Vector3f(0, 0, 0), // Position
			0.85f, 0.00001f, 0.00002f); // Attenuation (const/linear/quad)

	// Particle configs.

	// Player 1's lava particles. For player 2, some of these values need changing.
	m_oLavaParticles.m_vecOriginMin.Set(*m_vecFTL.x, *m_vecFTL.y, *m_vecFTL.z);
	m_oLavaParticles.m_vecOriginMax.Set(*m_vecFBR.x, *m_vecFBR.y, *m_vecFBR.z);
	m_oLavaParticles.m_vecStartVelMin.Set(0.0, 0.0, -0.027);
	m_oLavaParticles.m_vecStartVelMax.Set(0.0, 0.0, -0.036);
	m_oLavaParticles.m_vecAccelMin.Set(0.0, 0.0, 0.00008);
	m_oLavaParticles.m_vecAccelMax.Set(0.0, 0.0, 0.00008);
	m_oLavaParticles.m_iMinAngle = 0;
	m_oLavaParticles.m_iMaxAngle = 5;
	m_oLavaParticles.m_iLifeSpanMax = 1300;
	m_oLavaParticles.m_iLifeSpanMin = 900;
	m_oLavaParticles.m_fInvMassMin = 1.0f;
	m_oLavaParticles.m_fInvMassMax = 1.0f;
	m_oLavaParticles.m_fDampingMin = 1.0f;
	m_oLavaParticles.m_fDampingMax = 1.0f;
	m_oLavaParticles.m_fSizeMin = 0.15f;
	m_oLavaParticles.m_fSizeMax = 0.20f;
	m_oLavaParticles.m_colStartMax.Set(1.0, 0.7, 0.5, 1.0);
	m_oLavaParticles.m_colStartMin.Set(0.8, 0.6, 0.4, 1.0);
	m_oLavaParticles.m_colEndMax.Set(1.0, 1.0, 1.0, 0.5);
	m_oLavaParticles.m_colEndMin.Set(1.0, 0.6, 0.4, 0.5);
	m_oLavaParticles.m_iMaxParticles = 100;
	m_oLavaParticles.m_fParticleRate = 0.01;
	m_oLavaParticles.m_iTrailNoMin = 10;
	m_oLavaParticles.m_iTrailNoMax = 10;
	m_oLavaParticles.m_iTrailDelayMin = 40;
	m_oLavaParticles.m_iTrailDelayMax = 40;
	m_oLavaParticles.m_fTrailSizeEndMin = 0.01f; 
	m_oLavaParticles.m_fTrailSizeEndMax = 0.01f; 
	m_oLavaParticles.m_colTrailColEndMin.Set(1.0, 0.6, 0.2, 0.0);
	m_oLavaParticles.m_colTrailColEndMax.Set(0.8, 0.4, 0.0, 0.0);
	m_oLavaParticles.m_fMaxSpeed = 1.0f;

	// Shader settings for the lava.
	m_fWaveXFreq = 0.65f;
	m_fWaveYFreq = 0.65f;
	m_fWaveHeight = 2.0f;

	// Left engine particles. Rotate to get top/right/bottom.
	m_oEngineParticles.m_vecOriginMin.Set(-*m_vecDeflectorSize.x/2, 0, 0);
	m_oEngineParticles.m_vecOriginMax.Set(-*m_vecDeflectorSize.x/2, 0, 0);
	m_oEngineParticles.m_vecStartVelMin.Set(-0.005, 0.0, 0.0);
	m_oEngineParticles.m_vecStartVelMax.Set(-0.01, 0.0, 0.0);
	m_oEngineParticles.m_vecAccelMin.Set(0.0, 0.0, 0.00009);
	m_oEngineParticles.m_vecAccelMax.Set(0.0, 0.0, 0.00009);
	m_oEngineParticles.m_iMinAngle = 0;
	m_oEngineParticles.m_iMaxAngle = 10;
	m_oEngineParticles.m_iLifeSpanMax = 175;
	m_oEngineParticles.m_iLifeSpanMin = 75;
	m_oEngineParticles.m_fInvMassMin = 1.0f;
	m_oEngineParticles.m_fInvMassMax = 1.0f;
	m_oEngineParticles.m_fDampingMin = 1.0f;
	m_oEngineParticles.m_fDampingMax = 1.0f;
	m_oEngineParticles.m_fSizeMin = 0.03f;
	m_oEngineParticles.m_fSizeMax = 0.15f;
	m_oEngineParticles.m_colStartMax.Set(1.0, 1.0, 1.0, 0.9);
	m_oEngineParticles.m_colStartMin.Set(0.9, 0.9, 0.9, 0.7);
	m_oEngineParticles.m_colEndMax.Set(1.0, 1.0, 1.0, 0.1);
	m_oEngineParticles.m_colEndMin.Set(0.8, 0.8, 0.8, 0.1);
	m_oEngineParticles.m_iMaxParticles = 2000;
	m_oEngineParticles.m_fParticleRate = 0.1;
	m_oEngineParticles.m_iTrailNoMin = 1;
	m_oEngineParticles.m_iTrailNoMax = 1;
	m_oEngineParticles.m_iTrailDelayMin = 40;
	m_oEngineParticles.m_iTrailDelayMax = 40;
	m_oEngineParticles.m_fTrailSizeEndMin = 0.01f; 
	m_oEngineParticles.m_fTrailSizeEndMax = 0.01f; 
	m_oEngineParticles.m_colTrailColEndMin.Set(1.0, 0.6, 0.2, 0.0);
	m_oEngineParticles.m_colTrailColEndMax.Set(0.8, 0.4, 0.0, 0.0);
	m_oEngineParticles.m_fMaxSpeed = 5.0f;

	// Ball impact particles: origin and start velocity need to be set.
	m_oImpactParticles.m_vecOriginMin.Set(0, 0, 0);
	m_oImpactParticles.m_vecOriginMax.Set(0, 0, 0);
	m_oImpactParticles.m_vecStartVelMin.Set(0.01, 0.0, 0.0);
	m_oImpactParticles.m_vecStartVelMax.Set(0.03, 0.0, 0.0);
	m_oImpactParticles.m_vecAccelMin.Set(0.0, 0.0, 0.0);
	m_oImpactParticles.m_vecAccelMax.Set(0.0, 0.0, 0.0);
	m_oImpactParticles.m_iMinAngle = 60;
	m_oImpactParticles.m_iMaxAngle = 90;
	m_oImpactParticles.m_iLifeSpanMax = 75;
	m_oImpactParticles.m_iLifeSpanMin = 50;
	m_oImpactParticles.m_fInvMassMin = 1.0f;
	m_oImpactParticles.m_fInvMassMax = 1.0f;
	m_oImpactParticles.m_fDampingMin = 1.0f;
	m_oImpactParticles.m_fDampingMax = 1.0f;
	m_oImpactParticles.m_fSizeMin = 0.02f;
	m_oImpactParticles.m_fSizeMax = 0.04f;
	m_oImpactParticles.m_colStartMax.Set(1.0, 0.8, 0.8, 1.0);
	m_oImpactParticles.m_colStartMin.Set(0.9, 0.7, 0.5, 1.0);
	m_oImpactParticles.m_colEndMax.Set(1.0, 1.0, 1.0, 0.0);
	m_oImpactParticles.m_colEndMin.Set(1.0, 1.0, 1.0, 0.0);
	m_oImpactParticles.m_iMaxParticles = 50;
	m_oImpactParticles.m_fParticleRate = 50;
	m_oImpactParticles.m_iTrailNoMin = 1;
	m_oImpactParticles.m_iTrailNoMax = 1;
	m_oImpactParticles.m_iTrailDelayMin = 40;
	m_oImpactParticles.m_iTrailDelayMax = 40;
	m_oImpactParticles.m_fTrailSizeEndMin = 0.01f; 
	m_oImpactParticles.m_fTrailSizeEndMax = 0.01f; 
	m_oImpactParticles.m_colTrailColEndMin.Set(1.0, 0.6, 0.2, 0.0);
	m_oImpactParticles.m_colTrailColEndMax.Set(0.8, 0.4, 0.0, 0.0);
	m_oImpactParticles.m_fMaxSpeed = 5.0f;

	// Config for the wavey vertex shader for use with the lava.
	m_pLavaShader = new ShaderConfig("data/waves_vert.glsl", "data/waves_frag.glsl");

	// Physics settings
	m_vecGravityAccel.Set(0, 0, 0);
	// Uncomment for gravity!
	m_vecGravityAccel.Set(0, -0.000030, 0);

	m_fBallInvMass = 15.0f;
	m_fBallDamping = 0.99999f;
	m_fAttachedDamping = 0.95f;
	m_fBallAngularDamping = 0.9999f;
	
	m_oMetal1.m_iTexture = WALL;
	m_oMetal1.m_fRestitution = 0.90f;
	m_oMetal1.m_fFriction = 0.9995f;
		
	m_oMetal2.m_iTexture = WALL2;
	m_oMetal2.m_fRestitution = 0.65f;
	m_oMetal2.m_fFriction = 0.999f;

	m_fServingSpringConst = 0.0001f;
	m_fServingZDist = 2.0f;
	m_fGrabMaxDist = 15.0f;
	// The following two aren't used for now
	m_fGrabMaxXYDist = 0.2f;
	m_fGrabMaxSpeed = 0.01f;
	m_fCurveFactor = 0.85f;

	// Calculated velocity of the walls (circle circumference / rotation period)
	m_fWallVelocity = (Vector3f::PI * m_fGameFieldWidth/1000) / (360 / m_fGameFieldRotateRate);
}

// Get the instance of the singleton class.
const FPSettings& FPSettings::Get()
{
	if (m_pInstance == NULL)
		m_pInstance = new FPSettings();

	return *m_pInstance;
}
