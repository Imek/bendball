#include "GameField.h"

#include "FPSettings.h"

/**
 * GameField constructor - this sets up all the aspects of the game field, including setting up
 * the textures and putting much of it into appropriate display lists for efficiency.
 */
GameField::GameField(vector<Ball*> vBalls, GLhandleARB iWaveShader)
: WorldEntity()
, m_vBalls(vBalls)
, m_iTime(0)
, m_oLavaParticles1(FPSettings::Get().GetLavaParticles(), new SphereP())
, m_oLavaParticles2(FPSettings::Get().GetLavaParticles(), new SphereP())
// Set up a Quad object for each wall.
, m_qLWall(FPSettings::Get().GetFTL(), FPSettings::Get().GetBTL(),
		   FPSettings::Get().GetBBL(), FPSettings::Get().GetFBL(), Vector3f(1, 0, 0))
, m_qRWall(FPSettings::Get().GetBTR(), FPSettings::Get().GetFTR(),
		   FPSettings::Get().GetFBR(), FPSettings::Get().GetBBR(), Vector3f(-1, 0, 0))
, m_qTWall(FPSettings::Get().GetBTL(), FPSettings::Get().GetFTL(),
		   FPSettings::Get().GetFTR(), FPSettings::Get().GetBTR(), Vector3f(0, -1, 0))
, m_qBWall(FPSettings::Get().GetFBL(), FPSettings::Get().GetBBL(),
		   FPSettings::Get().GetBBR(), FPSettings::Get().GetFBR(), Vector3f(0, 1, 0))
, m_plnP1Lava(Vector3f(0, 0, -1), FPSettings::Get().GetFTR())
, m_plnP2Lava(Vector3f(0, 0, 1), FPSettings::Get().GetBTR())
, m_fRotation(0)
{
	const FPSettings& rFPSettings = FPSettings::Get();
	const FPTextureManager& rTexConf = FPTextureManager::Get();

	// Set up the lava quads.
	GLfloat fAddX = 11*rFPSettings.GetGameFieldWidth()/32;
	GLfloat fAddY = 11*rFPSettings.GetGameFieldHeight()/32;

	m_qP1Lava.TL.Set(*FPSettings::Get().GetFTR().x + fAddX,
					 *FPSettings::Get().GetFTR().y + fAddY,
					 *FPSettings::Get().GetFTR().z);
	m_qP1Lava.TR.Set(*FPSettings::Get().GetFTL().x - fAddX,
					 *FPSettings::Get().GetFTL().y + fAddY,
					 *FPSettings::Get().GetFTL().z);
	m_qP1Lava.BR.Set(*FPSettings::Get().GetFBL().x - fAddX,
					 *FPSettings::Get().GetFBL().y - fAddY,
					 *FPSettings::Get().GetFBL().z);
	m_qP1Lava.BL.Set(*FPSettings::Get().GetFBR().x + fAddX,
					 *FPSettings::Get().GetFBR().y - fAddY,
					 *FPSettings::Get().GetFBR().z);
	m_qP1Lava.N.Set(Vector3f(0, 0, -1));

	m_qP2Lava.TL.Set(*FPSettings::Get().GetBTL().x,
					 *FPSettings::Get().GetBTL().y,
					 *FPSettings::Get().GetBTL().z);
	m_qP2Lava.TR.Set(*FPSettings::Get().GetBTR().x,
					 *FPSettings::Get().GetBTR().y,
					 *FPSettings::Get().GetBTR().z);
	m_qP2Lava.BR.Set(*FPSettings::Get().GetBBR().x,
					 *FPSettings::Get().GetBBR().y,
					 *FPSettings::Get().GetBBR().z);
	m_qP2Lava.BL.Set(*FPSettings::Get().GetBBL().x,
					 *FPSettings::Get().GetBBL().y,
					 *FPSettings::Get().GetBBL().z);
	m_qP2Lava.N.Set(Vector3f(0, 0, 1));

	// Reorient the second particle generator for player 2's lava pool.
	ParticleConfig& rConf = m_oLavaParticles2.GetConfig();
	*rConf.m_vecOriginMin.z -= (rFPSettings.GetGameFieldLength() + rFPSettings.GetGameFieldDistFromCam()*2);
	*rConf.m_vecOriginMax.z -= (rFPSettings.GetGameFieldLength() + rFPSettings.GetGameFieldDistFromCam()*2);
	*rConf.m_vecStartVelMin.z = -*rConf.m_vecStartVelMin.z;
	*rConf.m_vecStartVelMax.z = -*rConf.m_vecStartVelMax.z;
	*rConf.m_vecAccelMin.z = -*rConf.m_vecAccelMin.z;
	*rConf.m_vecAccelMax.z = -*rConf.m_vecAccelMax.z;

	// Particles are always on.
	m_oLavaParticles1.SetOn(true);
	m_oLavaParticles2.SetOn(true);

	// Initialise the display lists.
	m_iWallDL = glGenLists(2);
	m_iLavaDL = m_iWallDL + 1;

	// Display list for the walls.
	glNewList(m_iWallDL, GL_COMPILE);
	
		// Set up the material attributes and texture.
		glMaterialfv(GL_FRONT, GL_AMBIENT, rFPSettings.GetWallAmb().arrVals);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, rFPSettings.GetWallDif().arrVals);
		glMaterialfv(GL_FRONT, GL_SPECULAR, rFPSettings.GetWallSpe().arrVals);
		glMateriali(GL_FRONT, GL_SHININESS, 10);
		glMaterialfv(GL_FRONT, GL_EMISSION, rFPSettings.GetWallEmi().arrVals);
		
		glBindTexture(GL_TEXTURE_2D, rTexConf.GetTexture(WALL));

		glBegin(GL_QUADS);

			DrawQuadMesh(m_qLWall.TL, m_qLWall.TR, m_qLWall.BR, m_qLWall.BL, m_qLWall.N, 5, 5);
			DrawQuadMesh(m_qRWall.TL, m_qRWall.TR, m_qRWall.BR, m_qRWall.BL, m_qRWall.N, 5, 5);
					
		glEnd();

		glBindTexture(GL_TEXTURE_2D, rTexConf.GetTexture(WALL2));
	
		glBegin(GL_QUADS);
		
			DrawQuadMesh(m_qTWall.TL, m_qTWall.TR, m_qTWall.BR, m_qTWall.BL, m_qTWall.N, 5, 5);
			DrawQuadMesh(m_qBWall.TL, m_qBWall.TR, m_qBWall.BR, m_qBWall.BL, m_qBWall.N, 5, 5);

		glEnd();

	glEndList();

	// Display list for the lava.
	glNewList(m_iLavaDL, GL_COMPILE);

		// Lava texture.
		glBindTexture(GL_TEXTURE_2D, FPTextureManager::Get().GetTexture(LAVA));

		// Draw the lava, depending on which viewport it is.
		glBegin(GL_QUADS);

			// This is for the lava behind player one; it is translated if m_bP1Lava is set to false when
			// Render() is called.
			DrawQuadMesh(m_qP1Lava.TL, m_qP1Lava.TR, m_qP1Lava.BR, m_qP1Lava.BL, m_qP1Lava.N, 5, 5);
		glEnd();
	
	glEndList();
}

/**
 * This is the Draw function for the GameField, which is part of the main game render loop. Its
 * responsibility is to make sure every aspect of the game field is rendered, including the lava
 * effects. As it is linked to the GLUT render loop, this is pretty much the only way this could
 * have been done, beyond splitting the whole class between walls and lava.
 */
void GameField::Render()
{

	const FPSettings& rFPSettings = FPSettings::Get();

	glColor4f(0.5f, 0.5f, 0.5f, 1.0f);

	// Front and back (lava)

	// Set up the material attributes
	glMaterialfv(GL_FRONT, GL_AMBIENT, rFPSettings.GetLavaAmb().arrVals);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, rFPSettings.GetLavaDif().arrVals);
	glMaterialfv(GL_FRONT, GL_SPECULAR, rFPSettings.GetLavaSpe().arrVals);
	glMateriali(GL_FRONT, GL_SHININESS, 30);
	glMaterialfv(GL_FRONT, GL_EMISSION, rFPSettings.GetLavaEmi().arrVals);

	// Activate the shader and set the uniform values for this frame.
	const ShaderConfig& rShader = rFPSettings.GetLavaShader();
	rShader.SwitchOn();
	rShader.SetUniform1f("time", (float)((float)m_iTime/100.0f));
	rShader.SetUniform1f("xs", rFPSettings.GetWaveXFreq());
	rShader.SetUniform1f("ys", rFPSettings.GetWaveYFreq());
	rShader.SetUniform1f("h", rFPSettings.GetWaveHeight());

	// Lava - translate and set normal based on which lava we want to draw.
	glPushMatrix();

	if (m_bP1Lava)
	{
		glNormal3f(0.0f, 0.0f, -1.0f);
	}
	else
	{
		glTranslatef(0, 0, -(rFPSettings.GetGameFieldLength() + rFPSettings.GetGameFieldDistFromCam()*2));
		glNormal3f(0.0f, 0.0f, 1.0f);		
	}
	glCallList(m_iLavaDL);

	// Done with the shader.
	rShader.SwitchOff();

	glPopMatrix();

	// Walls
	glPushMatrix();
	glRotatef(m_fRotation, 0, 0, 1);
	glCallList(m_iWallDL);
	glPopMatrix();


	// Finally, do the particles.
	if (m_bP1Lava)
		m_oLavaParticles1.Render();
	else
		m_oLavaParticles2.Render();

	

}

/**
 * The update function of the GameField, which technically should be either in a separate 
 * GameFieldController class or in the FPGame update function, manages the time variable for
 * the shader effect and the updating of the particle effects.
 */
void GameField::Update(int iTime)
{
	const FPSettings& rFPSettings = FPSettings::Get();

	// Increment the time value for the lava shader
	m_iTime = (m_iTime + iTime) % 640;

	// Update the particles for the lava.
	m_oLavaParticles1.Update(iTime);
	m_oLavaParticles2.Update(iTime);
}
