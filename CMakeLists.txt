cmake_minimum_required(VERSION 2.8)
project(BendBall) 

# Use bundled libraries (for Windows right now)
set(GLUT_ROOT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/lib/glut)
include_directories(${GLUT_ROOT_PATH}/include)

find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)

include_directories( ${OPENGL_INCLUDE_DIRS} ${GLUT_INCLUDE_DIRS} )
link_directories( ${OPENGL_LIBRARY_DIRS} ${GLUT_LIBRARY_DIRS} )

set(SOURCE_FILES
  src/glew.c
  src/Ball.cpp
  src/BallCollisionEvent.cpp
  src/BallController.cpp
  src/Core.cpp
  src/DefController.cpp
  src/Deflector.cpp
  src/FPGame.cpp
  src/FPSettings.cpp
  src/GameField.cpp
  src/Ball.h
  src/BallCollisionEvent.h
  src/BallController.h
  src/Core.h
  src/DefController.h
  src/Deflector.h
  src/FPGame.h
  src/FPSettings.h
  src/GameField.h
  src/Engine/3DUtil.cpp
  src/Engine/AbstractGame.cpp
  src/Engine/Bitmap.cpp
  src/Engine/GameSettings.cpp
  src/Engine/Particles.cpp
  src/Engine/Physics.cpp
  src/Engine/3DUtil.h
  src/Engine/AbstractGame.h
  src/Engine/Bitmap.h
  src/Engine/Controller.h
  src/Engine/Event.h
  src/Engine/GameSettings.h
  src/Engine/Particles.h
  src/Engine/Physics.h
  src/Engine/WorldEntity.h)

add_executable(BendBall ${SOURCE_FILES})

set(EXECUTABLE_OUTPUT_PATH .)

# Copy data folder over too.
add_custom_command(
  TARGET BendBall PRE_BUILD
  COMMAND ${CMAKE_COMMAND} -E copy_directory
  ${CMAKE_SOURCE_DIR}/data $<TARGET_FILE_DIR:BendBall>/../data)
